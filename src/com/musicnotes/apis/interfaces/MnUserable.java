package com.musicnotes.apis.interfaces;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnUsers;


public interface MnUserable
{
	public abstract String insertNewUser(MnUsers user, String jsonStr);
	public abstract MnUsers convertJsonToUserObj(String jsonStr);
	public abstract String getUserDetails(String userId,HttpServletRequest request);
	public abstract String fetchUserDetails(String roleId,HttpServletRequest request);
	public abstract String memberList(String userId,HttpServletRequest request);
	public abstract String updateUser(MnUsers user);
	public abstract String getViewAllActiveUserListForSearch(MnUsers user,HttpServletRequest request);
	public abstract MnUsers convertJsonToUpdateUserObj(String jsonStr);
	public abstract String getExistsEmail(String jsonStr);
	public abstract String updateEmail(String jsonStr);
	public abstract String updateProfilePicture(String userId,String filePath,HttpServletRequest request,boolean cropFlag);
	String resetPassword(String params);
	String forgetPassword(String params);
	public String getTeachersList(HttpServletRequest request);
	public abstract String ExistsUser(String jsonStr);
	public abstract String updateUserName(String jsonStr);
	public Map<String, String> getUserName(String recipients, String listType);
	public MnFeedbacks feedbackDetails(String param);
	public String getTrailEndMailValues(); 
	public String getTrailcount(String params);
	public String sendMailNotificationsForAutomatically();
	public String setUserLevelAutomatically();
	public String checkInviteuser(MnUsers user);
	public String sendUserCreationMail(String param);
	public String addNewUser(String jsonStr);
	public String fetchAddusersDetails(String params,HttpServletRequest request);
	public String getSecurityQuestion(String params);
	public String setResetPassword(String params);
	public String setResetPass();
	
	public String newToken(HttpServletRequest httpServletRequest);
	public String userMailNotificationList(
			Integer userId, String jsonStr);
	
	public String updatUserMailNotificationList(
			Integer userId, String jsonStr);
	public Boolean getUserMailNotification(Integer userId, String mailType);
	public String mailConfig(String  userId);
	public MnUsers limiteSize(String userId);
	public String getAdminSecurityQuestion();
	public String deleteProfilePicture(String userId);
	public abstract String getCrowdUserDetails(String userName,HttpServletRequest request);
}
