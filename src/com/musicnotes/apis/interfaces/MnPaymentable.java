package com.musicnotes.apis.interfaces;

public interface MnPaymentable {
	
	public String makePayment(String param);
	
	public String renewalPayment(String param);
	
	public String makePaymentFromRest(String custom, String txnNo, String txnType, String subscr_id, String amount, String domain);

}
