package com.musicnotes.apis.interfaces;

import javax.servlet.http.HttpServletRequest;

public interface MnLogable {
	
	String getNotificationsList(String userId);
	
	String inactivateNotifications(String params,String userId );
	
	String getRecentActitvity(String userId);
	
	String getRemainder(String userId, HttpServletRequest request);
	
	String inactiveRemainders(String params,String userId );

	String setNotificationsAD(String params);
	
}
