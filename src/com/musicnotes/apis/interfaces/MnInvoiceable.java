package com.musicnotes.apis.interfaces;

import com.musicnotes.apis.domain.MnInvoices;

public interface MnInvoiceable {
public String creatInvoice(MnInvoices mnInvoice);
public abstract MnInvoices convertJsonToUserObj(String jsonStr); 
}
