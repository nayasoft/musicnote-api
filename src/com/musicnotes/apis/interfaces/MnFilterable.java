package com.musicnotes.apis.interfaces;

import javax.servlet.http.HttpServletRequest;

public interface MnFilterable {

	public boolean isValidToken(String token, String date,
			HttpServletRequest request);
	
	public boolean isValidTokenForUpload(String token,String date);
	public boolean isValidTokenForUser(String token,String date,HttpServletRequest request);
	public boolean isValidTokenForNewGenerate(String token, String date,HttpServletRequest request);
}
