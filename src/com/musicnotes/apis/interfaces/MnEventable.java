package com.musicnotes.apis.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnEventDetails;

public interface MnEventable
{
	public String createNote(String mnEvent,String listId);
	public String fetchEvents(String params);
	public String fetchSchdulingEvents(String params);
	public String fetchParticularEvents(String params);
	public String fetchTodayEvents(String params);
	public String fetchCalendarSharingUsers(String params);
	public String fetchCalendarEvents(String params);
	public String getEvents(String listId, String noteId,String userId);
	public String updateNote(String params, String listId);
	public String eventAccesDecline(String params);
	public String fetchParticularEvents(Integer listId, Integer eventId, Integer userId);
	public MnEventDetails getEventsForMail(String listId, String noteId, String userId);
	
	public String getEventsFromList(String params);
	public String updateMnSubEvent(String params);
	public String updateMnSingleEvent(String params);
	public String updateAllMnSubEvent(String params);
	public String updateSubFollowingEvents(String params);
	public String deleteMnSubEvent(String params);

	public String fetchSubEvents(String params);
	public String getSubEvents(String params);
	
	public String getListBasedOnListIdForSchedule(String listId,String params);
	
	public String getNoteByKwywordsForSchedule(String params, MnNoteable mnNoteable);
	public String getEventMailValues();
}
