package com.musicnotes.apis.resources;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.musicnotes.apis.dao.interfaces.IFriendDao;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnUsers;

import com.musicnotes.apis.interfaces.MnFriendable;

public class MnFriend implements MnFriendable
{
	IFriendDao iFriendDao;
	Logger logger = Logger.getLogger(MnFriend.class);

	@Override
	public String addFriendRequest(MnFriends mnUsers)
	{
		if(logger.isDebugEnabled()){
		logger.debug("addFriendRequest method called for userId:"+mnUsers.getUserId());
		}
		String status = iFriendDao.addFriendRequest(mnUsers);
		if(logger.isDebugEnabled())
		logger.debug("addFriendRequest method successfully returned a response for userId:"+mnUsers.getUserId());
		return status;
	}

	@Override
	public MnFriends convertJsonToFriendObj(String jsonStr)
	{
		if(logger.isDebugEnabled())
		logger.debug("convertJsonToFriendObj method called");
		MnFriends friend = new MnFriends();
		JSONObject jsonObject;
		try {
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonObject = new JSONObject(jsonStr);

				friend.setUserId(Integer.parseInt((String) jsonObject.get("userId")));
				friend.setRequestedUserId(Integer.parseInt((String) jsonObject.get("requestId")));
				friend.setStatus("R");
				friend.setRequestedDate(new Date());
			}

		}
		catch (Exception e)
		{
			logger.error("Exception in convertJsonToFriendObj method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("convertJsonToFriendObj method successfully returned");
		return friend;
	}

	public IFriendDao getiFriendDao()
	{
		return iFriendDao;
	}

	public void setiFriendDao(IFriendDao iFriendDao)
	{
		this.iFriendDao = iFriendDao;
	}

	@Override
	public String checkAlreadyFriends(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriends method called for userId:"+mnFriends.getUserId());
		String status = "not friends";
		try
		{
		MnUsers mnUsers=iFriendDao.checkAlreadyFriends(mnFriends);
		if(mnUsers!=null){
				if(mnUsers.getFriends()!=null && !mnUsers.getFriends().isEmpty()){
					if(mnUsers.getFriends().contains(mnFriends.getRequestedUserId())){
						status="friends";
					}
			}
		}
		
		if(!status.equals("friends")){
			List<MnFriends> friends=iFriendDao.checkAlreadyFriendRequested(mnFriends.getUserId(),mnFriends.getRequestedUserId());
			if(friends!=null && !friends.isEmpty()){
						status="reponse waiting";
			}
		}
		
		if(!status.equals("friends")&&!status.equals("reponse waiting")){

			List<MnFriends> friends=iFriendDao.checkAlreadyFriendRequested(mnFriends.getRequestedUserId(),mnFriends.getUserId());
			
			if(friends!=null && !friends.isEmpty()){
						status="request send";
			}
		}
			
		}
		catch (Exception e)
		{
			logger.error("Exception in checkAlreadyFriends method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriends method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	@Override
	public String getFriendRequests(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("getFriendRequests method called for userId:"+mnFriends.getUserId());
		String status = "";
		try
		{
			List<MnFriends> friends = iFriendDao.checkFriendRequest(mnFriends);
			if (friends != null)
			{
				status = convertFriendsToJsonString(friends);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in getFriendRequests method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getFriendRequests method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	@Override
	public String acceptFriendRequest(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("acceptFriendRequest method called for userId:"+mnFriends.getUserId());
		String status = "Fail";

		try
		{
			status = iFriendDao.acceptFriendRequest(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in acceptFriendRequest method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("acceptFriendRequest method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	@Override
	public String declineFriendRequest(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("declineFriendRequest method called for userId:"+mnFriends.getUserId());
		String status = "Fail";

		try
		{
			status = iFriendDao.declineFriendRequest(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in declineFriendRequest method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("declineFriendRequest method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	public String addToFriendsList(Integer userId, Integer requestedId)
	{
		if(logger.isDebugEnabled())
		logger.debug("addToFriendsList method called "+userId+" requestedId: "+requestedId);
		String status = "Fail";

		try
		{
			status = iFriendDao.addToFriendsList(userId, requestedId);

		}
		catch (Exception e)
		{
			logger.error("Exception in addToFriendsList method for userId:"+userId+":"+e);
		}
		logger.debug("addToFriendsList method successfully returned for userId:"+userId);
		return status;
	}
	
	public String checkFollowers(MnFriends mnFriends)
	{
		logger.debug("checkFollowers method called for userId:"+mnFriends.getUserId());
		String status = "notfollower";

		try
		{
			MnUsers mnUsers = iFriendDao.checkAlreadyFriends(mnFriends);

			if (mnUsers != null)
			{
					if (mnUsers.getFollowers() != null && !mnUsers.getFollowers().isEmpty())
					{
						if (mnUsers.getFollowers().contains(mnFriends.getRequestedUserId()))
						{
							return "follower";
						}
				}
			}

		}
		catch (Exception e)
		{
			logger.error("Exception in checkFollowers method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("checkFollowers method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	public String addFollowers(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("addFollowers method called for userId:"+mnFriends.getUserId());
		String status = "Fail";

		try
		{
			status = iFriendDao.addFollowers(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in addFollowers method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("addFollowers method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	public String unFollowers(MnFriends mnFriends)
	{
		logger.debug("unFollowers method called for userId:"+mnFriends.getUserId());
		String status = "Fail";

		try
		{
			MnUsers mnUsers = iFriendDao.checkAlreadyFriends(mnFriends);

			if (mnUsers != null)
			{
					if (mnUsers.getFollowers() != null && !mnUsers.getFollowers().isEmpty())
					{
						if (mnUsers.getFollowers().contains(mnFriends.getRequestedUserId()))
						{
							mnUsers.getFollowers().remove(mnFriends.getRequestedUserId());
							status=iFriendDao.unFollowers(mnUsers,mnFriends);
						}
				}
			}

		}
		catch (Exception e)
		{
			logger.error("Exception in unFollowers method for userId:"+mnFriends.getUserId()+":"+e);
		}
		logger.debug("unFollowers method successfully returned a response for userId:"+mnFriends.getUserId());
		return status;
	}

	public String convertFriendsToJsonString(List<MnFriends> mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("convertFriendsToJsonString method called");
		StringBuffer jsonStr = new StringBuffer("[");
		for (MnFriends friends : mnFriends)
		{
			String name=iFriendDao.getUserDetails(friends.getRequestedUserId());
			
			jsonStr.append("{\"_id\" : \"" + friends.get_id() + "\",");
			jsonStr.append("\"userId\" : \"" + friends.getUserId() + "\",");
			jsonStr.append("\"requestedUserId\" : \"" + friends.getRequestedUserId() + "\",");
			jsonStr.append("\"status\" : \"" + friends.getStatus() + "\",");
			jsonStr.append("\"requestedDate\" : \"" + friends.getRequestedDate() + "\",");
			jsonStr.append("\"acceptedDate\" : \"" + friends.getAcceptedDate() + "\",");
			jsonStr.append("\"name\" : \"" + name + "\",");
			jsonStr.append("\"declineDate\" : \"" + friends.getDeclineDate() + "\"},");
			
			
		}
		if (jsonStr.indexOf(",") != -1) jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())
		logger.debug("convertFriendsToJsonString method successfully returned");
		return jsonStr.toString();
	}

	@Override
	public MnUsers getUsers(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("getUsers method called userId:"+mnFriends.getUserId());
		MnUsers mnUsers=null;
		try{
			 mnUsers=iFriendDao.checkAlreadyFriends(mnFriends);
		}catch(Exception e){
			logger.error("Exception in getUsers method for userId:"+mnFriends.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getUsers method successfully returned a response for userId:"+mnFriends.getUserId());
		return mnUsers;
	}

	@Override
	public List<MnUsers> getFriendsList(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())
		logger.debug("getFriendsList method called userId:"+mnUsers.getUserId());
		List<MnUsers> users=null;
		try{
			users=iFriendDao.getFriendsList(mnUsers);
		}catch(Exception e){
			logger.error("Exception in getFriendsList method for userId:"+mnUsers.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getFriendsList method successfully returned a response for userId:"+mnUsers.getUserId());
		return users;
	}
	//Covert Friend list Object To Json With Image 
	public String convertUserObjectToJsonObject(List<MnUsers> mnUsers,HttpServletRequest request){
		if(logger.isDebugEnabled())
		logger.debug("convertUserObjectToJsonObject method called");
		
		StringBuffer jsonStr = new StringBuffer("[");
		if(mnUsers!=null && !mnUsers.isEmpty()){
			for(MnUsers users: mnUsers){
				jsonStr.append("{\"_id\" : \"" + users.get_id() + "\",");
				if(users.getUserFirstName()!=null && !users.getUserFirstName().isEmpty() ){
					jsonStr.append("\"userName\" : \"" + users.getUserFirstName().trim() +" "+ users.getUserLastName().trim()+"\",");
				}else{
					jsonStr.append("\"userName\" : \"" + users.getUserName().trim()+"\",");
				}
				jsonStr.append("\"userId\" : \"" + users.getUserId() + "\",");
				jsonStr.append("\"filePath\" : \"" + users.getFilePath() + "\",");
				jsonStr.append("\"followers\" : " + users.getFollowers() + "},");
			}
		}
		if (jsonStr.indexOf(",") != -1) jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())
		logger.debug("convertUserObjectToJsonObject method successfully returned");
		return jsonStr.toString();
	}
	//Covert Friend list Object To Json WithOut Image 
	public String convertUserObjectWithOutImageToJsonObject(List<MnUsers> mnUsers){
		if(logger.isDebugEnabled())
		logger.debug("convertUserObjectWithOutImageToJsonObject method called");
		StringBuffer jsonStr = new StringBuffer("[");
		if(mnUsers!=null && !mnUsers.isEmpty()){
			for(MnUsers users: mnUsers){
				
				jsonStr.append("{\"_id\" : \"" + users.get_id() + "\",");
				jsonStr.append("\"userName\" : \"" + users.getUserFirstName() + users.getUserLastName()+"\",");
				jsonStr.append("\"userId\" : \"" + users.getUserId() + "\",");
				jsonStr.append("\"followers\" : " + users.getFollowers() + "},");
			}
		}
		if (jsonStr.indexOf(",") != -1) jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())
		logger.debug("convertUserObjectWithOutImageToJsonObject method successfully returned");
		return jsonStr.toString();
	}
	public String acceptSharedNoteByMail(String params){
		if(logger.isDebugEnabled())
		logger.debug("acceptSharedNoteByMail method called");
		String status="";
		String level="";
		String noteId="";
		String listId="";
		try{
			JSONObject jsonObject = new JSONObject(params);
			level=jsonObject.getString("level");
			noteId=jsonObject.getString("noteId");
			listId=jsonObject.getString("listId");
			status=iFriendDao.acceptDeclineSharedNoteByMail(Integer.parseInt( jsonObject.getString("userId")), jsonObject.getString("status"),Integer.parseInt( jsonObject.getString("friendUserId")),Boolean.parseBoolean( jsonObject.getString("finalflag")),level,noteId,listId);
		}catch (Exception e) {
			logger.error("Exception in acceptSharedNoteByMail method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("acceptSharedNoteByMail method successfully returned");
		return ""+status;
	}
}
