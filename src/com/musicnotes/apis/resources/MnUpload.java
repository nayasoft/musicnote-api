package com.musicnotes.apis.resources;

import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.musicnotes.apis.dao.interfaces.IUploadDao;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;
import com.musicnotes.apis.interfaces.MnUploadable;

public class MnUpload implements MnUploadable
{
	Logger logger = Logger.getLogger(MnUpload.class);

	IUploadDao iUploadDao;

	public IUploadDao getiUploadDao()
	{
		return iUploadDao;
	}

	public void setiUploadDao(IUploadDao iUploadDao)
	{
		this.iUploadDao = iUploadDao;
	}

	@Override
	public void saveVideoFileName(String fileName, String userId, String fileType, String status,String fullPath,double fileSize)
	{
		if(logger.isDebugEnabled()){logger.debug("Save video file name entered fileName:"+fileName+" and userId:"+userId);}
		try
		{
			
			iUploadDao.saveVideoFileName(userId, fileName, fileType, status,fullPath,fileSize);
		}
		catch (Exception e)
		{
			logger.error("Error in Save Video File Name : ", e);
		}
		if(logger.isDebugEnabled()){logger.debug("Video file name saved successfully");}
	}

	@Override
	public String getVideoFileNames(String userId)
	{
		if(logger.isDebugEnabled()){logger.debug("Getting video file names called userId:"+userId);}
		String jsonStr = "";
		List<MnVideoFile> files = null;
		try
		{
			files = iUploadDao.getVideoFileNames(userId);
			if (files != null)
			{
				jsonStr = convertToJson(files);

			}
		}
		catch (Exception e)
		{
			logger.error("Error in Get Video File Names : ", e);
		}
		if(logger.isDebugEnabled()){logger.debug("Getting video file names terminated with json{}");}
		return jsonStr;
	}
	
	@Override
	public String getVideoFileNamesForNote(String userId,String listId, String noteId)
	{
		if(logger.isDebugEnabled()){logger.debug("Getting video file names entered userId:"+userId+" noteId:"+noteId);}
		String jsonStr = "";
		List<MnVideoFile> files = null;
		try
		{
			files = iUploadDao.getVideoFileNamesForNote(userId,listId,noteId);
			if (files != null)
			{
				jsonStr = convertToJson(files);

			}
		}
		catch (Exception e)
		{
			logger.error("Error in Get Video File Names : ", e);
		}
		if(logger.isDebugEnabled()){logger.debug("Getting video file names for note terminated with json{}");}
		return jsonStr;
	}

	private String convertToJson(List<MnVideoFile> vcAudioFile)
	{
		if(logger.isDebugEnabled()){logger.debug("Json conversion method called with MnVideoFile List");}
		StringBuffer jsonStr = new StringBuffer("[");
		for (MnVideoFile videoFile : vcAudioFile)
		{
			jsonStr.append("{ \"_id\" : { \"$oid\" :\"" + videoFile.get_id() + "\"},");
			jsonStr.append("\"userId\" : \"" + videoFile.getUserId() + "\",");
			jsonStr.append("\"fileDummyName\" : \"" + videoFile.getFileDummyName() + "\",");
			jsonStr.append("\"fullPath\" : \"" + videoFile.getFullPath() + "\",");
			jsonStr.append("\"uploadDate\" : \"" +videoFile.getUploadDate() + "\",");
			jsonStr.append("\"fileName\" : \"" + videoFile.getFileName() + "\"},");
		}
		if(jsonStr!=null && jsonStr.indexOf(",")!=-1)
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())logger.debug("returned MnVideoFile list to Json{}");
		return jsonStr.toString();
	}

	@Override
	public String deleteVideoFileNames(String fileName, String userId)
	{
		
		if(logger.isDebugEnabled())logger.debug("delete video file entered userId:"+userId+" fileName:"+fileName);
		boolean flag = false;
		String jsonString = null;
		try
		{
			flag = iUploadDao.deleteVideoFileNames(fileName, userId);
			if(flag)
			{
				jsonString=convertDetailsToJson("success");
			}
			else
			{
				jsonString=convertDetailsToJson("error");
			}
		}
		catch (Exception e)
		{
			logger.error("Error in Delete Video File Names : ", e);
		}
		if(logger.isDebugEnabled())logger.debug(" video file deleted and returned status"+jsonString);
		return jsonString;
	
		
	}
	
	@Override
	public void saveOtherFileName(String fileName, String userId, String fileType, String status,String fullPath,double fileSize)
	{
		try
		{
			if(logger.isDebugEnabled())logger.debug("save other file names userId : "+userId+" fileName:"+fileName);
			iUploadDao.saveOtherFileName(userId, fileName,fileType,status,fullPath,fileSize);
		}
		catch (Exception e)
		{
			logger.error("Error in Save File Name : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Other files saved and terminated");
	}
	
	@Override
	public void saveOtherFileNameAttachWithNote(String fileName, String userId, String fileType, String status,String listId,String noteId,String fullPath,double fileSize)
	{
		if(logger.isDebugEnabled())logger.debug("Save other files and attached with note userId : "+userId+" ListId :"+listId);
		
		try
		{
			iUploadDao.saveOtherFileNameAttachWithNote(userId, fileName,fileType,status, listId, noteId,fullPath,fileSize);
		}
		catch (Exception e)
		{
			logger.error("Error in Save File Name : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Other files within note saved and terminated");
	}
	
	@Override
	public String getOtherFileNames(String userId)
	{
		if(logger.isDebugEnabled())logger.debug("get other file names userId"+userId);
		String jsonStr = "";
		List<MnVideoFile> files = null;
		try
		{
			files = iUploadDao.getOtherFileNames(userId);
			if (files != null)
			{
				jsonStr = convertToJson(files);

			}
		}
		catch (Exception e)
		{
			logger.error("Error in Get other File Names : ", e);
		}
		if(logger.isDebugEnabled())logger.info("get other file terminated with jsonStr {} ");
		return jsonStr;
	}
	
	@Override
	public String deleteOtherFileNames(String fileName, String userId)
	{
		if(logger.isDebugEnabled())logger.debug("Delete other file names called "+userId+" fileName: "+fileName);
		boolean flag = false;
		String jsonString = null;
		try
		{
			flag = iUploadDao.deleteOtherFileNames(fileName, userId);
			if(flag)
			{
				jsonString=convertDetailsToJson("success");
			}
			else
			{
				jsonString=convertDetailsToJson("error");
			}
		}
		catch (Exception e)
		{
			logger.error("Error in Delete File Names : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("File has deleted and returned json{}"+jsonString);
		return jsonString;
	}
	
	@Override
	public void commonLog(String fileName, String userId,String label){
		if(logger.isDebugEnabled())logger.debug("Common log has entered userId: "+userId+" fileName: "+fileName);
		try{
			iUploadDao.commonLog(fileName, userId,label);
		}catch (Exception e) {
			logger.error("Error in download file log : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Common log successfully writted ");
	}

	private String convertDetailsToJson(String success) {
		if(logger.isDebugEnabled())logger.debug("Convert details to json object"+success);
		StringBuffer jsonStr = new StringBuffer("[");
		jsonStr.append("{\"status\" : \"" + success + "\"},");
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())logger.debug("Convert details to json object terminated and return : "+jsonStr);
		return jsonStr.toString();
	}
	public String deleteUploadFileNamesForNote(String params,String filename ,String userId){
		if(logger.isDebugEnabled())logger.debug("delete upload file names for note userId :"+userId+"fileName : "+filename);
		String statusFlag = null;
		String listId=null;
		String noteId = null;
		try{
			JSONObject jsonObject = new JSONObject(params);
			listId = (String) jsonObject.get("listId");
			noteId = (String) jsonObject.get("noteId");
			if(listId != null &&  noteId!= null)
				statusFlag = iUploadDao.deleteUploadFileNamesForNote(listId, noteId, filename, userId);
			
			if(statusFlag!= null )
				statusFlag = convertDetailsToJson(statusFlag);
			
		}catch (Exception e) {
			logger.error(e);
		}
		if(logger.isDebugEnabled())logger.debug("file has deleted from note and retuned status");
		return statusFlag;
	}

	@Override
	public void saveOtherFileNameAttachWithNoteAudio(String fileName,
			String userId, String fileType, String status, String listId,
			String noteId,String fullPath,double fileSize) {
		if(logger.isDebugEnabled())logger.debug("Save other files and attached with note audios userId : "+userId+"fileName : "+fileName);
		try
		{
			iUploadDao.saveOtherFileNameAttachWithNoteAudio(userId, fileName,fileType,status, listId, noteId,fullPath,fileSize);
		}
		catch (Exception e)
		{
			logger.error("Error in Save File Name : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("File saved and attached to Note");
	}

	@Override
	public String getAllFiles(String fileName, Integer userId) {
		if(logger.isDebugEnabled())logger.debug("Geeting All files userId : "+userId+" fileName: "+fileName);
		String fullpath=null;
		try
		{
			fullpath=iUploadDao.getAllFiles(fileName,userId);
		}
		catch (Exception e)
		{
			logger.error("Error in Save Video File Name : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Getting all files and returned file path");
		return fullpath;
		
	}

	@Override
	public MnUsers limiteSize(String userId) {
		if(logger.isDebugEnabled())logger.debug("Limited Size upload: "+userId);
		MnUsers mnUser=null;
		try
		{
			mnUser=iUploadDao.limitedUpload(userId);	
		}
		catch (Exception e)
		{
			logger.error("Error in Limited Size upload : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Limited Size upload");
		return mnUser;
		
	}


}
