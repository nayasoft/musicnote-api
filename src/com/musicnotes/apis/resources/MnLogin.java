package com.musicnotes.apis.resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.musicnotes.apis.dao.interfaces.ILoginDao;
import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.interfaces.MnLoginable;
import com.musicnotes.apis.tokens.TokenGenerator;

public class MnLogin implements MnLoginable
{
	ILoginDao iLoginDao;
	Logger logger = Logger.getLogger(MnLogin.class);

	public String getUser(String userData, HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("getUser method entered "+userData);

		String flag = "";

		try
		{
			flag = iLoginDao.CheckLogin(userData);
		}
		catch (Exception e)
		{
			logger.error("error while getUser Method called:" + e);
		}
		if(logger.isDebugEnabled())logger.debug("returned getUser method response for "+userData);
		return flag;
	}

	public ILoginDao getiLoginDao()
	{
		return iLoginDao;
	}

	public void setiLoginDao(ILoginDao iLoginDao)
	{
		this.iLoginDao = iLoginDao;
	}
	
	@Override
	public MnUsers convertJsonToUserRObj(String jsonStr){
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserDetailObj has entered");

		MnUsers mnUsers = new MnUsers();
		JSONObject jsonObject;
		try {
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonObject = new JSONObject(jsonStr);
				mnUsers.setEmailId((String)jsonObject.get("emailId"));
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserDetailObj has terminated and returned MnUserLog Obj");

		return mnUsers;
	
		
	}
	@Override
	public MnUsers convertJsonToUserObj(String jsonStr)
	{
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserObj has entered");

		MnUsers user = new MnUsers();
		JSONObject jsonObject;
		try
		{
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonObject = new JSONObject(jsonStr);
				if (jsonObject.has("firstName")) {
					user.setUserFirstName((String)jsonObject.get("firstName"));
				}
				if (jsonObject.has("lastName")) {
					user.setUserLastName((String)jsonObject.get("lastName"));
				}
				if (jsonObject.has("userName")) {
					user.setUserName((String)jsonObject.get("userName"));
				}
				if (jsonObject.has("roleId")) {
					user.setUserRole((String)jsonObject.get("roleId"));
				}
				if (jsonObject.has("password")) {
					user.setPasswordText((String)jsonObject.get("password"));
				}
				if (jsonObject.has("loginId")) {
					user.setEmailId((String)jsonObject.get("loginId"));
				}
				if (jsonObject.has("id")) {
					user.setMailCheckId((String)jsonObject.get("id"));
				}
				if (jsonObject.has("facebookID")) {
					user.setFacebookId((String)jsonObject.get("facebookID"));
				}
				if (jsonObject.has("facebookEmailId")) {
					user.setEmailId((String)jsonObject.get("facebookEmailId"));
				}
				/*jsonStr = jsonStr.replace("{", "");
				jsonStr = jsonStr.replace("}", "");
				jsonStr = jsonStr.replace("\"", "");
				String[] params = jsonStr.split(",");
				for (String param : params)
				{
					String[] values = param.split(":");
					if (param.contains("firstName"))
						user.setUserFirstName(values[1]);
					else if (param.contains("lastName"))
						user.setUserLastName(values[1]);
					else if (param.contains("userName"))
						user.setUserName(values[1]);
					else if (param.contains("roleId"))
						user.setUserRole(values[1]);
					else if (param.contains("password"))
						user.setPasswordText(values[1]);
					else if (param.contains("loginId"))
						user.setEmailId(values[1]);
					else if (param.contains("id"))
						user.setMailCheckId(values[1]);
					else if (param.contains("facebookID"))
						user.setFacebookId(values[1]);
					else if (param.contains("facebookEmailId"))
						user.setEmailId(values[1]);
				}*/
				
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while converting json to user object !", e);

		}
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserObj has terminated and returned with MnUsers Obj");

		return user;
	}

	
	public String getLogin(String userData, String password,HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("getLogin method has entered "+userData);
		String userLogin="";
		
		try{
			userLogin=iLoginDao.getLogin(userData, password,request);	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while login used mn application for "+userData+":"+e);
		}
		if(logger.isDebugEnabled())logger.debug("getLogin has terminated and returned with userdetails"+userLogin);

		return userLogin;
	}

	@Override
	public String unsubcribeNotification(MnUsers mnUsers)
	{
		String userLogin="";
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has entered for "+mnUsers.getUserName());

		try{
			userLogin=iLoginDao.unsubcribeNotification(mnUsers);;	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while unsubcribeNotification method called for "+mnUsers.getUserName()+":"+e);
		}
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has returned success messages for "+mnUsers.getUserName());

		return userLogin;
	}

	@Override
	public String updateLoginCheck(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("updateLoginCheck has entered");

		String userLogin="";
		
		try{
			userLogin=iLoginDao.updateLoginCheck(mnUsers);;	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while updateLoginCheck method called for"+e);
		}
		if(logger.isDebugEnabled())logger.debug("updateLoginCheck has terminated with MN user/non MN user");

		return userLogin;
	}
	@Override
	public String updateLoginCheckViaFBLogin(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("updateLoginCheckViaFBLogin has entered");

		String userLogin="";
		
		try{
			userLogin=iLoginDao.updateLoginCheckViaFBLogin(mnUsers);;	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while updateLoginCheckViaFBLogin method called for"+e);
		}
		if(logger.isDebugEnabled())logger.debug("updateLoginCheckViaFBLogin has terminated with MN user/non MN user");

		return userLogin;
	}
	
	@Override
	public String resetVerificationCode(MnUsers user)
	{
		if(logger.isDebugEnabled())logger.debug("resetVerificationCode has entered for mailId: "+user.getEmailId());

		String status="";
		
		try{
			status=iLoginDao.resetVerificationCode(user);;	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while resetVerificationCode method called"+e);
		}
		if(logger.isDebugEnabled())logger.debug("resetVerificationCode has terminated with MN user/non MN user");

		return status;
	}
	
	
	@Override
	public MnUserLogs convertJsonToUserDetailObj(String jsonStr)
	{
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserDetailObj has entered");

		MnUserLogs mnUserLogs = new MnUserLogs();
		JSONObject jsonObject;
		try {
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonObject = new JSONObject(jsonStr);
				mnUserLogs.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
				mnUserLogs.setVersion((String)jsonObject.get("versions"));
				mnUserLogs.setOsName((String)jsonObject.get("osName"));
				mnUserLogs.setBrowserNameVersion((String)jsonObject.get("browserNameAndVersion"));
				mnUserLogs.setScreenSize((String)jsonObject.get("screenSize"));
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date today = Calendar.getInstance().getTime();        
				String reportDate = df.format(today);
				mnUserLogs.setLoginTime(reportDate);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}
		if(logger.isDebugEnabled())logger.debug("convertJsonToUserDetailObj has terminated and returned MnUserLog Obj");

		return mnUserLogs;
	}

	@Override
	public String doUserLoginDetails(MnUserLogs mnUserLogs)
	{
		if(logger.isDebugEnabled())logger.debug("doUserLoginDetails has entered");

		String userLogin="";
		try{
			userLogin=iLoginDao.doUserLoginDetails(mnUserLogs);	
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while doUserLoginDetails method called "+e);
		}
		if(logger.isDebugEnabled())logger.debug("doUserLoginDetails has terminated and returned usersUpdate details"+userLogin);

		return userLogin;
	}

	@Override
	public String loginAuthentication(String param, HttpServletRequest request) {
		String userId = "0";
		String inTime = "";
		String role = "";
		if(logger.isDebugEnabled())logger.debug("loginAuthentication has entered");

		try
		{
			if (param != null)
			{
				param=param.replaceAll("id=", "");
				param=param.replaceAll("=", "");
				String[] pass = param.toString().split("&");
				inTime = pass[0];
				userId = pass[1];
				role = pass[3];
			}
			userId = iLoginDao.loginAuthentication(userId, inTime, role);
		}catch(Exception e){
			logger.error("loginAuthentication Method  :"+e);
		}
		if(logger.isDebugEnabled())logger.debug("loginAuthentication has terminated and returned with userId : "+userId);

		return userId;
	}
	
	public MnUsersToken convertJsonToTokenObj(String jsonStr){
		MnUsersToken mnUsersToken=new MnUsersToken();
		JSONObject jsonObject;
		try {
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonObject = new JSONObject(jsonStr);
				mnUsersToken.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
				mnUsersToken.setLoginTime((String)jsonObject.get("versions"));
			}
			}catch (Exception e) {
			}
		return mnUsersToken;
	}

	@Override
	public String toGenerateTokens(MnUsersToken mnUsersToken) {
		if(logger.isDebugEnabled())logger.debug("toGenerateTokens has entered for "+mnUsersToken.getUserId());

		String token=null;
		try{
			JSONObject arbitraryPayload = new JSONObject();
			try {
			    arbitraryPayload.put("some", "arbitrary");
			    arbitraryPayload.put("data", "here");
			} catch (JSONException e) {
			    e.printStackTrace();
			}  
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date today = Calendar.getInstance().getTime();        
			String date = df.format(today);
			
			TokenGenerator tokenGenerator=new  TokenGenerator(mnUsersToken.getUserId()+""+mnUsersToken+""+date);
			String generatedToken = tokenGenerator.createToken(arbitraryPayload);
			logger.debug("token token"+generatedToken);
			
			mnUsersToken.setTokens(generatedToken);
			mnUsersToken.setCreatedDateTime(date);
			
			String status=iLoginDao.toGenerateTokens(mnUsersToken);	
			if(status.equals("success"))
				token=generatedToken;
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("error while toGenerateTokens method called for "+mnUsersToken.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())logger.debug("toGenerateTokens has terminated and returned user "+mnUsersToken.getUserId()+" token"+token);

		return token;
	}
	public String getFaceBookUser(String userName,String emailId,String facebookId, HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("getFaceBookUser method entered "+userName);

		String flag = "";

		try
		{
			flag = iLoginDao.checkFaceBookLogin(userName,emailId,facebookId,request);
		}
		catch (Exception e)
		{
			logger.error("error while getFaceBookUser Method called:" + e);
		}
		if(logger.isDebugEnabled())logger.debug("returned getFaceBookUser method response for "+userName);
		return flag;
	}
	
}
