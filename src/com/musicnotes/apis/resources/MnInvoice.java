package com.musicnotes.apis.resources;

import org.apache.log4j.Logger;

import com.musicnotes.apis.dao.interfaces.InvoiceDao;
import com.musicnotes.apis.domain.MnInvoices;
import com.musicnotes.apis.interfaces.MnInvoiceable;

public class MnInvoice implements MnInvoiceable {

	Logger logger = Logger.getLogger(MnInvoice.class);

	InvoiceDao invoiceDao;
	@Override
	public String creatInvoice(MnInvoices mnInvoice) {
		String status = "";
		try
		{
			status = invoiceDao.createUser(mnInvoice);
		}
		catch (Exception e)
		{
			logger.error("Exception in insertNewInvoice method  : ", e);

		}
		return status;
	}

	@Override
	public MnInvoices convertJsonToUserObj(String jsonStr) {
		MnInvoices invoices = new MnInvoices();
		try
		{
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonStr = jsonStr.replace("{", "");
				jsonStr = jsonStr.replace("}", "");
				jsonStr = jsonStr.replace("\"", "");
				String params[] = jsonStr.split(",");

				for (String param : params)
				{
					String[] values = param.split(":");
					if (param.contains("classId"))
						invoices.setClassId(values[1]);
					else if (param.contains("totalQty"))
						invoices.setTotalQty(values[1]);
					else if (param.contains("subTotal"))
						invoices.setSubTotal(values[1]);
					else if (param.contains("totalAmount"))
						invoices.setTotalAmount(values[1]);
					else if (param.contains("unitPrice"))
						invoices.setUnitPrice(values[1]);
					
				}
			
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while converting json to invoice object !", e);

		}
		return invoices;
	}

	public InvoiceDao getInvoiceDao() {
		return invoiceDao;
	}

	public void setInvoiceDao(InvoiceDao invoiceDao) {
		this.invoiceDao = invoiceDao;
	}

}
