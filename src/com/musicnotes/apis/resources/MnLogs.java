package com.musicnotes.apis.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.musicnotes.apis.dao.interfaces.ILogDao;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.interfaces.MnLogable;
import com.musicnotes.apis.interfaces.MnNoteable;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

public class MnLogs implements MnLogable{
	
	ILogDao iLogDao;
	MnNoteable mnNoteable;
	public ILogDao getiLogDao() {
		return iLogDao;
	}
	public void setiLogDao(ILogDao iLogDao) {
		this.iLogDao = iLogDao;
	}
	
	Logger logger = Logger.getLogger(MnLogs.class);
	
	public String getNotificationsList(String userId){
		
		String jsonString = null;
		
		try{
			if(logger.isDebugEnabled()){logger.debug("getNotificationsList method called "+userId);}
			List<MnLog> mnLogList = iLogDao.getNotificationsList(userId);
			if (mnLogList != null && !mnLogList.isEmpty())
			{
				Collections.sort(mnLogList, SENIORITY_ORDER);
				jsonString = convertToJsonListOfList(mnLogList);


			}else{
				mnLogList = iLogDao.getInactiveNotificationsList(userId);
				if(mnLogList!= null &&!mnLogList.isEmpty())
				{
					Collections.sort(mnLogList, SENIORITY_ORDER);
					if(mnLogList.size() > 5){
						mnLogList = mnLogList.subList(0, 5);
					}
					jsonString = convertToJsonListOfList(mnLogList);
				}
			}
		}catch (Exception e) {
			logger.error("Exception in fetching inactive notification list"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Successfully got inactive notification list for"+userId);}
		return jsonString;
	}
	static final Comparator<MnLog> SENIORITY_ORDER = new Comparator<MnLog>()
	{
		public int compare(MnLog e1, MnLog e2)
		{
			try
			{
				Integer i1 = e1.getLogId();
				Integer i2 = e2.getLogId();
				return i2.compareTo(i1);

			}
			catch (Exception e)
			{
				return e2.getLogId().compareTo(e1.getLogId());
			}
		}
	};
	private String convertToJsonListOfList(List<MnLog> mnLogList) {

		StringBuffer jsonStr = new StringBuffer("[");
		if(logger.isDebugEnabled()){logger.debug("convertToJsonListOfList method called");}
		try{
		for (MnLog log : mnLogList)
		{
			jsonStr.append("{\"logId\" : \"" + log.getLogId() + "\",");
			jsonStr.append("\"listId\" : \"" + log.getListId() + "\",");
			jsonStr.append("\"userId\" : \"" + log.getUserId() + "\",");
			jsonStr.append("\"noteId\" : \"" + log.getNoteId() + "\",");
			jsonStr.append("\"logDescription\" : \"" + log.getLogDescription() + "\",");
			jsonStr.append("\"logType\" : \"" + log.getLogType() + "\",");
			jsonStr.append("\"date\" : \"" + log.getDate() + "\",");
			jsonStr.append("\"time\" : \"" + log.getTime() + "\",");
			jsonStr.append("\"notUserId\" : \"" + log.getNotUserId() + "\",");
			jsonStr.append("\"status\" : \"" + log.getStatus() + "\",");
			jsonStr.append("\"pageName\" : \"" + log.getPageName()+ "\",");
			jsonStr.append("\"pageType\" :\"" + log.getPageType() + "\"},");
		}
		
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled()){logger.debug("converted from json to list");}
		}catch(Exception e){
		logger.error("Exception while converting json to list"+e);	
		}
		return jsonStr.toString();
	}
	
	public String inactivateNotifications(String params,String userId ){
		String status="";
		try{
			if(logger.isDebugEnabled()){logger.debug("inactivateNotifications method called "+userId);}
			String[] notificationArray =  params.split(",");
			List<Integer> notifiationList = new ArrayList<Integer>();
			for(String string : notificationArray){
				notifiationList.add(Integer.parseInt(string.trim()));
			}
			if(notifiationList!= null && !notifiationList.isEmpty()){
				status = iLogDao.inactivateNotifications(notifiationList, userId);
			}
		}catch (Exception e)
		{
			logger.error("Exception in fetching inactiveNotifications"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Got inactivateNotifications for"+userId);}
		return status;
	}
	
	public String getRecentActitvity(String userId){
		String jsonString="";
		try{
			if(logger.isDebugEnabled()){logger.debug("getRecentActivity method called "+userId);}
			List<MnLog> mnLogList = iLogDao.getRecentActitvity(userId);
			if (mnLogList != null && !mnLogList.isEmpty())
			{
				Collections.sort(mnLogList, SENIORITY_ORDER);
				//System.err.println("list size "+mnLogList.size());
				if(mnLogList.size() > 12){
					mnLogList = mnLogList.subList(0, 12);
				}
				
				jsonString = convertToJsonListOfList(mnLogList);
				if(logger.isDebugEnabled()){logger.debug("Got Recent Activity for"+userId);}
			}
		}catch (Exception e) {
			logger.error("Exception while fetching recent activity for "+userId+":"+e);
		}
		return jsonString;
	}
	
	public String getRemainder(String userId, HttpServletRequest request){
		String jsonString="";
		try{
			if(logger.isDebugEnabled()){logger.debug("getRemainder method called "+userId);}
			mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
			
			List<MnRemainders> mnRemainders = iLogDao.getRemainder(userId);
			
			if(mnRemainders!= null && !mnRemainders.isEmpty()){
				
				Collections.sort(mnRemainders, SENIORITY_ORDER_REMAIND);
				
				jsonString = mnNoteable.convertMnRamindObjectToJson(mnRemainders,Integer.parseInt(userId));
				
			}
		}catch (Exception e) {
			logger.error("Exception while getting remainder"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Got Remainder for "+userId);}
		return jsonString;
	}
	static final Comparator<MnRemainders> SENIORITY_ORDER_REMAIND = new Comparator<MnRemainders>()
	{
		public int compare(MnRemainders e1, MnRemainders e2)
		{
			try
			{
				Integer i1 = e1.getrId();
				Integer i2 = e2.getrId();
				return i2.compareTo(i1);

			}
			catch (Exception e)
			{
				return e2.getrId().compareTo(e1.getrId());
			}
		}
	};
	
	public String inactiveRemainders(String params,String userId ){
		String status="";
		try{
			if(logger.isDebugEnabled()){logger.debug("inactiveRemainders method called "+userId);}
			String[] remainderArray =  params.split(",");
			List<Integer> remainderList = new ArrayList<Integer>();
			for(String string : remainderArray){
				remainderList.add(Integer.parseInt(string.trim()));
			}
			if(remainderList!= null && !remainderList.isEmpty()){
				status = iLogDao.inactivateRemainders(remainderList, userId);
			}
		}catch (Exception e)
		{
			logger.error("Exception while fetching inactive remainders for "+userId+":"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Got inactive remainders for"+userId);}
		return status;
	}
	@Override
	public String setNotificationsAD(String params) {
		String status="";
			try{
			if(logger.isDebugEnabled()){logger.debug("setNotificationsAD method called");}	
			JSONObject jsonObject=new JSONObject(params);
			String userId=jsonObject.getString("userId");
			String noteId=jsonObject.getString("noteId");
			String listId=jsonObject.getString("listId");
			String logID=jsonObject.getString("logID");
			status=iLogDao.setNotificationsAD(userId,noteId,listId,logID);
			if(logger.isDebugEnabled()){logger.debug("set Notification Accept/Decline for"+userId);}
			}catch(Exception e){
				logger.error("Exception while setting notification accept/decline"+e);
			}
		return status;
	}
}
