package com.musicnotes.apis.resources;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.musicnotes.apis.dao.impl.BaseDaoImpl;
import com.musicnotes.apis.dao.interfaces.IPaymentDao;
import com.musicnotes.apis.domain.MnSubscription;
import com.musicnotes.apis.interfaces.MnGroupable;
import com.musicnotes.apis.interfaces.MnPaymentable;

public class MnPayment extends BaseDaoImpl implements MnPaymentable{

	IPaymentDao iPaymentDao;
	MnGroupable iMnGroup;
	
	public MnGroupable getiMnGroup() {
		return iMnGroup;
	}

	public void setiMnGroup(MnGroupable iMnGroup) {
		this.iMnGroup = iMnGroup;
	}

	
	static Logger logger = Logger.getLogger(MnPayment.class);

	public IPaymentDao getiPaymentDao() {
		return iPaymentDao;
	}



	public void setiPaymentDao(IPaymentDao iPaymentDao) {
		this.iPaymentDao = iPaymentDao;
	}



	public String makePayment(String param) 
	{
		String status="";
		try
		{
		if(logger.isDebugEnabled()){logger.info("makePayment method called");}	
		MnSubscription subscription=new MnSubscription();
		JSONObject jsonstr=new JSONObject(param);
		String userId=(String) jsonstr.get("userId");
		subscription.setUserId(Integer.parseInt(userId));
		subscription.setSubType((String) jsonstr.get("type"));
		subscription.setTotAmount((String) jsonstr.get("amount"));
		String offer=(String) jsonstr.get("offer");
		String paidFor=(String)jsonstr.get("paidNo");
		subscription.setPaidFor(paidFor);
		status=iPaymentDao.makePayment(subscription,userId,userId);	
		if(Integer.parseInt(paidFor)>1)
		{
			status=iMnGroup.createDefaultGroup(Integer.parseInt(userId));
		}
		if(logger.isDebugEnabled()){logger.info("Successfully made payment");}
		}catch (Exception e) {
			logger.error("Exception in making payment"+e.getMessage());
		}
		return status;
	}

	public String updatePayNo(String param) {
		String status="";
		try{
		if(logger.isDebugEnabled()){logger.info("updatePayNo method called");}
		JSONObject jsonstr=new JSONObject(param);
		String userId=jsonstr.getString("userId");
		String paidNo=jsonstr.getString("paidNo");
		status=iPaymentDao.updatePaidNo(userId,paidNo);
		if(logger.isDebugEnabled()){logger.info("Updated Payment number");}
		}catch(Exception e){
			logger.error("Exception while updating paid no for premium users");
		}
		return status;
	}

	public String renewalPayment(String param) {
		String status="";
		try
		{
		if(logger.isDebugEnabled()){logger.info("renewalpayment method called");}	
		JSONObject jsonstr=new JSONObject(param);
		String userId=(String) jsonstr.get("userId");
		String paidUserId=(String) jsonstr.get("paidUser");
		String type=(String) jsonstr.get("type");
		String amount=(String) jsonstr.get("amount");
		String offer=(String) jsonstr.get("offer");
		String paidFor=(String)jsonstr.get("paidNo");
		//status=iPaymentDao.renewalPayment(Integer.parseInt(userId),paidUserId,type,amount,offer,paidFor);	
		if(logger.isDebugEnabled()){logger.info("renewed payment");}	
		}catch (Exception e) {
			logger.error("Exception while renewing payment"+e.getMessage());
		}
		return status;
	}

	// method is used to make renewal or payment from paypal response
	public String makePaymentFromRest(String custom, String txnNo, String txnType, String subscr_id, String amount, String domain) {
		String status = "";
		try {
			if(logger.isDebugEnabled()){logger.info("makePaymentFromRest method called");}
			MnSubscription subscription=new MnSubscription();
			String[] values = custom.split("~");
			String transUserId = values[0];
			String payUserId = values[1];
			String quantity=values[2];
			String type = values[3];
			String payOrRenewal=values[4];
			subscription.setUserId(Integer.parseInt(transUserId));
			subscription.setTxnNo(txnNo);
			subscription.setSubType(type);
			subscription.setTxnType(txnType);
			subscription.setSubscrId(subscr_id);
			subscription.setDomain(domain);
			subscription.setTotAmount(amount);
			subscription.setPaidFor(quantity);
			status=iPaymentDao.makePayment(subscription,payUserId,payOrRenewal);
			if(Integer.parseInt(quantity)>1)
			{
				if(payOrRenewal.equalsIgnoreCase("Payment"))
				status=iMnGroup.createDefaultGroup(Integer.parseInt(transUserId));
			}
			if(logger.isDebugEnabled()){logger.info("Successfully made payment from paypal");}
		} catch (Exception e) {
			logger.error("Exception in making payment from paypal"+e.getMessage());
		}
		return status;
	}



	public String getOfferMessageForPayment(String param) {
		String status="";
		try{
			if(logger.isDebugEnabled()){logger.info("getOfferMessageForpayment method called");}
			JSONObject jsonObject=new JSONObject(param);
			String userId=jsonObject.getString("userId");
			status=iPaymentDao.getOfferMessageForPayment(userId);
			if(logger.isDebugEnabled()){logger.info("Successfully got offer messsage for payment");}
		}catch(Exception e){
			logger.error("Exception while fetching offer message");
		}
		return status;
	}
		
	

	
}
