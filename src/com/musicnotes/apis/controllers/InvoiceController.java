package com.musicnotes.apis.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.domain.MnInvoices;
import com.musicnotes.apis.interfaces.MnInvoiceable;
import com.musicnotes.apis.resources.MnInvoice;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/invoice")
public class InvoiceController extends BaseController{

	MnInvoiceable mnInvoice;
	Logger logger = Logger.getLogger(UserController.class);

	
	@RequestMapping(value = "/createInvoice", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createUser(@RequestBody String jsonStr, HttpServletRequest request)
	{
		mnInvoice = (MnInvoice) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNINVOICE);
		String status = "";
		try
		{
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnInvoices invoices=mnInvoice.convertJsonToUserObj(jsonStr);
            status=mnInvoice.creatInvoice(invoices);
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Invoice : ", e);
			status = "Exception while creating Invoice";
		}
		return status;
	}
}
