package com.musicnotes.apis.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnFriendable;
import com.musicnotes.apis.resources.MnFriend;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/friends")
public class FriendsController extends BaseController
{
	MnFriendable mnFriend;
	Logger logger = Logger.getLogger(FriendsController.class);

	@RequestMapping(value = "/addfriend", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String addFriendRequest(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("addFriendRequest method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.addFriendRequest(mnFriends);
		}
		catch (Exception e)
		{
			logger.error("Exception in addFriendRequest method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("addFriendRequest method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/checkfriends", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkFriends(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("checkFriends method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.checkAlreadyFriends(mnFriends);
		}
		catch (Exception e)
		{
			logger.error("Exception in checkFriends method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("checkFriends method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/requestedfriends", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String requestedFriends(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("requestedFriends method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.getFriendRequests(mnFriends);
		}
		catch (Exception e)
		{
			logger.error("Exception in requestedFriends method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("requestedFriends method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/acceptFriendRequest", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String acceptFriendRequest(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("acceptFriendRequest method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.acceptFriendRequest(mnFriends);

			if (status.equals("success"))
			{
				mnFriend.addToFriendsList(mnFriends.getUserId(), mnFriends.getRequestedUserId());
				mnFriend.addToFriendsList(mnFriends.getRequestedUserId(), mnFriends.getUserId());
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in acceptFriendRequest method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("acceptFriendRequest method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/declineFriendRequest", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String declineFriendRequest(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("declineFriendRequest method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.declineFriendRequest(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in declineFriendRequest method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("declineFriendRequest method successfully returned");
		}
		return status;
	}
	
	@RequestMapping(value = "/checkfollowers", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkFollowers(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("checkFollowers method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.checkFollowers(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in checkFollowers method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("checkFollowers method successfully returned");
		}
		return status;
	}
	
	@RequestMapping(value = "/addfollower", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String addFollower(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("addfollower method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.addFollowers(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in addfollower method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("addfollower method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/unfollower", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String unFollower(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("unFollower method called");
		}
		String status = "";
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			status = mnFriend.unFollowers(mnFriends);

		}
		catch (Exception e)
		{
			logger.error("Exception in unFollower method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("unFollower method successfully returned");
		}
		return status;
	}

	@RequestMapping(value = "/getFriendsList", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getFriendsList(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled()){
		logger.info("getFriendsList method called");
		}
		String status="";
		List<MnUsers> usersList=null;
		try
		{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnFriends mnFriends = mnFriend.convertJsonToFriendObj(params);
			MnUsers mnUsers= mnFriend.getUsers(mnFriends);
			usersList= mnFriend.getFriendsList(mnUsers);
			status=mnFriend.convertUserObjectToJsonObject(usersList,request);
		}
		catch (Exception e)
		{
			logger.error("Exception in getFriendsList method:"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("getFriendsList method successfully returned");
		}
		return status;
	}
	@RequestMapping(value = "/mailSharingAceept", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String acceptSharedNotesByMail(@RequestBody String params, HttpServletRequest request ){
		if(logger.isInfoEnabled()){
		logger.info("acceptSharedNotesByMail method called");
		}
		String status ="";
		try{
			mnFriend = (MnFriend) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFRIEND);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status=mnFriend.acceptSharedNoteByMail(params);
		}catch (Exception e) {
			logger.error("Exception in acceptSharedNotesByMail method"+e);
		}
		if(logger.isInfoEnabled()){
		logger.info("acceptSharedNotesByMail method successfully returned");
		}
		return status;
	}
}
