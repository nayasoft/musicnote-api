package com.musicnotes.apis.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVoteViewCount;
import com.musicnotes.apis.interfaces.MnEventable;
import com.musicnotes.apis.interfaces.MnNoteable;
import com.musicnotes.apis.resources.MnEvents;
import com.musicnotes.apis.resources.MnNote;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;
import com.musicnotes.apis.util.SpringBeans;
/**
 * This class acts as Notes Controller for All Which consumes request and produces
 * response
 * 
 * @author - Venu 
 */

@Controller
@RequestMapping("/note")
public class NoteController  extends BaseController{
	MnNoteable mnNoteable;
	MnEventable mnEventable;
	Logger logger = Logger.getLogger(NoteController.class);

	/* Create Book */
	@RequestMapping(value = "/createList/{listIdFromList}/{noteIdFromList}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createList(@PathVariable("listIdFromList") String listId,@PathVariable("noteIdFromList") String noteId,@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info(" CreateList method called--ListId is:"+listId+"noteId is:"+noteId);
		
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			MnList mnList = mnNoteable.convertJsonToMnListObj(params);
		    status = mnNoteable.createList(mnList,listId,noteId);
		    
			if(status!=null && !status.equalsIgnoreCase("0")){
				if(logger.isInfoEnabled())
				logger.info(" CreateList method returned response successfully");
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating list : "+ e.getMessage());
			status = "0";
		}
		return status;
	}
	
	
	
	/* Create Book */
	@RequestMapping(value = "/createBook", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createBook(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" createBook method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			MnList mnList = mnNoteable.convertJsonToMnListObj(params);
		    status = mnNoteable.createBook(mnList);
		    
			if(status!=null && !status.equalsIgnoreCase("0")){
				if(logger.isInfoEnabled())
				logger.info(" createBook method returned response :");
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Book : ", e);
			status = "0";
		}
		return status;
	}
	/* Fetching All Book */
	@RequestMapping(value = "/fetchList", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchList(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status = "";
		
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchList method called ");
			
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			JSONObject jsonObject = new JSONObject(params);
			String listType=(String) jsonObject.get("listType");
			boolean eventFlag = jsonObject.getBoolean("fetchEvent");
			
			if(!eventFlag && listType.equals("schedule")){
				status = mnNoteable.fetchList(params); // this is used for fetch events from mnList table while passing event flag is false
			}else if(eventFlag && listType.equals("schedule")){
				status = mnEventable.fetchEvents(params); // this is used for fetch events from mnEvents table while passing event flag is true for showing date&time order.
			}else{
				status = mnNoteable.fetchList(params);
			}
			if(logger.isInfoEnabled())
			logger.info(" fetchList method returned response successfully");
		}
		catch (Exception e)
		{
			logger.error("Exception while fetching Book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Schedule Note Event Access Decline */
	@RequestMapping(value = "/calendarEventAccess", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String calendarEventAccess(@RequestBody String params, HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" calendarEventAccess method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status=mnEventable.eventAccesDecline(params);
			if(logger.isInfoEnabled())
			logger.info(" calendarEventAccess method returned response successfully");
		}
		catch (Exception e)
		{
			logger.error("Exception in calendarEventAccess NoteController: "+ e.getMessage());
			status = "0";
		}
		return status;
	}
	
	/* Fetching Notes For Particular Tag */
	@RequestMapping(value = "/fetchTaggedNote", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchTagBasedNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchTaggedNote method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.fetchTaggedBasedNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchTaggedNote method returned response successfully ");
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchTaggedNote NoteController: "+ e.getMessage());
			status = "0";
		}
		return status;
	}
	
	/* Fetching Due date For Particular Note */
	@RequestMapping(value = "/fetchDueDateNote", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchDueDateBasedNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchDueDateNote method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.fetchDueDateBasedNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchDueDateNote method returned response successfully");
		}
		catch (Exception e)
		{
			logger.error("Exception while fetching DueDate based note on Book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Create Note */
	@RequestMapping(value = "/createNote/{listId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createNote(@PathVariable("listId") String listId,@RequestBody String params,@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" createNote method called--UserId is:"+userId+"ListId is:"+listId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
		    status = mnNoteable.createNote(params,listId,userId);
		    
			
		    if(status!=null && !status.equalsIgnoreCase("") && !status.trim().equals("0")){
		    	if(status.contains("\"noteId\""))
		    	{
		    		//nothing do..
		    	}
		    	else
		    	{
		    		String param[]=status.split(",");
		    		if(param.length==2)
		    			status="{\"eventId\":\""+param[0]+"\",\"listName\":\""+param[1]+"\"}";
		    		else
		    			status="{\"eventId\":\""+status+"\"}";
		    	}
		    }
		    if(logger.isInfoEnabled())
		    logger.info(" createNote method returned response successfully");
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Update Schedule Note */
	@RequestMapping(value = "/updateNote/{listId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateSchedulrNote(@PathVariable("listId") String listId,@RequestBody String params,@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" updateNote method called userId: "+userId+" listId: "+listId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
		    status = mnNoteable.updateScheduleNote(params,listId,userId);
		    
			
		    if(status!=null && !status.equalsIgnoreCase("") && !status.trim().equals("0")){
		    	if(status.contains("\"noteId\""))
		    	{
		    		//nothing do..
		    	}
		    	else
		    	{
		    		String param[]=status.split(",");
		    		if(param.length==2)
		    			status="{\"eventId\":\""+param[0]+"\",\"listName\":\""+param[1]+"\"}";
		    		else
		    			status="{\"eventId\":\""+status+"\"}";
		    	}
		    }
		    if(logger.isInfoEnabled())
		    logger.info(" updateNote method returned response  :");
		}
		catch (Exception e)
		{
			logger.error("Exception while updateNote : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Fetching Calendar Sharing Users Details Based On UserId*/
	@RequestMapping(value = "/fetchCalendarSharingUsers", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchCalendarSharingUsers(@RequestBody String params,HttpServletRequest request)
	{
		String status="";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchCalendarSharingUsers method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status=mnEventable.fetchCalendarSharingUsers(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchCalendarSharingUsers method returned response  :");
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarSharingUsers  :"+e.getMessage());
		}
		return status;
		
	}
	
	/* Fetching Calendar Events Based On UserId And ListId*/
	@RequestMapping(value = "/fetchCalendarEvents", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchCalendarEvents(@RequestBody String params,HttpServletRequest request)
	{
		String status="";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchCalendarEvents method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status=mnEventable.fetchCalendarEvents(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchCalendarEvents method returned response  :");
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarEvents  :"+e);
		}
		return status;
		
	}
	
	/* Fetching Particular Calendar Events Based On ListId,NoteId and UserId */
	@RequestMapping(value = "/fetchParticularEvent/{listId}/{eventId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchParticularEvent(@PathVariable("listId") String listId,@PathVariable("eventId") String eventId,@PathVariable("userId") String userId,HttpServletRequest request)
	{
		String status="";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchParticularEvent method called userId is: "+userId+" listId: "+listId);
			Integer listID=Integer.parseInt(listId);
			Integer eventID=Integer.parseInt(eventId);
			Integer userID=Integer.parseInt(userId);
			status=mnEventable.fetchParticularEvents(listID, eventID, userID);
			if(logger.isInfoEnabled())
			logger.info(" fetchParticularEvent method returned response :");
		}
		catch (Exception e) {
			logger.error("Error on fetchParticularEvent :"+e);
		}
		return status;
		
	}
	
	/* Create Schedule Event Based On ListId */
	@RequestMapping(value = "/createScheduleNote/{listId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createScheduleNote(@PathVariable("listId") String listId,@RequestBody String params, HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status="";
		String eventName="";
		String description="";
		String startdate="";
		String enddate="";
		String allday="";
		String repeatEvent="";
		String repeatUntil="";
		String repeatUntils="";
		String location="";
		String userEmail="";
		String userFullName="";
		String sendEmailFlag="";
		JSONObject jsonObject;
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" createScheduleNote method called listId: "+listId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			jsonObject = new JSONObject(params);
			eventName=(String)jsonObject.get("eventName");
			if(eventName.indexOf("`*`") != -1){
				eventName = eventName.replace("`*`", "\\\"");
			}
			description=(String) jsonObject.get("description");
			if(description.indexOf("`*`") != -1){
				description = description.replace("`*`", "\\\"");
			}
			startdate=(String) jsonObject.get("startDate");
			enddate=(String) jsonObject.get("endDate");
			allday=(String) jsonObject.get("allDay");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
			if(allday!=null && !allday.isEmpty() && allday.equals("true"))
			{
				allday="Yes";
			}
			else
			{
				allday="No";
			}
			location=(String)jsonObject.get("location");
			if(location!=null && !location.isEmpty())
			{
				if(location.indexOf("`*`") != -1){
					location = location.replace("`*`", "\\\"");
				}
			}
			else
			{
				location="-";
			}
			repeatEvent=(String) jsonObject.get("repeatEvent");
			if(repeatEvent!=null && !repeatEvent.isEmpty())
			{
				repeatEvent=(String) jsonObject.get("repeatEvent");
			}
			else
			{
				repeatEvent="-";
			}
			repeatUntils=(String) jsonObject.get("eventEndDate");
			if(repeatUntils!=null && !repeatUntils.isEmpty())
			{
				if(repeatEvent!=null && !repeatEvent.isEmpty()&& repeatEvent.equals("Daily"))
				{
				repeatUntil="This is a recurring event Occurs every day until ["+repeatUntils+"]";
				}
				else if(repeatEvent!=null && !repeatEvent.isEmpty()&& repeatEvent.equals("Weekly"))
				{
					repeatUntil="This is a recurring event Occurs every weekday until ["+repeatUntils+"]";
				}
				else
				{
					repeatUntil="This is a recurring event Occurs every month until ["+repeatUntils+"]";
				}
				
			}
			else
			{
				repeatUntil="-";
			}
			userEmail=(String) jsonObject.get("userMail");
			userFullName=(String) jsonObject.get("userFullName");
			status=mnEventable.createNote(params, listId);
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	status="{\"eventId\":\""+status+"\"}";
		    

			}
			if(logger.isInfoEnabled())
			logger.info(" createScheduleNote method returned response :");
		}
		catch (Exception e) {
			logger.error("Error on createScheduleNote  :"+e);
			status="0";
		}
		return status;
		
	}
	
	
	/* Update Schedule Event Details Based On ListId And EventId */
	@RequestMapping(value = "/updateScheduleNote/{listId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateScheduleNote(@PathVariable("listId") String listId,@RequestBody String params, HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status="";
		String eventName="";
		String description="";
		String startdate="";
		String enddate="";
		String allday="";
		String repeatEvent="";
		String repeatUntil="";
		String repeatUntils="";
		String location="";
		String userEmail="";
		String userFullName="";
		String sendEmailFlag="";
		JSONObject jsonObject;
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" updateScheduleNote method called listId: "+listId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			jsonObject = new JSONObject(params);
			eventName=(String)jsonObject.get("eventName");
			if(eventName.indexOf("`*`") != -1){
				eventName = eventName.replace("`*`", "\\\"");
			}
			description=(String) jsonObject.get("description");
			if(description.indexOf("`*`") != -1){
				description = description.replace("`*`", "\\\"");
			}
			startdate=(String) jsonObject.get("startDate");
			enddate=(String) jsonObject.get("endDate");
			allday=(String) jsonObject.get("allDay");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
			if(allday!=null && !allday.isEmpty() && allday.equals("true"))
			{
				allday="Yes";
			}
			else
			{
				allday="No";
			}
			location=(String)jsonObject.get("location");
			if(location!=null && !location.isEmpty())
			{
				if(location.indexOf("`*`") != -1){
					location = location.replace("`*`", "\\\"");
				}
			}
			else
			{
				location="-";
			}
			repeatEvent=(String) jsonObject.get("repeatEvent");
			if(repeatEvent!=null && !repeatEvent.isEmpty())
			{
				repeatEvent=(String) jsonObject.get("repeatEvent");
			}
			else
			{
				repeatEvent="-";
			}
			repeatUntils=(String) jsonObject.get("eventEndDate");
			if(repeatUntils!=null && !repeatUntils.isEmpty())
			{
				if(repeatEvent!=null && !repeatEvent.isEmpty()&& repeatEvent.equals("Daily"))
				{
				repeatUntil="This is a recurring event Occurs every day until ["+repeatUntils+"]";
				}
				else if(repeatEvent!=null && !repeatEvent.isEmpty()&& repeatEvent.equals("Weekly"))
				{
					repeatUntil="This is a recurring event Occurs every weekday until ["+repeatUntils+"]";
				}
				else
				{
					repeatUntil="This is a recurring event Occurs every month until ["+repeatUntils+"]";
				}
				
			}
			else
			{
				repeatUntil="-";
			}
			userEmail=(String) jsonObject.get("userMail");
			userFullName=(String) jsonObject.get("userFullName");
			status=mnEventable.updateNote(params, listId);
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	status="{\"eventId\":\""+status+"\"}";
			}
			if(logger.isInfoEnabled())
			logger.info(" updateScheduleNote method returned response :");
		}
		catch (Exception e) {
			logger.error("Error updateScheduleNote  :"+e);
			status="0";
		}
		return status;
		
	}
	
	/* Fetching Calendar Events Based On UserId */
	@RequestMapping(value="/fetchScheduleEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String fetchEvents(@RequestBody String params,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchScheduleEvents method called ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			result=mnEventable.fetchSchdulingEvents(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchScheduleEvents method returned response :");
			
		}
		catch (Exception e) {
			logger.error("Error on fetchScheduleEvents  :"+e);
			result="0";
		}
		return result;
	}
	
	/* Fetching Today Schedule Events Based On UserId */
	@RequestMapping(value="/fetchTodayScheduleEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String fetchTodayScheduleEvents(@RequestBody String params,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" fetchTodayScheduleEvents method called ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			result=mnEventable.fetchTodayEvents(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchTodayScheduleEvents method returned response :");
			
		}
		catch (Exception e) {
			logger.error("Error in fetchTodayScheduleEvents :"+e);
			result="0";
		}
		return result;
	}
	
	/* Copy Book */
	@RequestMapping(value = "/copyList", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String copyList(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" copyList method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.copyList(params);
			if(logger.isInfoEnabled())
			logger.info(" copyList method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception on copyList : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Archive All Notes In This Book */
	@RequestMapping(value = "/archieveAllNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String archieveAllNotesInThisList(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" archieveAllNotes method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.archiveAllNoteInList(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" archieveAllNotes method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Archieve All Note In This Book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Schedule Book Enable Disable */
	@RequestMapping(value = "/bookEnableDisable", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String enableDisableBook(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" bookEnableDisable method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.enabledisableBook(params);
			if(status!=null && !status.equalsIgnoreCase("0")){
		    }
			if(logger.isInfoEnabled())
			logger.info(" bookEnableDisable method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while enable and disable schedule book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Archive Book */
	@RequestMapping(value = "/archiveList/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String archiveThisList(@PathVariable ("userId") String userId,@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" archiveList method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.archiveList(params,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    }
			if(logger.isInfoEnabled())
			logger.info(" archiveList method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Archive This Book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Move All Notes To Another Book */
	@RequestMapping(value = "/moveAllNotes/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String moveAllNotesInThisList(@PathVariable ("userId") String userId,@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" moveAllNotes method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.moveAllNote(params,userId);
			
			if(status!=null && !status.equalsIgnoreCase("")){
		    }
			if(logger.isInfoEnabled())
			logger.info(" moveAllNotes method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception While Move All Note In This Book : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Set and Remove Due date For Note */
	@RequestMapping(value = "/dueDate/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteDueDate(@PathVariable ("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "success";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" dueDate method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.updateDuedateForNote(params, noteId, listId,userId);
			status="success";
			if(logger.isInfoEnabled())
			logger.info(" dueDate method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Due Date Set : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Copy Note To Individual */
	@RequestMapping(value = "/copyToIndividual/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteCopyToIndividual(@PathVariable ("userId")String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" copyToIndividual method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.copyNoteToIndividual(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" copyToIndividual method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Copy To Individual : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Copy Note To Group */
	@RequestMapping(value = "/copyToGroup/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteCopyToGroup(@PathVariable ("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" noteCopyToGroup method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.copyNoteToGroups(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteCopyToGroup method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Copy To Groups : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Copy Note To All Contact */
	@RequestMapping(value = "/copyToAllContact", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteCopyToAllContact(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" noteCopyToAllContact method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.copyNoteToAllContact(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteCopyToAllContact method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Copy To All Contact : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Sharing Note To Individual */
	@RequestMapping(value = "/members/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteMembers(@PathVariable ("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" noteMembers method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateMembersForNote(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteMembers method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Shared Individual Members : ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Sharing Note To Group */
	@RequestMapping(value = "/groups/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteGroups(@PathVariable("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" noteGroups method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateGroupsForNote(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteGroups method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Shared Groups : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Sharing Note To All Contact */
	@RequestMapping(value = "/shareAllContacts", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteAllContacts(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" shareAllContacts method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateAllContactMembersForNote(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    }
			if(logger.isInfoEnabled())
			logger.info(" shareAllContacts method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Shared All Contact Members : ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Shared Note Members Deleting */
	@RequestMapping(value = "/delmembers/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateMember(@PathVariable ("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" delmembers method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deleteMembersForNote(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delmembers method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Shared Notes Members Deleting : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Shared Note Groups Deleting */
	@RequestMapping(value = "/delgroup/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updategroup(@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" delgroup method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deletegroupsForNote(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delgroup method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Shared Notes Groups Deleting : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Shared Note All Contact Deleting */
	@RequestMapping(value = "/delSharedAllConNote", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateNoteAllContacts(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" delSharedAllConNote method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deleteAllContactMembersForNote(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delSharedAllConNote method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Deleting Note Shared All Contact Members : ", e);
			status = "0";
		}
		return status;
	}
	
	
	
	/* Sharing Book To Individual */
	@RequestMapping(value = "/bookSharedMembers", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String bookMembers(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" bookSharedMembers method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateMembersForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" bookSharedMembers method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Book Shared Individual Members : ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Sharing Book To Group */
	@RequestMapping(value = "/bookSharedgroups", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String bookGroups(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" bookSharedgroups method called ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateGroupsForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" bookSharedgroups method returned response :");
		}
		catch (Exception e)
		{
			logger.error("Exception while Book Shared Groups : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Sharing Book To All Contact */
	@RequestMapping(value = "/bookShareAllContacts", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String bookAllContacts(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("bookShareAllContacts method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateAllContactMembersForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" bookSharedgroups method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Book Shared All Contact Members : ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Shared Book Members Deleting */
	@RequestMapping(value = "/delBookmembers", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateBookMember(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("delBookmembers method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deleteMembersForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delBookmembers method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Deleting Book Shared Members : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Shared Book Groups Deleting */
	@RequestMapping(value = "/delBookgroup", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateBookgroup(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("delBookgroup method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deletegroupsForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delBookgroup method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Deleting Book Shared Groups : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Shared All Contact Deleting Based On Book */
	@RequestMapping(value = "/delBookSharedAllCon", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String delteBookSharedAllContacts(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("delteBookSharedAllContacts method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deleteAllContactMembersForBook(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" delteBookSharedAllContacts method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while delteBookSharedAllContacts Members : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Move Note In One Book To Another Book */
	@RequestMapping(value = "/moveNote/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String moveNote(@PathVariable ("userId") String userId,@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("moveNote method called  userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.moveNote(params,userId);
			
			if(status!=null && !status.equalsIgnoreCase("")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" moveNote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Move Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Check Book More Actions Access */
	@RequestMapping(value = "/checkListAccess", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkListMenuAccess(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("checkListAccess method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.checkListMenuAccess(params);
			
			if(status!=null && !status.equalsIgnoreCase("")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" checkListAccess method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Check List Access : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Change Note Access (Public/Private) */
	@RequestMapping(value = "/noteAccessChange/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkNoteAccessChange(@PathVariable ("userId") String userId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("noteAccessChange method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.changeNoteAccess(params,userId);
			
			/*if(status!=null && !status.equalsIgnoreCase("")){
				JSONObject jsonObject;
				jsonObject = new JSONObject(status);
				String access=(String) jsonObject.get("access");
				String noteName=(String) jsonObject.get("noteName");
				if(access.equals("public")){
				MnUsers mnUsers=mnNoteable.getUserDetails(Integer.parseInt(userId));
				
				if(mnUsers!=null ){
					String userName = mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName();
					List<Integer> usersIds = new ArrayList<Integer>(mnUsers.getFollowers());
					List<MnUsers> list = mnNoteable.getUsersDetails(usersIds);
					if (list != null && !list.isEmpty())
					{
						for (MnUsers users : list)
						{
							try{
							if(users.getNotificationFlag().equalsIgnoreCase("yes"))
							{
							SendMail sendMail=new SendMail(); 	
							String recipients[]={users.getEmailId()};
							String message=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+userName+" published a note to the Crowd : "+noteName+ ""+MailContent.sharingNotification2;
							sendMail.postEmail(recipients, "New content in the Musicnote Crowd",message);
							}
							}catch(Exception e){
								e.printStackTrace();
							}
						

						}
					}
					
				}

				}
		    	
		    }*/
			if(logger.isInfoEnabled())
			logger.info(" noteAccessChange method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Access Change : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Put Vote For Particular Note */
	@RequestMapping(value = "/vote", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String vote(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			
		}
		catch (Exception e)
		{
			logger.error("Exception while vote Particular Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Add Files To Particular Note */
	@RequestMapping(value = "/attachFile/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String attachFileInNote(@PathVariable ("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("attachFileInNote method called  userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			
			if(!params.trim().equals("")){
				params = decodeJsonString(params);
				
				if(params.lastIndexOf("=")!= -1 && params.lastIndexOf("=") !=0 ){
					params = params.substring(0, params.lastIndexOf("="));
				}
				status=params;
				if(!status.trim().equals("")){
					
					if(params.indexOf(",")!= -1 && params.indexOf(",") <= 2 ){
						
						params = params.substring(params.indexOf(",")+1,params.length() );
					}
				}
			}
			status = "";
			
			status = mnNoteable.updateAttachedFileForNote(params, noteId, listId,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" attachFileInNote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Attach File In Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Add Link To Particular Note */
	@RequestMapping(value = "/attachLink", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String attachLinkInNote(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			
		}
		catch (Exception e)
		{
			logger.error("Exception while Attach Link In Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Copy Note In One Book To Another Book */
	@RequestMapping(value = "/copyNote/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String copyNote(@PathVariable("userId") String userId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("copyNote method called  userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.copyNote(params,userId);
			
			if(status!=null && !status.equalsIgnoreCase("")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" copyNote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Copy Note : ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Copy Public Note In To Our Own Book */
	@RequestMapping(value = "/copyPublicNote", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String copyPublicNote(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("copyPublicNote method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.copyPublicNote(params);
			
			if(status!=null && !status.equalsIgnoreCase("")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" copyPublicNote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Copy Public Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Archive Particular Note */
	@RequestMapping(value = "/archiveNote/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String archiveNote(@PathVariable("userId")String userId, @RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("archiveNote method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			status = mnNoteable.archiveNote(params,userId);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" archiveNote method returned response ");

		}
		catch (Exception e)
		{
			logger.error("Exception while Archive Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Getting Attached File Based On Note */
	@RequestMapping(value = "/getAttachFile/{listId}/{noteId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getAttachFileInNote(@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		String attachFilePath = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getAttachFile method called listId: "+listId+" noteId :"+noteId);
			attachFilePath = mnNoteable.getAttachedFileForNote(noteId, listId);
			if(attachFilePath!=null && !attachFilePath.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" getAttachFile method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Fetching Attach File In Note : ", e);
			attachFilePath = "0";
		}
		return attachFilePath;
	}
	
	/* Fetch Particular Note Detail */
	@RequestMapping(value = "/noteDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getNoteDetails(@RequestBody String params, HttpServletRequest request)
	{
		String noteDetails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getNoteDetails method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			noteDetails = mnNoteable.getNoteDetails( params);
			if(noteDetails!=null && !noteDetails.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" getNoteDetails method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Attach File In Note : ", e);
			noteDetails = "0";
		}
		return noteDetails;
	}
	
	/* Fetch List Level Share Details */ 
	@RequestMapping(value = "/listDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getListDetails(@RequestBody String params, HttpServletRequest request)
	{
		String listDetails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("listDetails method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			listDetails = mnNoteable.getListDetails( params);
			if(listDetails!=null && !listDetails.equals("")){
				listDetails=listDetails.trim();
		    }
			if(logger.isInfoEnabled())
			logger.info(" listDetails method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Fetch List level Share Details : ", e);
			listDetails = "0";
		}
		return listDetails;
	}
	
	
	/* Tag The Particular Note */
	@RequestMapping(value = "/noteToTag", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteToTag(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("noteToTag method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateTagForNote(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteToTag method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Note Tag The Particular Note: ", e);
			status = "0";
		}
		return status;
	}
	
	/* Select And UnSelect Book To The Particular Note */
	@RequestMapping(value = "/noteToBook", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String noteToBook(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("noteToBook method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateBookForNote(params);
			
			if(status!=null && !status.equalsIgnoreCase("0")){
		    	
		    }
			if(logger.isInfoEnabled())
			logger.info(" noteToBook method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while noteToBook The Particular Note: ", e);
			status = "0";
		}
		return status;
	}
	
	
	/* Fetch Tags Based On User*/
	@RequestMapping(value = "/getTags", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getTags(@RequestBody String params, HttpServletRequest request)
	{
		String tagDeatails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getTags method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			tagDeatails = mnNoteable.getTags( params);
			if(tagDeatails!=null && !tagDeatails.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" getTags method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while noteToBook Based On user : ", e);
			tagDeatails = "0";
		}
		return tagDeatails;
	}
	
	/* Create Tag Based On User */
	@RequestMapping(value = "/createTag", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createTag(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("createTag method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.createTag( params);
			if(status!=null && !status.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" createTag method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while createTag In Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Update Tag */
	@RequestMapping(value = "/updateTag/{listId}/{noteId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateTag(@RequestBody String params,@PathVariable ("listId") String listId, @PathVariable ("noteId") String noteId,HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateTag method called listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.updateTag(params,listId,noteId);
			if(status!=null && !status.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" updateTag method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Updating Tag based On User : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Delete Tag */
	@RequestMapping(value = "/deleteTag", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String deleteTag(@RequestBody String params, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("deleteTag method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.deleteTag(params);
			if(status!=null && !status.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" deleteTag method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Deleting Tag Based On User : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Update Note Name Based On ListId,NoteId And UserId */
	@RequestMapping(value = "/updateNote/{listId}/{noteId}/{userId}/{type}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String updateNote(@RequestBody String newNoteName,@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("type") String type, HttpServletRequest request)
	{
		String status = "";
		try
		{	
			if(logger.isInfoEnabled())
			logger.info("updateNote method called  userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			newNoteName = decodeJsonString(newNoteName);
			newNoteName = removeUnWantedChar(newNoteName);
			if(newNoteName.indexOf("`*`") != -1){
				newNoteName = newNoteName.replace("`*`", "\\\"");
			}
		    status = mnNoteable.updateNoteName(listId,noteId,newNoteName,userId,type);
		    if(logger.isInfoEnabled())
		    logger.info(" updateNote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Updating Note Name : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Add Description For Particular Note Based On ListId,NoteId And UserId */
	@RequestMapping(value = "/addDescription/{listId}/{noteId}/{userId}", consumes = "text/plain",  method = RequestMethod.POST)
	@ResponseBody
	public String addDescription(@PathVariable ("userId") String userId, @RequestBody String params,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("addDescription method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(params.indexOf("\"") != -1){
				params = params.replace("\"", "\\\"");
			}
		    status = mnNoteable.addNoteDrescription(listId,noteId,params,userId);
		    if(logger.isInfoEnabled())
		    logger.info(" addDescription method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Adding Description For Note : ", e);
			status = "0";
		}
		return status;
	}
	
	/* Fetch Note Detail Based On ListId,NoteId And UserId */
	@RequestMapping(value = "/getNote/{listId}/{noteId}/{userId}", consumes = "text/plain",  produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchSingleNote(@PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String note = "";
		try
		{
		    note = mnNoteable.getNote(listId,noteId,userId);
		    
		}
		catch (Exception e)
		{
			logger.error("Exception while Fetching getNote  : ", e);
			note = "";
		}
		if(logger.isInfoEnabled())
		    logger.info(" getNote method returned response ");
		return note;
	}
	
	/* Fetch Note Detail Based On ListId,EventId And UserId */
	@RequestMapping(value="/getEvents/{listId}/{noteId}/{userId}", consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String getEvents(@PathVariable ("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("userId") String userId,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getEvents method called  userId :"+userId+" listId: "+listId+" noteId :"+noteId);
			result=mnEventable.getEvents(listId,noteId,userId);
			if(logger.isInfoEnabled())
			logger.info(" getEvents method returned response ");
			
		}
		catch (Exception e) {
			logger.error("Error getEvents method :"+e);
			result="0";
		}
		return result;
	}
	
	
	/* Add Comments For Note Based On ListId And NoteId */
	@RequestMapping(value = "/addComments/{listId}/{noteId}/{listType}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String addCommentsBasedUser(@RequestBody String params,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("listType") String listType, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("addComments method called listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			MnComments mnComments = mnNoteable.convertJsonToMnCommentsObj(params); 
		    note = mnNoteable.addComments(mnComments,listId,noteId,listType);
		    if(logger.isInfoEnabled())
		    logger.info(" addComments method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Add Comment Based On Note : ", e);
			note = "";
		}
		return note;
	}
	
	/* Fetch Comments Based On NoteId */
	@RequestMapping (value ="/getComments/{cmtsLevel}/{userId}" ,consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getComments(@RequestBody String params,@PathVariable("cmtsLevel") String cmtsLevel,@PathVariable ("userId") String userId,HttpServletRequest request){
		String comments="";
		try{
			if(logger.isInfoEnabled())
			logger.info("getComments method called  userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			comments= mnNoteable.getComments(params,cmtsLevel,userId);
			if(logger.isInfoEnabled())
			logger.info(" getComments method returned response ");
			
		}catch (Exception e) {
			logger.error("Exception while getComments Based On Note : ", e);
		}
		return comments;
	}
	
	/**/
	@RequestMapping (value ="/getAttachedByFileId/{userId}" ,consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getAttachByFileId(@RequestBody String  params,@PathVariable("userId") String userId,HttpServletRequest request){
		String files="";
		try{
			if(logger.isInfoEnabled())
			logger.info("getAttachedByFileId method called  userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			files= mnNoteable.getAttachByFileId(params,userId);
			if(logger.isInfoEnabled())
			logger.info(" getAttachedByFileId method returned response ");
			
		}catch (Exception e) {
			logger.error("Exception while Fetch getAttachedByFileId Based On Note : ", e);
		}
		return files;
	}
	
	@RequestMapping (value ="/getAttachedFileInNote/{listId}/{noteId}/{userId}" ,consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getAttachedFileInNote(@PathVariable("listId") String listId, @PathVariable("noteId") String noteId, @PathVariable("userId") String userId,HttpServletRequest request){
		String files="";
		try{
			if(logger.isInfoEnabled())
			logger.info("getAttachedFileInNote method called  userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			files= mnNoteable.getAttachedFileInNote(listId, noteId, userId);
			if(logger.isInfoEnabled())
			logger.info("getAttachedFileInNote method returned response ");
			
		}catch (Exception e) {
			logger.error("Exception while Fetch getAttachedFileInNote Based On Note : ", e);
		}
		return files;
	}
	
	/* Fetch Comments Count Based On Note */
	@RequestMapping (value ="/getCommentsCount" ,consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getCommentsCount(@RequestBody String params,HttpServletRequest request){
		String comments="";
		try{
			if(logger.isInfoEnabled())
			logger.info("getCommentsCount method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			comments= mnNoteable.getCommentsCountBasedOnNote(params);
			if(logger.isInfoEnabled())
			logger.info(" getCommentsCount method returned response ");
			
		}catch (Exception e) {
			logger.error("Exception while getCommentsCount Based On Note : ", e);
		}
		return comments;
	}
	
	/* Update Comment */
	@RequestMapping(value = "/updateComments/{listId}/{noteId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String updateCommentsBasedUser(@RequestBody String params,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateCommentsBasedUser method called listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
		    note = mnNoteable.updateComments(params,listId,noteId);
		    if(logger.isInfoEnabled())
		    logger.info(" updateCommentsBasedUser method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Update Comment Based On Note : ", e);
			note = "";
		}
		return note;
	}
	
	/* Delete Comment */
	@RequestMapping(value = "/deleteComments/{listId}/{noteId}/{cId}/{type}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCommentsBasedUser(@PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("cId") String cId,@PathVariable("type") String type, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("deleteComments method called listId: "+listId+" noteId :"+noteId);
		    note = mnNoteable.deleteComments(cId, listId, noteId,type);
		    if(logger.isInfoEnabled())
		    logger.info(" deleteComments method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Delete Comment For Note : ", e);
			note = "";
		}
		return note;
	}
	
	@RequestMapping(value = "/castUncastVote/{listId}/{noteId}/{userId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String castAndUncastVote(@RequestBody String params, @PathVariable("userId") String userId, @PathVariable("listId") String listId, @PathVariable("noteId") String noteId, HttpServletRequest request ){
		String status="";
		try{
			if(logger.isInfoEnabled())
			logger.info("castAndUncastVote method called userId: "+userId+" listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status=params;
			if(!status.trim().equals("")){
				
				if(params.indexOf(",")!= -1 && params.indexOf(",") <= 2 ){
					
					params = params.substring(params.indexOf(",")+1,params.length() );
				}
				
				params="["+params+"]"; // for an array of string
			}
			status = "";
			
			status = mnNoteable.castUncastVotes(params,listId, noteId,userId );
			if(logger.isInfoEnabled())
			logger.info(" castAndUncastVote method returned response ");
			
		}catch (Exception e) {
			logger.error("Exception while casting vote to Note : ", e);
			status = "";
		}
		return status;
	}
	

	
	
	@RequestMapping(value = "/getList", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getList(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getList method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.getList(params);
			if(logger.isInfoEnabled())
			logger.info(" getList method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while get List : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/newNoteLog", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String newNoteLog(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("newNoteLog method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnLog log=mnNoteable.convertJsonToMnLogObj(params);
			status = mnNoteable.insertNewNoteLog(log);
			if(logger.isInfoEnabled())
			logger.info(" newNoteLog method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while create new note Log : ", e);
			status = "0";
		}
		return status;
	}
	
	
	@RequestMapping(value = "/insertMostViewed/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String mostViewed(@RequestBody String params,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("insertMostViewed method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnVoteViewCount mnVoteViewCount=mnNoteable.convertJsonToMnVoteViewCountObj(params);
			status = mnNoteable.insertMostViewed(mnVoteViewCount,userId);
			if(logger.isInfoEnabled())
			logger.info(" insertMostViewed method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while insertMostViewed method in Note : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/updatemostViewed/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updatemostViewed(@RequestBody String params,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updatemostViewed method called  userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			MnVoteViewCount mnVoteViewCount=mnNoteable.convertJsonToUpdateMnVoteViewCountObj(params);
			status = mnNoteable.updateMostViewed(mnVoteViewCount,userId);
			if(logger.isInfoEnabled())
			logger.info(" updatemostViewed method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while updated most viewed count : ", e);
			status = "0";
		}
		return status;
	}
	
	
	@RequestMapping(value = "/getmostViewed", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchmostViewed(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getmostViewed method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.fetchMostViewed(params);
			if(logger.isInfoEnabled())
			logger.info(" getmostViewed method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while fetching most viewed count : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/RecentnewNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getRecentNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("RecentnewNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.recentNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" RecentnewNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getting recent Notes : ", e);
			status = "0";
		}
		return status;
	}
	
	
	
	@RequestMapping(value = "/maxNostView", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String maxMostViewed(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("maxMostViewed method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnNoteable.maxMostViewed(params);
			if(logger.isInfoEnabled())
			logger.info(" maxMostViewed method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while fetching max most viewed count : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/checkOwner/{listId}/{noteId}/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkOwnerOfList(@PathVariable("noteId") String noteId,@PathVariable("listId") String listId, @PathVariable ("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("checkOwner method called  "+userId);
			status = mnNoteable.checkOwnerOfList(listId, noteId, userId);
			
			if(logger.isInfoEnabled())
			logger.info(" checkOwner method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while checkOwner method in  checkOwner: ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/publicShareWarnMsgFlagChange/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String publicShareWarnMsgFlagChange(@RequestBody String params,@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("publicShareWarnMsgFlagChange method called userId: "+userId);
			status = mnNoteable.changePublicShareWarnMsgFlag(params, userId);
			if(logger.isInfoEnabled())
			logger.info(" publicShareWarnMsgFlagChange method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Public Share Warn Msg Flag change : ", e);
			status = "0";
		}
		return status;
	}
	
	
	
	
	@RequestMapping(value = "/dateSearchNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getdateSearchNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("dateSearchNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.dateSearchNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" dateSearchNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getting Date Search Notes : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/MostVieweddateSearchNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getMostVieweddateSearchNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getMostVieweddateSearchNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.MostVieweddateSearchNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" getMostVieweddateSearchNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getting Date Search Notes : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/MostVoted", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getMostVoted(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getMostVoted method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.MostVoted(params);
			if(logger.isInfoEnabled())
			logger.info(" getMostVoted method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getting Most Voted  : ", e);
			status = "0";
		}
		return status;
	}
	


	@RequestMapping(value = "/MostVoteddateSearchNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getMostVoteddateSearchNotes(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getMostVoteddateSearchNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
		
			status = mnNoteable.MostVoteddateSearchNotes(params);
			if(logger.isInfoEnabled())
			logger.info(" getMostVoteddateSearchNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getMostVoteddateSearchNotes : ", e);
			status = "0";
		}
		return status;
	}

	@RequestMapping (value ="/addRemainder", consumes  = "text/plain", produces="application/json", method =  RequestMethod.POST)
	@ResponseBody
	public String addRemainder (@RequestBody String params, HttpServletRequest request){
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try{
			if(logger.isInfoEnabled())
			logger.info("addRemainder method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			JSONObject jobj=new JSONObject(params);
			String noteName= (String) jobj.get("noteName");
			MnRemainders mnRemainders =mnNoteable.convertJsonToMnRwmainderObj(params);
			status = mnNoteable.addRemainder(mnRemainders,noteName);
			JSONObject jsonObject = new JSONObject(status);
			/*if(mnRemainders.isSendMail()){
				status = jsonObject.getString("sharedUserListForMail");
				
				if(status!= null && !status.equals("")){
					if(status.indexOf("[")!= -1){
						status = status.substring(status.indexOf("[")+1,status.length());
					}
					if(status.indexOf("]")!= -1){
						status = status.substring(0,status.indexOf("]"));
					}
					String[] array =  status.split(",");
					if(array.length > 0 && !array[0].equals("") ){
						for(String shrserId : array){
							MnUsers mnUsers=mnNoteable.getUserDetails(Integer.parseInt(shrserId.trim()));
							if(mnUsers!=null ){
								String label ="Added reminder on your note, "+noteName+": \""+mnRemainders.getEventDate()+" "+mnRemainders.getEventTime()+"\""
												+"<BR>Description : "+mnRemainders.getrName()+"<BR>" ;
								try{
									if(mnUsers.getNotificationFlag().equalsIgnoreCase("yes"))
									{
									SendMail sendMail=new SendMail(); 	
									String recipients[]={mnUsers.getEmailId()};
									String message=MailContent.sharingNotification+mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName()+MailContent.sharingNotification1+label+" "+MailContent.sharingNotification2;
									sendMail.postEmail(recipients, "Reminder Added",message);
									}
									
								}catch(Exception e){
									e.printStackTrace();
								}
							}	
						}
					}
				}
			}*/
			status = jsonObject.getString("rId");
			if(logger.isInfoEnabled())
			logger.info(" addRemainder method returned response ");
		}catch (Exception e	) {
			logger.error("Exception while add Remainder : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/getListBasedOnListId/{listId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getListBasedOnListId(@PathVariable("listId") String listId,@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getListBasedOnListId method called  listId :"+listId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(!params.trim().equals("")){
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				if(((String) jsonObject.get("listType")).equals("schedule")){
					status = mnEventable.getListBasedOnListIdForSchedule(listId, params);
				}else{
					status = mnNoteable.getListBasedOnListId(listId, params);
				}
				
				if(status==null || status.isEmpty())
					status="0";
					
			}
			if(logger.isInfoEnabled())
			logger.info(" getListBasedOnListId method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getListBasedOnListId Notes : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping (value ="/getRemainder/{userId}", consumes  = "text/plain", method =  RequestMethod.POST)
	@ResponseBody
	public String getRemainder (@RequestBody String params,@PathVariable("userId") String userId ,HttpServletRequest request){
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try{
			if(logger.isInfoEnabled())
			logger.info("getRemainder method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(!params.trim().equals("")){
				status = mnNoteable.getRemaindersList(params,userId);
			}
			if(logger.isInfoEnabled())
			logger.info(" getRemainder method returned response ");
		}catch (Exception e	) {
			logger.error("Exception while get Remainder : ", e);
		}
		return status;
	}
	
	@RequestMapping(value = "/updateRemainder/{listId}/{noteId}/{rId}/{noteName}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String updateRemainderBasedUser(@RequestBody String params,@PathVariable("noteName") String noteName ,@PathVariable("rId") String rId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateRemainderBasedUser method called listId: "+listId+" noteId :"+noteId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
		    note = mnNoteable.updateRemainders(params,listId,noteId,noteName);

		    JSONObject jsonObject = new JSONObject(params);
		    JSONObject object = new JSONObject(note);
			if(jsonObject.getBoolean("sendMail")){
				
				/*note = object.getString("sharedUserListForMail");
				
				if(note!= null && !note.equals("")){
					if(note.indexOf("[")!= -1){
						note = note.substring(note.indexOf("[")+1,note.length());
					}
					if(note.indexOf("]")!= -1){
						note = note.substring(0,note.indexOf("]"));
					}
					String[] array =  note.split(",");
					if(array.length > 0 && !array[0].equals("") ){
						for(String shrserId : array){
							MnUsers mnUsers=mnNoteable.getUserDetails(Integer.parseInt(shrserId.trim()));
							if(mnUsers!=null ){
								String label ="Updated reminder on your note, "+noteName+": \""+jsonObject.getString("eventDate")+" "+  jsonObject.getString("eventTime")+"\""
								+"<BR>Description : "+jsonObject.getString("rName")+"<BR>" ;
								try{
									if(mnUsers.getNotificationFlag().equalsIgnoreCase("yes"))	
									{
									SendMail sendMail=new SendMail(); 	
									String recipients[]={mnUsers.getEmailId()};
											
									String message=MailContent.sharingNotification+mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName()+MailContent.sharingNotification1+label+" "+MailContent.sharingNotification2;
									sendMail.postEmail(recipients, "Reminder Updated",message);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}	
						}
					}
				}*/
			}
			note = object.getString("rId");
			if(logger.isInfoEnabled())
			logger.info(" updateRemainderBasedUser method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while updateRemainderBasedUser : ", e);
			note = "";
		}
		return note;
	}
	
	@RequestMapping(value = "/deleteRemainders/{listId}/{noteId}/{rId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String deleteRemaindersBasedUser(@PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("rId") String rId, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("deleteRemainders method called listId: "+listId+" noteId :"+noteId);
		    note = mnNoteable.deleteRemainder(rId, listId, noteId);
		    if(logger.isInfoEnabled())
		    logger.info(" deleteRemainders method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while deleteRemainders : ", e);
			note = "";
		}
		return note;
	}
	
	@RequestMapping(value = "/getVotes/{listId}/{noteId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getVoteBasedObnote(@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getVoteBasedObnote method called  listId: "+listId+" noteId :"+noteId);
		    note = mnNoteable.getVotesBasedNote(listId, noteId);
		    if(logger.isInfoEnabled())
		    logger.info(" getVoteBasedObnote method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getVoteBasedObnote : ", e);
			note = "";
		}
		return note;
	}
	
	@RequestMapping(value = "/getFeaturedNotes", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getFeaturedNotes(@RequestBody String params,HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getFeaturedNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
		    note = mnNoteable.getFeatureNotes(params);
		    if(logger.isInfoEnabled())
		    logger.info(" getFeaturedNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getFeaturedNotes : ", e);
			note = "";
		}
		return note;
	}
	
	@RequestMapping(value = "/Searching", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String searchEngine(@RequestBody String searchingString,HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("Searching method called  ");
			searchingString = decodeJsonString(searchingString);
			searchingString = removeUnWantedChar(searchingString);
			status = mnNoteable.searchEngine(searchingString);
			if(logger.isInfoEnabled())
			logger.info(" Searching method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while searching Note : ", e);
			status = "";
		}
		return status;
	}
	
	@RequestMapping(value = "/getSearching/{userId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getsearch(@RequestBody String searchingString,@PathVariable("userId") String userId,HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getSearching method called userId: "+userId);
			searchingString = decodeJsonString(searchingString);
			searchingString = removeUnWantedChar(searchingString);
			status = mnNoteable.getSearch(searchingString,userId);
			if(logger.isInfoEnabled())
			logger.info(" getSearching method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while get Seraching Note : ", e);
			status = "";
		}
		return status;
	}
	
	@RequestMapping(value = "/getsearchMostViewd/{userId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getsearchMostViewd(@RequestBody String searchingString,@PathVariable("userId")String userId,HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getsearchMostViewd method called userId: "+userId);
			searchingString = decodeJsonString(searchingString);
			searchingString = removeUnWantedChar(searchingString);
			status = mnNoteable.getSearchMostViewd(searchingString,userId);
			if(logger.isInfoEnabled())
			logger.info(" getSearching method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while get Searching  most viewed count : ", e);
			status = "0";
		}
		return status;
	}
	
	
	@RequestMapping(value = "/getSearchMostVoted/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getSearchMostVoted(@RequestBody String params,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getSearchMostVoted method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.getSerachMostVoted(params,userId);
			if(logger.isInfoEnabled())
			logger.info(" getSearchMostVoted method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getting search Most Voted  : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/getAllNoteByKeyWord", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getAllNoteByKeyWord(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getAllNoteByKeyWord method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(!params.trim().equals("")){
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				if(((String) jsonObject.get("listType")).equals("schedule")){
					status = mnEventable.getNoteByKwywordsForSchedule(params,mnNoteable);
				}else{
					status = mnNoteable.getNoteByKwywords(params);
				}
				
				if(status==null || status.isEmpty())
					status="0";
					
			}
			if(logger.isInfoEnabled())
			logger.info(" getAllNoteByKeyWord method returned response ");
			
		}
		catch (Exception e)
		{
			logger.error("Exception while getAllNoteByKeyWord  : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value="/getEventsFromList", consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String getEventsFromList(@RequestBody String params ,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getEventsFromList method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			result=mnEventable.getEventsFromList(params);
			
		}
		catch (Exception e) {
			logger.error("Error getEventsFromList :"+e);
			result="0";
		}
		return result;
	}
	
	@RequestMapping(value = "/createNoteInDefualtList/{listType}/{userId}/{selectedBookId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createNoteInDefualtList(@PathVariable("listType") String listType,@PathVariable("selectedBookId") String selectedBookId,@RequestBody String params,@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("createNoteInDefualtList method called userId: "+userId);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
		    status = mnNoteable.createNoteInDefualtList(params,listType,userId,selectedBookId);
		    if(logger.isInfoEnabled())
		    logger.info(" createNoteInDefualtList method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while createNoteInDefualtList : ", e);
			status = "0";
		}
		return status;
	}
	
	
	@RequestMapping(value = "/fetchBookNames", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getBookNames(@RequestBody String params, HttpServletRequest request)
	{
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("fetchBookNames method called  ");
		    note = mnNoteable.getBookNames(params);
		    if(logger.isInfoEnabled())
		    logger.info(" fetchBookNames method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while fetchBookNames ", e);
			note = "";
		}
		logger.info("note :"+note);
		return note;
	}
	
	@RequestMapping(value = "/getTagForAutoSearch", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String getTagForAutoSearch(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String note = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getTagForAutoSearch method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
		    note = mnNoteable.getTagForAutoSearch(params);
		    if(logger.isInfoEnabled())
		    logger.info(" getTagForAutoSearch method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getTagForAutoSearch ", e);
			note = "";
		}
		logger.info("note :"+note);
		return note;
	}
	
	@RequestMapping(value = "/getAllNoteFilters", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getAllNoteFilters(@RequestBody String params, HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String status = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" getAllNoteFilters method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			status = mnNoteable.getAllNoteFilters(params);
			if(logger.isInfoEnabled())
			logger.info(" getAllNoteFilters method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while getAllNoteFilters  : ", e);
			status = "0";
		}
		return status;
	}

	
	@RequestMapping(value = "/getAllTags", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getAllTags(HttpServletRequest request)
	{
		String tagDeatails = "";
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		try
		{
			if(logger.isInfoEnabled())
			logger.info(" getAllTags method called  ");
			tagDeatails = mnNoteable.getAllTags();
			if(tagDeatails!=null && !tagDeatails.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" getAllTags method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Fetching All Tag ", e);
			tagDeatails = "0";
		}
		return tagDeatails;
	}
	
	@RequestMapping(value = "/sharedNotes", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String sharedNotes(@RequestBody String params, HttpServletRequest request)
	{
		String sharedDeatails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("sharedNotes method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			sharedDeatails = mnNoteable.getSharedNotes(params);
			if(sharedDeatails!=null && !sharedDeatails.equals("")){

		    }else{
		    	sharedDeatails = "0";
		    }
			if(logger.isInfoEnabled())
			logger.info(" sharedNotes method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception while Fetching sharedNote details user : ", e);
		}
		return sharedDeatails;
	}
	
	
	@RequestMapping(value="/updateSubEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String updateSubEvents(@RequestBody String params,HttpServletRequest request)
	{
		String eventName="";
		String description="";
		String startdate="";
		String enddate="";
		String allday="";
		String location="";
		String userEmail="";
		String userFullName="";
		String sendEmailFlag="";
		String overlapping=null;
		JSONObject jsonObject;
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateSubEvents method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			
			jsonObject = new JSONObject(params);
			eventName=(String)jsonObject.get("eventName");
			description=(String) jsonObject.get("description");
			startdate=(String) jsonObject.get("startDate");
			enddate=(String) jsonObject.get("endDate");
			allday=(String) jsonObject.get("alldayevent");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
			
			if(allday!=null && !allday.isEmpty() && allday.equals("true"))
			{
				allday="Yes";
			}
			else
			{
				allday="No";
			}
			location=(String)jsonObject.get("location");
			if(location!=null && !location.isEmpty())
			{
				
			}
			else
			{
				location="-";
			}
			
			userEmail=(String) jsonObject.get("userMail");
			userFullName=(String) jsonObject.get("userFullName");
			overlapping=(String)jsonObject.get("overlappingEvent");
			if(overlapping!=null && overlapping.equals("true"))
			{
			result=mnEventable.updateMnSubEvent(params);
			}
			else
			{
				result=mnEventable.updateAllMnSubEvent(params);
			}
			
			if(logger.isInfoEnabled())
			logger.info(" updateSubEvents method returned response ");
			
		}
		catch (Exception e) {
			logger.error("Error in updateSubEvents method:"+e);
			result="0";
		}
	return result;
	}
	
	
	@RequestMapping(value="/deleteSubEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String deleteSubEvents(@RequestBody String params,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("deleteSubEvents method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			result=mnEventable.deleteMnSubEvent(params);
			if(logger.isInfoEnabled())
			logger.info(" deleteSubEvents method returned response ");
			
		}
		catch (Exception e) {
			logger.error("Error deleteSubEvents method :"+e);
			result="0";
		}
		return result;
	}
	@RequestMapping(value = "/fetchSubEvents", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchSubEvents(@RequestBody String params, HttpServletRequest request)
	{
		String sharedDeatails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("fetchSubEvents method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			sharedDeatails = mnEventable.fetchSubEvents(params);
			if(sharedDeatails!=null && !sharedDeatails.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" fetchSubEvents method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchSubEvents  methods ", e);
			sharedDeatails = "0";
		}
		return sharedDeatails;
	}
	
	
	@RequestMapping(value = "/getSubEvents", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getSubEvents(@RequestBody String params, HttpServletRequest request)
	{
		String sharedDeatails = "";
		try
		{
			if(logger.isInfoEnabled())
			logger.info("getSubEvents method called  ");
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			sharedDeatails = mnEventable.getSubEvents(params);
			if(sharedDeatails!=null && !sharedDeatails.equals("")){

		    }
			if(logger.isInfoEnabled())
			logger.info(" getSubEvents method returned response ");
		}
		catch (Exception e)
		{
			logger.error("Exception in getSubEvents methods  ", e);
			sharedDeatails = "0";
		}
		return sharedDeatails;
	}
	
	
	@RequestMapping(value="/updateSubFollowingEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String updateSubFollowingEvents(@RequestBody String params,HttpServletRequest request)
	{
		String eventName="";
		String description="";
		String startdate="";
		String enddate="";
		String allday="";
		String location="";
		String userEmail="";
		String userFullName="";
		String sendEmailFlag="";
		String overlapping=null;
		JSONObject jsonObject;
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateSubFollowingEvents method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			
			jsonObject = new JSONObject(params);
			eventName=(String)jsonObject.get("eventName");
			description=(String) jsonObject.get("description");
			startdate=(String) jsonObject.get("startDate");
			enddate=(String) jsonObject.get("endDate");
			allday=(String) jsonObject.get("alldayevent");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
			
			if(allday!=null && !allday.isEmpty() && allday.equals("true"))
			{
				allday="Yes";
			}
			else
			{
				allday="No";
			}
			location=(String)jsonObject.get("location");
			if(location!=null && !location.isEmpty())
			{
				
			}
			else
			{
				location="-";
			}
			
			userEmail=(String) jsonObject.get("userMail");
			userFullName=(String) jsonObject.get("userFullName");
			overlapping=(String)jsonObject.get("overlappingEvent");
			if(overlapping!=null && overlapping.equals("true"))
			{
			result=mnEventable.updateMnSubEvent(params);
			}
			else
			{
				result=mnEventable.updateSubFollowingEvents(params);
			}
			
			if(logger.isInfoEnabled())
			logger.info(" updateSubFollowingEvents method returned response ");
		}
		catch (Exception e) {
			logger.error("Error in updateSubFollowingEvents:"+e);
			result="0";
		}
	return result;
	}
	
	
	@RequestMapping(value="/updateSingleEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String updateSingleEvents(@RequestBody String params,HttpServletRequest request)
	{
		String eventName="";
		String description="";
		String startdate="";
		String enddate="";
		String allday="";
		String location="";
		String userEmail="";
		String userFullName="";
		String sendEmailFlag="";
		String overlapping=null;
		JSONObject jsonObject;
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("updateSingleEvents method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			
			jsonObject = new JSONObject(params);
			eventName=(String)jsonObject.get("eventName");
			description=(String) jsonObject.get("description");
			startdate=(String) jsonObject.get("startDate");
			enddate=(String) jsonObject.get("endDate");
			allday=(String) jsonObject.get("alldayevent");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
			
			if(allday!=null && !allday.isEmpty() && allday.equals("true"))
			{
				allday="Yes";
			}
			else
			{
				allday="No";
			}
			location=(String)jsonObject.get("location");
			if(location!=null && !location.isEmpty())
			{
				
			}
			else
			{
				location="-";
			}
			
			userEmail=(String) jsonObject.get("userMail");
			userFullName=(String) jsonObject.get("userFullName");
			overlapping=(String)jsonObject.get("overlappingEvent");
			
			result=mnEventable.updateMnSingleEvent(params);
			if(logger.isInfoEnabled())
			logger.info(" updateSingleEvents method returned response ");
		}
		catch (Exception e) {
			logger.error("Error in updateSingleEvents:"+e);
			result="0";
		}
	return result;
	}
	
	
	
	@RequestMapping(value="/fetchEvents",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String fetchParticularEvents(@RequestBody String params,HttpServletRequest request)
	{
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		String result=null;
		try
		{
			if(logger.isInfoEnabled())
			logger.info("fetchEvents method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			result=mnEventable.fetchParticularEvents(params);
			if(logger.isInfoEnabled())
			logger.info(" fetchEvents method returned response ");
		}
		catch (Exception e) {
			logger.error("Error in fetchEvents:"+e);
			result="0";
		}
		return result;
	}
	
	
	@RequestMapping(value="/complaintReportMail",consumes="text/plain",produces="application/json",method=RequestMethod.POST)
	@ResponseBody
	public String complaintReportMail(@RequestBody String params,HttpServletRequest request)
	{
		mnNoteable = (MnNote) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLIST);
		String result=null;
		int commentCount=1;
		if(logger.isInfoEnabled())
			logger.info("complaintReportMail method called  ");
			params=decodeJsonString(params);
			params=removeUnWantedChar(params);
			MnComplaints report = mnNoteable.MnComplaints(params);
			String status = "";
			//StringBuffer commentBuffer=new StringBuffer();
			List<String> arrayList=new ArrayList<String>();
			//arrayList.addAll(report.getCommentsList());
			try{
				if(report!= null){
					
					if(report.getReportPage().equals("crowd")){
						// for crowd page-- admin
					/*for(String comments:arrayList){
						//commentBuffer.append("<b>("+commentCount+++")</b>"+comments);
						//commentBuffer.append("<br></br>");
						//commentBuffer.append("<br></br>");
					}*/
					SendMail sendMail=new SendMail();
					String recipient[]={MailContent.clientMailId};//feedback to client
					String recipients[]={report.getUserMailId()};//from client to user
					String userComplaint=report.getUserComplaints();
						if(userComplaint.indexOf("\\\"") != -1){
							userComplaint = userComplaint.replace("\\\"", "\"");
						}
					try{
						
						String message1=MailContent.userfeedback+"<br><b> User Name : </b>" +report.getUserName()+
						"<br><br><b> Email id: </b>" +report.getUserMailId()+
						"<br><br><b> Complaint : </b>" +userComplaint+
						"<br><br><b> Note name : </b>" +report.getNoteName()+
						"<br><br><b> Owner UserName : </b>" +report.getOwnerUserName()+
						"<br><br><b> Shared By : </b>" +report.getSharedByUserName()+
						"<br><br><b> Note ID : </b>" +report.getCompliantId();
						
						//MailContent.signature;
						String subject1="Complaint";
						String message=MailContent.sharingNotification+report.getUserFirstName()+""+MailContent.sharingNotification1+"<br>Thanks for your complaint. Our team is looking into it and will respond to You as soon as possible."+ 
						MailContent.signature1;
						String subject="Thanks for your complaint";
						sendMail.postEmail(recipient, subject1, message1);
						//sendMail.postEmail(recipients, subject,message);
						status="success";
					}catch(Exception e){
						logger.error("Exception while complaintReportMail : ", e);
					}
					
					
				  }else{
						// for note and memo page-- admin not handle
					 /* for(String comments:arrayList){
							commentBuffer.append("<b>("+commentCount+++")</b>"+comments);
							commentBuffer.append("<br></br>");
							commentBuffer.append("<br></br>");
						}*/
					  SendMail sendMail=new SendMail();
						String recipient[]={report.getOwnerMailId()};//feedback to client
						//String recipients[]={report.getUserMailId()};//from client to user
						String userComplaint=report.getUserComplaints();
							if(userComplaint.indexOf("\\\"") != -1){
								userComplaint = userComplaint.replace("\\\"", "\"");
							}
						try{
							
							String message=MailContent.userfeedback+"<br><b> User Name : </b>" +report.getUserFirstName()+" "+report.getUserLastName()+
							"<br><br><b> Email id: </b>" +report.getUserMailId()+
							"<br><br><b> Complaint : </b>" +userComplaint+
							"<br><br><b> Note name : </b>" +report.getNoteName()+
							//"<br><br><b> Comments : </b>" +commentBuffer+
							MailContent.signature;
							String subject="Complaint regarding your shared note";
							sendMail.postEmail(recipient, subject, message);
							status="success";
						}catch(Exception e){
							logger.error("Exception while complaintReportMail : ", e);
						}
						
					}
				}
			}
			catch (Exception e)
			{
				logger.error("Exception while complaintReportMail  : ", e);
				status = "0";
			}
			if(logger.isInfoEnabled())
			logger.info("Return response for complaintReportMail method : "+status);
			return status;
	}
}