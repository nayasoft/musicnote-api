package com.musicnotes.apis.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import amazonupload.AmazonUploadable;

import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.interfaces.MnAdminable;
import com.musicnotes.apis.interfaces.MnUserable;
import com.musicnotes.apis.resources.MnAdmin;
import com.musicnotes.apis.resources.MnUser;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController
{

	MnAdminable mnAdmin;
	Logger logger = Logger.getLogger(AdminController.class);
	
	
	@RequestMapping(value = "/checkDefaultConfig", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkDefaultConfiguration(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while checking  a checkDefaultConfiguration method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.checkDefaultConfiguration();
		} catch (Exception e) {
			logger.error("Exception while fetching user level: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for checkDefaultConfiguration method : ");
		return status;
	}
	
	@RequestMapping(value = "/addDefaultConfig", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String addDefaultConfiguration(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while adding  a addDefaultConfiguration method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.addDefaultConfiguration(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user level: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for addDefaultConfig method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/checkUserLevel", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkExistingUserLevel(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while checking  a checkExistingUserLevel method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.checkExistingUserLevel(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user level: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for checkExistingUserLevel method : ");
		return status;
	}
	
	@RequestMapping(value = "/addMailConfiguration", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String addMailConfiguration(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while adding  a addMailConfiguration method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		MnMailConfiguration configuration=mnAdmin.convertJsonToMailconfigurationObj(params);
		String status = "";
		try {
			status = mnAdmin.createMailConfiguration(configuration);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for addMailConfiguration method : "+status);
		return status;
	}
	
	@RequestMapping(value = "/updateMailConfiguration/{mailNo}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateMailConfiguration(@PathVariable("mailNo") String mailNo,@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while updating  a updateMailConfiguration method: mailNo: "+mailNo);
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		MnMailConfiguration configuration=mnAdmin.convertJsonToMailconfigurationObj(params);
		configuration.setMailNo(Integer.parseInt(mailNo));
		try {
			status = mnAdmin.updateMailConfiguration(configuration);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for updateMailConfiguration method : mailNo: "+mailNo);
		return status;
	}
	
	
	@RequestMapping(value = "/deleteMailConfiguration", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String deleteMailConfiguration(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while deleting  a deleteMailConfiguration method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.deleteMailConfiguration(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for deleteMailConfiguration method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/getMailConfiguration", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getMailConfiguration(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while getting  a getMailConfiguration method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.getMailconfiguration(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getMailConfiguration method : ");
		return status;
	}
	
	@RequestMapping(value = "/updateMailNotification", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateMailNotification(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while updating  a updateMailNotification method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnAdmin.updateMailNotification(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user level: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for updateMailNotification method : status:  "+status);
		return status;
	}
	
	@RequestMapping(value = "/viewDefaultConfig", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String viewDefaultConfiguration(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled()){logger.info("viewDefaultConfiguration method called");};
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		
		String status = "";
		try {
			status = mnAdmin.viewDefaultConfiguration();
			if(logger.isInfoEnabled()){logger.info("Successfully got data for default configuration");};
		} catch (Exception e) {
			logger.error("Exception while fetching default configuration: ", e);
			status = "Exception while fetching default configuration:";
		}
		if(logger.isInfoEnabled()){logger.info("Return response for viewDefaultConfiguration method called");};
		return status;
	}
	
	@RequestMapping(value = "/updateDefaultConfig/{configId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateDefaultConfiguration(@PathVariable("configId") String configId,@RequestBody String params,HttpServletRequest request){
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		
		String status = "";
		try {
			if(logger.isInfoEnabled()){logger.info("updateDefaultConfiguration method called");};
			
			
			if(configId!="" || configId!="undefined"){
			status = mnAdmin.updateDefaultConfiguration(configId,params);
			if(logger.isInfoEnabled()){logger.info("Updated default configuration "+status);};
			}
		} catch (Exception e) {
			logger.error("Exception while updating default configuration: ", e);
			status = "Exception while updating default configuration";
		}
		if(logger.isInfoEnabled()){logger.info("Return response for updateDefaultConfiguration method called");};
		return status;
	}
	

	
	@RequestMapping(value = "/fetchUserDetailsBasedLevelRole",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchUserDetailsBasedLevelRole(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchUserDetailsBasedLevelRole method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			status = mnAdmin.fetchUserDetailsBasedLevelRole(param);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details for fetchUserDetailsBasedLevelRole: ", e);
			status = "Exception while student fetching user details in fetchUserDetailsBasedLevelRole";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchUserDetailsBasedLevelRole method: ");
		return status;
	}
	@RequestMapping(value = "/fetchComplaintDetailsForAdmin",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchComplaintDetailsForAdmin(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchComplaintDetailsForAdmin method called: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			status = mnAdmin.fetchComplaintDetailsForAdmin(param);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details for fetchComplaintDetailsForAdmin: ", e);
			status = "Exception while student fetching user details in fetchComplaintDetailsForAdmin";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchComplaintDetailsForAdmin method called: ");
		return status;
	}
	@RequestMapping(value = "/fetchNoteComplaintDetailsForAdmin",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchNoteComplaintDetailsForAdmin(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchNoteComplaintDetailsForAdmin method called: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			status = mnAdmin.fetchNoteComplaintDetailsForAdmin(param);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details for fetchComplaintDetailsForAdmin: ", e);
			status = "Exception while student fetching user details in fetchComplaintDetailsForAdmin";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchNoteComplaintDetailsForAdmin method called: ");
		return status;
	}
	@RequestMapping(value = "/getComplaintNote",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getComplaintNote(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching getComplaintNote method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String note = "";
		try {
			note = mnAdmin.getComplaintNote(param);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details: ", e);
			note = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching getComplaintNote method: ");
		return note;
	}
	@RequestMapping(value = "/actionComplaintNote",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getComplaintDeleteNote(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching getComplaintDeleteNote method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String note = "";
		try {
			note = mnAdmin.getComplaintDeleteNote(param);
		} catch (Exception e) {
			logger.error("Exception while getComplaintDeleteNote: ", e);
			note = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching getComplaintDeleteNote method: ");
		return note;
	}
	
	@RequestMapping(value = "/fetchUserDetailsForChartView",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchUserDetailsForChartView(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchUserDetailsForChartView method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.fetchUserDetailsForChartView(param);
		} catch (Exception e) {
			logger.error("Exception while fetchUserDetailsForChartView user details: ", e);
			status = "Exception while student fetching user details for fetchUserDetailsForChartView";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchUserDetailsForChartView method: ");
		return status;
	}
	
	/* Delete Comment */
	@RequestMapping(value = "/actionCompliantComments", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCommentsBasedUser(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("deleteCommentsBasedUser method: ");
		String note = "";
		try
		{
			mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
			if(logger.isInfoEnabled())
			logger.info("deleteComments method called  ");
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
		    note = mnAdmin.deleteComments(param);
		}
		catch (Exception e)
		{
			logger.error("Exception while Delete Comment For Note : ", e);
			note = "";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for deleteCommentsBasedUser method controller: ");
		return note;
	}
	/* Delete Attachment for crowd only*/
	@RequestMapping(value = "/deleteAttachmentInCrowd/{fileName}/{userId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String deleteAttachmentInCrowd(@PathVariable("fileName") String fileName,@PathVariable("userId") String userId,@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("deleteAttachmentInCrowd method called  userId: "+userId);
		String note = "";
		try
		{
			mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
		    note = mnAdmin.deleteAttachmentInCrowd(param,fileName, userId);
		}
		catch (Exception e)
		{
			logger.error("Exception while deleteAttachmentInCrowd : ", e);
			note = "";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for deleteAttachmentInCrowd method userId: "+userId);
		return note;
	}
	/* Delete Attachment for both note and crowd */
	@RequestMapping(value = "/deleteAttachmentInBothNoteCrowd/{fileName}/{userId}", consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String deleteAttachmentInBothNoteCrowd(@PathVariable("fileName") String fileName,@PathVariable("userId") String userId,@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("deleteAttachmentInBothNoteCrowd method userId: "+userId);
		String note = "";
		try
		{
			mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
		    note = mnAdmin.deleteAttachmentInBothNoteCrowd(param,fileName, userId);
		}
		catch (Exception e)
		{
			logger.error("Exception while deleteAttachmentInBothNoteCrowd For Note : ", e);
			note = "";
		}
	    if(logger.isInfoEnabled())
	    logger.info("Return response deleteAttachmentInBothNoteCrowd method  userId: "+userId);

		return note;
	}
	@RequestMapping(value = "/adminStatusReport",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String adminStatusReport(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching adminStatusReport method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String note = "";
		try {
			note = mnAdmin.adminStatusReport(param);
		} catch (Exception e) {
			logger.error("Exception while adminStatusReport: ", e);
			note = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for adminStatusReport method controller: ");
		return note;
	}
	@RequestMapping(value = "/addMailTemplates",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String addMailTemplates(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching addMailTemplates method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String note = "";
		try {
			note = mnAdmin.addMailTemplates(param);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details: ", e);
			note = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for addMailTemplates method: ");
		return note;
	}
	@RequestMapping(value = "/fetchMailTemplates",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchMailTemplates(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchMailTemplates method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.fetchMailTemplates(param,request);
		} catch (Exception e) {
			logger.error("Exception while updateMailTemplates user details: ", e);
			status = "Exception while student fetching user details for updateMailTemplates";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchMailTemplates method: ");
		return status;
	}
	@RequestMapping(value = "/updateMailTemplates",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateMailTemplates(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching updateMailTemplates method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.updateMailTemplates(param);
		} catch (Exception e) {
			logger.error("Exception while updateMailTemplates user details: ", e);
			status = "Exception while student fetching user details for updateMailTemplates";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching updateMailTemplates method: ");
		return status;
	}
	@RequestMapping(value = "/adminAddSecurityQuestions",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String adminAddSecurityQuestions(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching adminAddSecurityQuestions method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			status = mnAdmin.adminAddSecurityQuestions(param);
		} catch (Exception e) {
			logger.error("Exception while adminAddSecurityQuestions: ", e);
			status = "Exception while adminAddSecurityQuestions";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching adminAddSecurityQuestions method: ");
		return status;
	}
	@RequestMapping(value = "/fetchsecurityQuestionView",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchsecurityQuestionView(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchsecurityQuestionView method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.fetchsecurityQuestionView(param,request);
		} catch (Exception e) {
			logger.error("Exception while fetchsecurityQuestionView: ", e);
			status = "Exception while student fetching user details for fetchsecurityQuestionView";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for while fetching fetchsecurityQuestionView method: ");
		return status;
	}
	@RequestMapping(value = "/updateSecurityQuestions",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateSecurityQuestions(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching updateSecurityQuestions method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.updateSecurityQuestions(param);
		} catch (Exception e) {
			logger.error("Exception while updateSecurityQuestions: ", e);
			status = "Exception while updateSecurityQuestions";
		}
		if(logger.isInfoEnabled())
			logger.info("Return request for while fetching updateSecurityQuestions method: ");
		return status;
	}
	//getting Block and unBlocked user Details
	@RequestMapping(value = "/getBlockedReport", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getComplaintsReport(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while adding  a getComplaintsReport method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String params = decodeJsonString(param);
		params = removeUnWantedChar(params);
		String status = "";
		try
		{
			status = mnAdmin.FetchComplaintReports(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user Complaints details: ", e);
			status = "Exception while fetching user Complaints details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getComplaintsReport method : "+status);
		return status;
	}
	
	//insert and update the blocked user details
	@RequestMapping(value = "/insertBlockedReport", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String insertBlockedReport(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while adding  a insertBlockedReport method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String status = "";
		try 
		{
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnBlockedUsers mnBlockedUsers=mnAdmin.convertJsonToUserObj(param);
			status=mnAdmin.fetchBlockedUser(mnBlockedUsers);
		} catch (Exception e) {
			logger.error("Exception while Updating blocked user details: ", e);
			status = "Exception while Updating blocked user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for insertBlockedReport method : "+status);
		return status;
	}
	//check security question for admin
	@RequestMapping(value = "/checkSecurityQuestion",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String checkSecurityQuestion(@RequestBody String param,HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("checkSecurityQuestion method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String status="";
		try
		{
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String questionId = (String) jsonObject.get("questionId");
			status=mnAdmin.checkSecurityQuestion(questionId);
		}
		catch (Exception e) {
			logger.error("Exception while check Security Question: ", e);
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for checkSecurityQuestion method : ");
		return status;
	}
	//getMnUserDetailsForUserPrefrence Grid Method
	@RequestMapping(value = "/getMnUserDetailsForUserPrefrence",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getMnUserDetailsForUserPrefrence(HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("getMnUserDetailsForUserPrefrence method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String status="";
		try
		{
			status=mnAdmin.getMnUserDetails();
		}
		catch (Exception e) {
			logger.error("Exception while updateSecurityQuestions: ", e);
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for getMnUserDetailsForUserPrefrence method: ");
		return status;
	}
	//add prefrenceflag on MnUserTable
	@RequestMapping(value = "/addUserFlagOnMnUser/{addPreferenceUserId}/{userFlag}",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String addUserFlagOnMnUser(@PathVariable("addPreferenceUserId") String userId,@PathVariable("userFlag") String userFlag, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("addUserFlagOnMnUser method called: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String status="";
		try
		{
			status=mnAdmin.addPrefrenceFlag(userId,userFlag);
		}
		catch (Exception e) {
			logger.error("Exception while updateSecurityQuestions: ", e);
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for addUserFlagOnMnUser method: ");
		return status;
	}
	//get compalintid for crowd complaint delete button block
	@RequestMapping(value = "/crowdComplaintDeleteButtonBlock/{compliantId}",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String crowdComplaintDeleteButtonBlock(@PathVariable("compliantId") String compliantId,HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("get crowdComplaintDeleteButtonBlock method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		String status="";
		try
		{
			status=mnAdmin.crowdComplaintDeleteButtonBlock(compliantId);
		}
		catch (Exception e) {
			logger.error("Exception while crowdComplaintDeleteButtonBlock: ", e);
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for crowdComplaintDeleteButtonBlock method: ");
		return status;
	}
	//getting uploaded Files for Help page
			@RequestMapping(value = "/getUploadedFilesForHelp",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
			public String getUploadedFilesForHelp(HttpServletRequest request)
			{
				if(logger.isInfoEnabled())
					logger.info("get getUploadedFilesForHelp method: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				String status="";
				try
				{
					status=mnAdmin.getUploadedFilesForHelp();
				}
				catch (Exception e) {
					logger.error("Exception while getUploadedFilesForHelp files: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for get getUploadedFilesForHelp method: ");
				return status;
			}
		    @RequestMapping(value = "/fetchUserDetailsBasedOnReportLevel",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
		    @ResponseBody
		    public String fetchUserDetailsBasedOnReportLevel(@RequestBody String param, HttpServletRequest request)
		   {
		        if(logger.isInfoEnabled())
		        logger.info("while fetching fetchUserDetailsBasedOnReportLevel method: ");
		        mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		        param = decodeJsonString(param);
		        param = removeUnWantedChar(param);
		        //MnUsers user = mnUser.convertJsonToUserObj(param);
		        String status = "";
		        try {
		            status = mnAdmin.fetchUserDetailsBasedOnReportLevel(param);
		        } catch (Exception e) {
		            logger.error("Exception while fetching user details for fetchUserDetailsBasedOnReportLevel: ", e);
		            status = "Exception while fetching user details in fetchUserDetailsBasedOnReportLevel";
		        }
		        if(logger.isInfoEnabled())
					logger.info("Return response for fetchUserDetailsBasedOnReportLevel method: ");
		        return status;
		    }
		  //getting uploaded Files for Admin
			@RequestMapping(value = "/getUploadedFiles",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
			public String getUploadedFilesForAdmin(HttpServletRequest request)
			{
				if(logger.isInfoEnabled())
					logger.info("getUploadedFilesForAdmin method: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				String status="";
				try
				{
					status=mnAdmin.getUploadedFilesForAdmin();
				}
				catch (Exception e) {
					logger.error("Exception while getting uploaded files: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for getUploadedFilesForAdmin method: ");
				return status;
			}
			// Delete uploaded files
			@RequestMapping(value = "/deleteUploadedFile/{fileId}",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
			public String deleteUploadedFile(@PathVariable("fileId") String fileId,HttpServletRequest request)
			{
				if(logger.isInfoEnabled())
					logger.info("get deleteUploadedFile method: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				String status="";
				try
				{
					String filePath=mnAdmin.deleteUploadedFile(fileId);
					String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
					String FullPath=filePath.replace(name, "");
					AmazonUploadable amazonFileDelete=(AmazonUploadable)SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileDelete);
					String deleteStatus=amazonFileDelete.deleteFile(filePath.replaceAll(filePath.substring(filePath.lastIndexOf("/"), filePath.length()), ""), name);
					status="success";
					
				}
				catch (Exception e) {
					logger.error("Exception while delete Uploaded File: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for deleteUploadedFile method: ");
				return status;
			}
			//Amazon file upload for Admin
			@RequestMapping(value = "/uploadFiles/{userTokens}",method = RequestMethod.POST)
			@ResponseBody
			public String fileUploadForAdmin(@PathVariable("userTokens") String userTokens,HttpServletRequest request, HttpServletResponse response)
			{
				if(logger.isInfoEnabled())
					logger.info("fileUploadForAdmin for admin method: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				StringBuffer jsonStr = new StringBuffer("[");
				try
				{
					request.setAttribute("userId", "admin");
					AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
					String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
					String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
					String type = "";
					int mid = name.lastIndexOf(".");
					type = name.substring(mid + 1, name.length());
					type= type.toLowerCase();
					jsonStr.append("{\"fileName\" : \"" + name + "\",");
					jsonStr.append("\"fileType\" : \"" + type + "\",");
					jsonStr.append("\"filePath\" : \"" + filePath + "\"");
					jsonStr.append("}]");
					response.setStatus(200);
				}
				catch (Exception e) {
					logger.error("Exception while uploading files: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for fileUploadForAdmin method : ");
				return jsonStr.toString();
			}
			// insert uploaded files
			@RequestMapping(value = "/insertFileDetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
			public String insertFileDetails(@RequestBody String param,HttpServletRequest request)
			{
				if(logger.isInfoEnabled())
					logger.info("get insertFileDetails method called: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				String status="";
				try
				{
					param = decodeJsonString(param);
					param = removeUnWantedChar(param);
					status=mnAdmin.convertJsonToStringForfileDetails(param);
				}
				catch (Exception e) {
					logger.error("Exception while delete Uploaded File: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for insertFileDetails method : ");
				return status;
			}
			//checkBlockedUserForAddPreference
			@RequestMapping(value = "/checkBlockedUserForAddPreference/{addPreferenceUserId}",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
			public String checkBlockedUserForAddPreference(@PathVariable("addPreferenceUserId") String userId, HttpServletRequest request)
			{
				if(logger.isInfoEnabled())
					logger.info("get checkBlockedUserForAddPreference method: ");
				mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
				String status="";
				try
				{
					status=mnAdmin.checkBlockedUserForAddPreference(userId);
				}
				catch(Exception e)
				{
					logger.error("Exception while checkBlockedUserForAddPreference: ", e);
				}
				if(logger.isInfoEnabled())
					logger.info("Return response for get checkBlockedUserForAddPreference method: ");
				return status;
			}
			@RequestMapping(value = "/fetchLoginDetailsForChartView",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
			@ResponseBody
	public String fetchLoginDetailsForChartView(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchUserDetailsForChartView method: ");
		mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnAdmin.fetchLoginDetailsForChartView(param);
		} catch (Exception e) {
			logger.error("Exception while fetchUserDetailsForChartView user details: ", e);
			status = "Exception while student fetching user details for fetchUserDetailsForChartView";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for fetchUserDetailsForChartView method: ");
		return status;
	}

		    @RequestMapping(value = "/insertBlogDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
		    @ResponseBody
		    public String insertBlogDetails(@RequestBody String param, HttpServletRequest request)
		    {
		        if(logger.isInfoEnabled())
		            logger.info("insertBlogDetails method called");
		        mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		        param=decodeJsonString(param);
		        param=removeUnWantedChar(param);
		        String status="";
		        try
		        {
		            status=mnAdmin.insertBlogDetails(param);
		        }
		        catch(Exception e)
		        {
		            logger.error("Exception for insertBlogDetails ", e);
		        }
		        if(logger.isInfoEnabled())
		            logger.info("Return response for insertBlogDetails method");
		        return status;
		    }
		    @RequestMapping(value="getBlogDetails",consumes="application/json",produces="application/json",method=RequestMethod.POST)
		    @ResponseBody
		    public String getBlogDetails(@RequestBody String param,HttpServletRequest request)
		    {
		        if(logger.isInfoEnabled())
		            logger.info("getBlogDetails method called");
		        mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		        param=decodeJsonString(param);
		        param=removeUnWantedChar(param);
		        String status="";
		        try
		        {
		            status=mnAdmin.getBlogDetails(param);
		        }
		        catch(Exception e)
		        {
		            logger.error("Exception for getBlogDetails ", e);
		        }
		        if(logger.isInfoEnabled())
		            logger.info("Return response for getBlogDetails method");
		        return status;
		        
		    }
		    @RequestMapping(value = "/updateBlogDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
		    @ResponseBody
		    public String updateBlogDetails(@RequestBody String param, HttpServletRequest request)
		    {
		        if(logger.isInfoEnabled())
		            logger.info("updateBlogDetails method called");
		        mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		        param=decodeJsonString(param);
		        param=removeUnWantedChar(param);
		        String status="";
		        try
		        {
		            status=mnAdmin.updateBlogDetails(param);
		        }
		        catch(Exception e)
		        {
		            logger.error("Exception for updateBlogDetails ", e);
		        }
		        if(logger.isInfoEnabled())
		            logger.info("Return response for updateBlogDetails method");
		        return status;
		    }
		    @RequestMapping(value = "/deleteBlogDetails/{blogId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
		    @ResponseBody
		    public String deleteBlogDetails(@PathVariable("blogId") String blogId, HttpServletRequest request)
		    {
		        if(logger.isInfoEnabled())
		            logger.info("deleteBlogDetails method called");
		        mnAdmin = (MnAdmin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNADMIN);
		        String status="";
		        try
		        {
		            status=mnAdmin.deleteBlogDetails(blogId);
		        }
		        catch(Exception e)
		        {
		            logger.error("Exception for deleteBlogDetails ", e);
		        }
		        if(logger.isInfoEnabled())
		            logger.info("Return response for deleteBlogDetails method");
		        return status;
		    }
		    

}

