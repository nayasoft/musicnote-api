package com.musicnotes.apis.mail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.log4j.Logger;

import com.sun.mail.imap.protocol.FLAGS;

public class DeleteMail extends AMailCommon{

	String folderType;
	String portNo;
	String host;
	String userName;
	String password;
	Logger logger;
	String mode;
//	String folderType = "Inbox";
//	String portNo = "993";
//	String host = "imap.gmail.com";
//	String userName = "naya.qatesting@gmail.com";
//	String password = "nayatesting";

	public DeleteMail() {
				logger = Logger.getLogger(DeleteMail.class);
	}
	
	public void initialize(String folderType, String portNo, String host,String userName, String password,String mode)
	{
		
		
		this.folderType = folderType;
		this.portNo = portNo;
		this.host = host;
		this.userName = userName;
		this.password = password;
		this.mode=mode;
		
		logger.info("send mail : folderType : " + folderType + "portNo :" + portNo + " host : " + host +" mode : " + mode +" userName :" +userName +" password :  " +password);
	}

	/**
	 * @param args
	 */
	public String deleteMail(String mailNo) throws IOException {

		String deleteMailFlag = "false";
		
		String mode="incoming";
		try {
			Properties props=super.loadMailProperties(portNo, host,mode);
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");
			store.connect(host, userName, password);

			Folder folder = store.getFolder(folderType);
			folder.open(Folder.READ_WRITE);
			if (folder.getFullName().equalsIgnoreCase("inbox")) {
				if(logger.isDebugEnabled())
					logger.debug("No. of unRead Messages : "
						+ folder.getUnreadMessageCount());
			}
			Message messages[] = folder.getMessages();

			System.out.println(Integer.parseInt(mailNo) - 1 + ": "
					+ messages[Integer.parseInt(mailNo) - 1].getFrom()[0] + "	"
					+ messages[Integer.parseInt(mailNo) - 1].getSubject());

			// Mark as deleted if appropriate
			messages[Integer.parseInt(mailNo) - 1].setFlag(FLAGS.Flag.DELETED,
					true);

			// Close connection
			folder.close(true);
			store.close();
			deleteMailFlag = "true";

		} catch (NoSuchProviderException e) {
			deleteMailFlag = "false";
			e.printStackTrace();
		} catch (MessagingException e) {
			deleteMailFlag = "false";
			e.printStackTrace();
		}
		return deleteMailFlag;
	}

}
