package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnBlockedUsers;


public class MnBlockedUsersConverter implements Converter<MnBlockedUsers, DBObject>
{

	@Override
	public DBObject convert(MnBlockedUsers source)
	{
		DBObject dbo = new BasicDBObject();
		dbo.put("blockedId", source.getBlockedId());
		dbo.put("userName", source.getUserName());
		dbo.put("userId", source.getUserId());
		dbo.put("adminId", source.getAdminId());
		dbo.put("blockedLevel", source.getBlockedLevel());
		dbo.put("crowdShareFlag", source.getCrowdShareFlag());
		dbo.put("numberOfDays", source.getNumberOfDays());
		dbo.put("blockedDate", source.getBlockedDate());
		dbo.put("endBlockedDate",source.getEndBlockedDate());
		dbo.put("status", source.getStatus());
		return dbo;
	}

}
