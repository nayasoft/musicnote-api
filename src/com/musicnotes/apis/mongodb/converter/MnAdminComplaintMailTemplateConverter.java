package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAdminComplaintMailTemplate;

public class MnAdminComplaintMailTemplateConverter implements Converter<MnAdminComplaintMailTemplate,DBObject> {

	@Override
	public DBObject convert(MnAdminComplaintMailTemplate adminComplaintMailTemplate) 
	{
		DBObject dbo=new BasicDBObject();
		dbo.put("templateId", adminComplaintMailTemplate.getTemplateId());
		dbo.put("templateName",adminComplaintMailTemplate.getTemplateName());
		dbo.put("templateText",adminComplaintMailTemplate.getTemplateText());
		dbo.put("status",adminComplaintMailTemplate.getStatus());
		
		return dbo;
	}

}
