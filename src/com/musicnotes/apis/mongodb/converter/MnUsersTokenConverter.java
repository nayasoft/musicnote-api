package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnUsersToken;

public class MnUsersTokenConverter implements Converter<MnUsersToken,DBObject> {

	@Override
	public DBObject convert(MnUsersToken source) {
	    DBObject dbo = new BasicDBObject();
	    dbo.put("userId", source.getUserId());
	    dbo.put("tokens", source.getTokens());
	    dbo.put("loginTime", source.getLoginTime());
	    dbo.put("createdDateTime", source.getCreatedDateTime());
	    dbo.put("Ipaddress", source.getIpaddress());
	    return dbo;
	
	  }

}
