package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnVoteViewCount;

public class MnVoteViewCountConverter implements Converter<MnVoteViewCount, DBObject>{
		@Override
		public DBObject convert(MnVoteViewCount mnVoteViewCount) {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("countId", mnVoteViewCount.getCountId());
			dbObject.put("listId", mnVoteViewCount.getListId());
			dbObject.put("noteId", mnVoteViewCount.getNoteId());
			dbObject.put("vote", mnVoteViewCount.getVote());
			dbObject.put("viewed", mnVoteViewCount.getViewed());
			dbObject.put("adminVote", mnVoteViewCount.getAdminVote());
			dbObject.put("status", mnVoteViewCount.getStatus());
			
			return dbObject;
		}
	}


