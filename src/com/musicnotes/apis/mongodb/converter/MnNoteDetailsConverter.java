package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnNoteDetails;

public class MnNoteDetailsConverter implements Converter<MnNoteDetails, DBObject> {

	@Override
	public DBObject convert(MnNoteDetails mnNoteDetails) {
	
		DBObject dbObject = new BasicDBObject();
		
		dbObject.put("_id", mnNoteDetails.get_id());
		dbObject.put("listId", mnNoteDetails.getListId());
		dbObject.put("listName", mnNoteDetails.getListName());
		dbObject.put("listType", mnNoteDetails.getListType());
		dbObject.put("userId", mnNoteDetails.getUserId());
		dbObject.put("noteAccess", mnNoteDetails.getNoteAccess());
		dbObject.put("defaultNote", mnNoteDetails.isDefaultNote());
		dbObject.put("enableDisableFlag", mnNoteDetails.isEnableDisableFlag());
		dbObject.put("sharedIndividuals", mnNoteDetails.getSharedIndividuals());
		dbObject.put("sharedGroups", mnNoteDetails.getSharedGroups());
		dbObject.put("shareAllContactFlag", mnNoteDetails.isShareAllContactFlag());
		dbObject.put("status", mnNoteDetails.getStatus());
		dbObject.put("listOwnerStatus","A");
		
		dbObject.put("noteId", mnNoteDetails.getNoteId());
	    dbObject.put("noteName", mnNoteDetails.getNoteName());
	    dbObject.put("startDate", mnNoteDetails.getStartDate());
	    dbObject.put("notesMembers", mnNoteDetails.getNotesMembers());
	    dbObject.put("noteGroups", mnNoteDetails.getNoteGroups());
	    dbObject.put("noteSharedAllContact", mnNoteDetails.isNoteSharedAllContact());
	    dbObject.put("noteSharedAllContactMembers", mnNoteDetails.getNoteSharedAllContactMembers());
	    dbObject.put("ownerNoteStatus", mnNoteDetails.getOwnerNoteStatus());
	    dbObject.put("tag", mnNoteDetails.getTag());
	    dbObject.put("remainders", mnNoteDetails.getRemainders());
	    dbObject.put("vote", mnNoteDetails.getVote());
	    dbObject.put("links", mnNoteDetails.getLinks());
	    dbObject.put("endDate", mnNoteDetails.getEndDate());
	    dbObject.put("dueDate", mnNoteDetails.getDueDate());
	    dbObject.put("dueTime", mnNoteDetails.getDueTime());
	    dbObject.put("access", mnNoteDetails.getAccess());
	    dbObject.put("attachFilePath", mnNoteDetails.getAttachFilePath());
	    dbObject.put("description", mnNoteDetails.getDescription());
	    dbObject.put("comments", mnNoteDetails.getComments());
	    dbObject.put("pcomments", mnNoteDetails.getPcomments());
	    dbObject.put("copyToMember", mnNoteDetails.getCopyToMember());
	    dbObject.put("copyToGroup", mnNoteDetails.getCopyToGroup());
	    dbObject.put("copyToAllContact", mnNoteDetails.isCopyToAllContact());
	    dbObject.put("copyFromMember", mnNoteDetails.getCopyFromMember());
	    dbObject.put("publicUser", mnNoteDetails.getPublicUser());
	    dbObject.put("publicDate", mnNoteDetails.getPublicDate());
	    dbObject.put("shareType", mnNoteDetails.getShareType());
	    dbObject.put("privateTags", mnNoteDetails.getPrivateTags());
	    dbObject.put("privateAttachFilePath", mnNoteDetails.getPrivateAttachFilePath());
		return dbObject;
	}

}
