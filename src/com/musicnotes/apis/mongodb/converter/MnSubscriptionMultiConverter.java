package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnSubscriptionMulti;

public class MnSubscriptionMultiConverter implements Converter<MnSubscriptionMulti, DBObject>{

	@Override
	public DBObject convert(MnSubscriptionMulti source) {
		DBObject dbo = new BasicDBObject();
		
		dbo.put("Id", source.getId());
		dbo.put("userId", source.getUserId());
		dbo.put("subId", source.getSubId());
		dbo.put("emailId", source.getEmailId());
		dbo.put("startDate", source.getStartDate());
		dbo.put("endDate", source.getEndDate());
		dbo.put("paidUserId", source.getPaidUserId());
		dbo.put("status", source.getStatus());
		
		return dbo;
		
	}

}