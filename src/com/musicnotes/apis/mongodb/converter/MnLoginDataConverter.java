package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnLoginData;

public class MnLoginDataConverter implements Converter<MnLoginData,DBObject> {

	@Override
	public DBObject convert(MnLoginData source) {
	    DBObject dbo = new BasicDBObject();
	    dbo.put("userId", source.getUserId());
	    dbo.put("inTime", source.getInTime());
	    dbo.put("outTime", source.getOutTime());
	    return dbo;
	
	  }

}
