package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnBlog;

public class MnBlogConverter implements Converter<MnBlog,DBObject> 
{

	@Override
	public DBObject convert(MnBlog source) 
	{
		DBObject dbo = new BasicDBObject();
		dbo.put("adminId", source.getAdminId());
		dbo.put("blogId", source.getBlogId());
		dbo.put("blogTitle", source.getBlogTitle());
		dbo.put("blogDescription", source.getBlogDescription());
		dbo.put("blogDate", source.getBlogDate());
		dbo.put("status", source.getStatus());
		dbo.put("tempDate", source.getTempDate());
		return dbo;
	}

}
