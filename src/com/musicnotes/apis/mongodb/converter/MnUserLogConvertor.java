package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnUserLogs;

public class MnUserLogConvertor implements Converter<MnUserLogs, DBObject>{

	@Override
	public DBObject convert(MnUserLogs mnUserLogs) {
		
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", mnUserLogs.getUserId());
		dbObject.put("version", mnUserLogs.getVersion());
		dbObject.put("osName", mnUserLogs.getOsName());
		dbObject.put("loginTime", mnUserLogs.getLoginTime());
		dbObject.put("logoutTime", mnUserLogs.getLogoutTime());
		dbObject.put("browserNameVersion", mnUserLogs.getBrowserNameVersion());
		dbObject.put("screenSize", mnUserLogs.getScreenSize());
		return dbObject;
	}

}