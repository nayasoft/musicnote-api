package com.musicnotes.apis.mongodb.converter;
import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnFeedbacks;

public class MnUserFeedbackConverter implements Converter<MnFeedbacks, DBObject> {

	public DBObject convert(MnFeedbacks user)
	{
		DBObject dbo = new BasicDBObject();

		dbo.put("userId", user.getUserId());
		dbo.put("userFirstName", user.getUserFirstName());
		dbo.put("userLastName", user.getUserLastName());
		dbo.put("emailId", user.getUserMailId());
		dbo.put("description", user.getUserFeedback());
		dbo.put("feedbackType", user.getFeedbackType());
		dbo.put("status", user.getStatus());
		dbo.put("lastUpdate", user.getDate());
		return dbo;
	}
}