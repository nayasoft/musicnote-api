package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnTag;

public class MnTagConverter implements Converter<MnTag, DBObject>{

	@Override
	public DBObject convert(MnTag mnTag) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("tagId", mnTag.getTagId());
		dbObject.put("tagName", mnTag.getTagName());
		dbObject.put("userId", mnTag.getUserId());
		dbObject.put("createDate", mnTag.getCreateDate());
		dbObject.put("endDate", mnTag.getEndDate());
		dbObject.put("status", mnTag.getStatus());

		return dbObject;
	}

}
