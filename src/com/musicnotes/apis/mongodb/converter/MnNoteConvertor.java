package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnList;

public class MnNoteConvertor implements Converter<MnList, DBObject> {
		@Override
		public DBObject convert(MnList mnList) {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("listId", mnList.getListId());
			dbObject.put("listName", mnList.getListName());
			dbObject.put("listType", mnList.getListType());
			dbObject.put("userId", mnList.getUserId());
			dbObject.put("noteAccess", mnList.getNoteAccess());
			dbObject.put("defaultNote", mnList.isDefaultNote());
			dbObject.put("enableDisableFlag", mnList.isEnableDisableFlag());
			dbObject.put("sharedIndividuals", mnList.getSharedIndividuals());
			dbObject.put("sharedGroups", mnList.getSharedGroups());
			dbObject.put("shareAllContactFlag", mnList.isShareAllContactFlag());
			dbObject.put("status", mnList.getStatus());
			dbObject.put("listOwnerStatus","A");
			dbObject.put("mnNotesDetails", mnList.getMnNotesDetails());

			return dbObject;
		}

}
