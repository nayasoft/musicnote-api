package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnUsers;

public class MnUserConverter implements Converter<MnUsers, DBObject>
{

	public DBObject convert(MnUsers source)
	{
		DBObject dbo = new BasicDBObject();

		dbo.put("userId", source.getUserId());
		dbo.put("userName", source.getUserName());
		dbo.put("password", source.getPassword());
		dbo.put("userFirstName", source.getUserFirstName());
		dbo.put("userLastName", source.getUserLastName());
		dbo.put("emailId", source.getEmailId());
		dbo.put("userRole", source.getUserRole());
		dbo.put("contactNumber", source.getContactNumber());
		dbo.put("emergencyContactName", source.getEmergencyContactName());
		dbo.put("emergencyContactNumber", source.getEmergencyContactNumber());
		dbo.put("followers", source.getFollowers());
		dbo.put("notificationFlag", source.getNotificationFlag());
		dbo.put("noteCreateBasedOn", source.getNoteCreateBasedOn());
		dbo.put("friends", source.getFriends());
		dbo.put("publicShareWarnMsgFlag",source.isPublicShareWarnMsgFlag());
		dbo.put("startDate", source.getStartDate());
		dbo.put("status", source.getStatus());
		dbo.put("endDate", source.getEndDate());
		dbo.put("instrument", source.getInstrument());
		dbo.put("skillLevel", source.getSkillLevel());
		dbo.put("filePath", source.getFilePath());
		dbo.put("favoriteMusic", source.getFavoriteMusic());
		dbo.put("requestPending", source.isRequestPending());
		dbo.put("timeZone", source.getTimeZone());
		dbo.put("termscheck", source.getTermscheck());
		dbo.put("userLevel", source.getUserLevel());
		dbo.put("uploadedSize", source.getUploadedSize());
		dbo.put("requestedUser", source.getRequestedUser());
		dbo.put("multipleUsersRequest",source.isMultipleUsersRequest());
		dbo.put("adminFlag",source.isAdminFlag());
		dbo.put("mailCheckFlag",source.isMailCheckFlag());
		dbo.put("mailCheckId", source.getMailCheckId());
		dbo.put("addedByUserId", source.getAddedByUserId());
		dbo.put("addedUserStatus", source.getAddedUserStatus());
		dbo.put("displayUserName", source.getDisplayUserName());
		dbo.put("cropFlag", source.isCropFlag());
		dbo.put("limitedSize", source.getLimitedSize());
		dbo.put("adminNotificationFlag", source.isAdminNotificationFlag());
		
		dbo.put("addUserFlag", source.isAddUserFlag());
		dbo.put("url",source.getUrl());
		dbo.put("description", source.getDescription());
		dbo.put("facebookId", source.getFacebookId());
		dbo.put("fbLogoutFlag", source.isFbLogoutFlag());
    	return dbo;
	}

}
