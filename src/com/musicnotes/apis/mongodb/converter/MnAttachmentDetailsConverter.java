package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnComments;

public class MnAttachmentDetailsConverter implements Converter<MnAttachmentDetails, DBObject>{
	@Override
	public DBObject convert(MnAttachmentDetails mnAttachmentDetails) {
		
		DBObject dbObject = new BasicDBObject();
		
		dbObject.put("attachId", mnAttachmentDetails.getAttachId());
		dbObject.put("fileId", mnAttachmentDetails.getFileId());
		dbObject.put("userId", mnAttachmentDetails.getUserId());
		dbObject.put("listId", mnAttachmentDetails.getListId());
		dbObject.put("noteId", mnAttachmentDetails.getNoteId());
		dbObject.put("aDate", mnAttachmentDetails.getaDate());
		dbObject.put("aTime",mnAttachmentDetails.getaTime());
		dbObject.put("status", mnAttachmentDetails.getStatus());
		dbObject.put("endDate", mnAttachmentDetails.getEndDate());
		dbObject.put("endTime", mnAttachmentDetails.getEndTime());
		dbObject.put("shareInd", mnAttachmentDetails.isShareInd());
		dbObject.put("shareCon", mnAttachmentDetails.isShareCon());
		dbObject.put("shareGrp", mnAttachmentDetails.isShareGrp());
		dbObject.put("fileName", mnAttachmentDetails.getFileName());
		dbObject.put("currentTime", mnAttachmentDetails.getCurrentTime());
		dbObject.put("fileUploadName", mnAttachmentDetails.getFileUploadName());
		dbObject.put("uploadDate", mnAttachmentDetails.getUploadDate());
		dbObject.put("fullPath", mnAttachmentDetails.getFullPath());
		
		return dbObject;
	}
}
