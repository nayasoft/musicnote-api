package com.musicnotes.apis.domain;

public class MnBlog 
{
private Integer blogId;
private String blogTitle;
private String blogDescription;
private String blogDate;
private String status;
private Integer adminId;
private String tempDate;
public Integer getBlogId() {
	return blogId;
}
public void setBlogId(Integer blogId) {
	this.blogId = blogId;
}
public String getBlogTitle() {
	return blogTitle;
}
public void setBlogTitle(String blogTitle) {
	this.blogTitle = blogTitle;
}

public String getBlogDate() {
	return blogDate;
}
public void setBlogDate(String blogDate) {
	this.blogDate = blogDate;
}
public Integer getAdminId() {
	return adminId;
}
public void setAdminId(Integer adminId) {
	this.adminId = adminId;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getBlogDescription() {
	return blogDescription;
}
public void setBlogDescription(String blogDescription) {
	this.blogDescription = blogDescription;
}

@Override
public String toString() {
	return "MnBlog [blogId=" + blogId + ", blogTitle=" + blogTitle
			+ ", blogDescription=" + blogDescription + ", blogDate=" + blogDate
			+ ", status=" + status + ", adminId=" + adminId + ", tempDate="
			+ tempDate + "]";
}
public String getTempDate() {
	return tempDate;
}
public void setTempDate(String tempDate) {
	this.tempDate = tempDate;
}

}
