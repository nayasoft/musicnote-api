package com.musicnotes.apis.domain;

public class MnTagDetails {
	private String _id;

	private Integer tagId;
	private Integer userId;
	private Integer listId;
	private Integer noteId;
	private String listType;
	private String tagedDate;
	private String endDate;
	private String status;
	
	@Override
	public String toString() {
		return "MnTagDetails [_id:" + get_id() + ", tagId:" + getTagId()+ ", userId:" + getUserId()+ ", listId:" + getListId()+ ", noteId:" + getNoteId() + ", listType:" + getListType() + ", tagedDate:" + getTagedDate() + ", endDate:" + getEndDate() + ", status:" + getStatus() + "]";
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getTagId() {
		return tagId;
	}
	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getListId() {
		return listId;
	}
	public void setListId(Integer listId) {
		this.listId = listId;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setTagedDate(String tagedDate) {
		this.tagedDate = tagedDate;
	}

	public String getTagedDate() {
		return tagedDate;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getListType() {
		return listType;
	}
}
