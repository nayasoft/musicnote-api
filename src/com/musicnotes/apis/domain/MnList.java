package com.musicnotes.apis.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MnList {
	private String _id;

	private Integer listId;
	private String listName;
	private String listType;
	private Integer userId;
	private String noteAccess;
	private boolean defaultNote;
	private boolean enableDisableFlag;
	private List<Integer> sharedIndividuals=new ArrayList<Integer>();
	private List<Integer> sharedGroups=new ArrayList<Integer>();
	private boolean shareAllContactFlag;
	private String status;
	private String listOwnerStatus;
	
	private Set<String> mnNotesDetails=new HashSet<String>(0);
	
	private List<String> mnNotesDetailsList=new ArrayList<String>();
	@Override
	public String toString() {
		return "MnList [_id:" + get_id() + ", listId:" + getListId()+ ", listName:" + getListName()+ ", listType:" + getListType()+ ", noteAccess:" + getNoteAccess() + ", defaultNote:" + isDefaultNote()+ ", enableDisableFlag:" + isEnableDisableFlag() + ", userId:" + getUserId() + ", enableDisableFlag:" + isEnableDisableFlag() + ", sharedIndividuals:" + getSharedIndividuals().toString() + ", sharedGroups:" + getSharedGroups().toString() + ", shareAllContactFlag:" + isShareAllContactFlag() + ", listOwnerStatus:"+getListOwnerStatus()+" ,mnNotesDetails:"+mnNotesDetails.toString()+"]";
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String id) {
		_id = id;
	}

	public Integer getListId() {
		return listId;
	}

	public void setListId(Integer listId) {
		this.listId = listId;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<String> getMnNotesDetails() {
		return mnNotesDetails;
	}

	public void setMnNotesDetails(Set<String> mnNotesDetails) {
		this.mnNotesDetails = mnNotesDetails;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getListType() {
		return listType;
	}

	public void setNoteAccess(String noteAccess) {
		this.noteAccess = noteAccess;
	}

	public String getNoteAccess() {
		return noteAccess;
	}

	public void setDefaultNote(boolean defaultNote) {
		this.defaultNote = defaultNote;
	}

	public boolean isDefaultNote() {
		return defaultNote;
	}

	public boolean isEnableDisableFlag() {
		return enableDisableFlag;
	}

	public void setEnableDisableFlag(boolean enableDisableFlag) {
		this.enableDisableFlag = enableDisableFlag;
	}

	public boolean isShareAllContactFlag() {
		return shareAllContactFlag;
	}

	public void setShareAllContactFlag(boolean shareAllContactFlag) {
		this.shareAllContactFlag = shareAllContactFlag;
	}

	public List<Integer> getSharedIndividuals() {
		return sharedIndividuals;
	}

	public void setSharedIndividuals(List<Integer> sharedIndividuals) {
		this.sharedIndividuals = sharedIndividuals;
	}

	public List<Integer> getSharedGroups() {
		return sharedGroups;
	}

	public void setSharedGroups(List<Integer> sharedGroups) {
		this.sharedGroups = sharedGroups;
	}

	public void setMnNotesDetailsList(List<String> mnNotesDetailsList) {
		this.mnNotesDetailsList = mnNotesDetailsList;
	}

	public List<String> getMnNotesDetailsList() {
		return mnNotesDetailsList;
	}

	public void setListOwnerStatus(String listOwnerStatus) {
		this.listOwnerStatus = listOwnerStatus;
	}

	public String getListOwnerStatus() {
		return listOwnerStatus;
	}
}
