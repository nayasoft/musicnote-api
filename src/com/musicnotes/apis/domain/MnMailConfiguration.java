package com.musicnotes.apis.domain;

public class MnMailConfiguration {

	private Integer mailNo;
	private Integer userId;
	private String userLevel;
	private String intervel;
	private String startDate;
	private String endDate;
	private String status;
	private String amountDays;
	private String mailMessage;
	private String mailSubject;
	private String userSubject;
	private String userMessage;
	
	@Override
	public String toString() {
		return "MnMailConfiguration [endDate=" + endDate + ", intervel="
				+ intervel + ", mailNo=" + mailNo + ", message=" + mailMessage
				+ ", startDate=" + startDate + ", status=" + status
				+ ", subject=" + mailSubject + ", userId=" + userId
				+ ", userLevel=" + userLevel + ",userSubject="+userSubject+",userMessage="+userMessage+"]";
	}

	public Integer getMailNo() {
		return mailNo;
	}

	public void setMailNo(Integer mailNo) {
		this.mailNo = mailNo;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserLevel() {
		return userLevel;
	}
	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}
	public String getIntervel() {
		return intervel;
	}
	public void setIntervel(String intervel) {
		this.intervel = intervel;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setAmountDays(String amountDays) {
		this.amountDays = amountDays;
	}

	public String getAmountDays() {
		return amountDays;
	}

	public void setMailMessage(String mailMessage) {
		this.mailMessage = mailMessage;
	}

	public String getMailMessage() {
		return mailMessage;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setUserSubject(String userSubject) {
		this.userSubject = userSubject;
	}

	public String getUserSubject() {
		return userSubject;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public String getUserMessage() {
		return userMessage;
	}
	
	
	
}
