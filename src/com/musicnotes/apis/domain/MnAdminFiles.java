package com.musicnotes.apis.domain;

public class MnAdminFiles {
	private Integer adminFileId;
	private String adminFileName;
	private String title;
	private String discription;
	private String status;
	private String uploadDate;
	private String deleteDate;
	private String adminFileType;
	private String adminFilePath;
	public Integer getAdminFileId() {
		return adminFileId;
	}
	public void setAdminFileId(Integer adminFileId) {
		this.adminFileId = adminFileId;
	}
	public String getAdminFileName() {
		return adminFileName;
	}
	public void setAdminFileName(String adminFileName) {
		this.adminFileName = adminFileName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	public String getAdminFileType() {
		return adminFileType;
	}
	public void setAdminFileType(String adminFileType) {
		this.adminFileType = adminFileType;
	}
	public String getAdminFilePath() {
		return adminFilePath;
	}
	public void setAdminFilePath(String adminFilePath) {
		this.adminFilePath = adminFilePath;
	}
}
	
