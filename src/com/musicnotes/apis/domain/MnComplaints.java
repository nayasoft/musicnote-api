package com.musicnotes.apis.domain;

import java.util.List;

public class MnComplaints {
	
	private Integer compliantId;
	private String reportLevel;
	private String commentId;
	private String userComplaints;
	private String userMailId;
	private String status;
	private String userName;
	private String userFirstName;
	private String userLastName;
	private String noteName;
	private Integer complaintUserId;
	private String date;
	private String listId;
	private String noteId;
	private String ownerId;
	private String ownerUserName;
	private String SharedByUserName;
	private String ownerMailId;
	private List<String> commentsList;
	private String reportPage;
	private String adminComment;
	private String adminAction;
	private String mailTemplateId;
	
	
	
	public String getUserComplaints() {
		return userComplaints;
	}
	public void setUserComplaints(String userComplaints) {
		this.userComplaints = userComplaints;
	}
	public String getUserMailId() {
		return userMailId;
	}
	public void setUserMailId(String userMailId) {
		this.userMailId = userMailId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNoteName() {
		return noteName;
	}
	public void setNoteName(String noteName) {
		this.noteName = noteName;
	}
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerUserName() {
		return ownerUserName;
	}
	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}
	public List getCommentsList() {
		return commentsList;
	}
	public void setCommentsList(List commentsList) {
		this.commentsList = commentsList;
	}
	public String getReportLevel() {
		return reportLevel;
	}
	public void setReportLevel(String reportLevel) {
		this.reportLevel = reportLevel;
	}
	public String getCommentId() {
		return commentId;
	}
	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReportPage() {
		return reportPage;
	}
	public void setReportPage(String reportPage) {
		this.reportPage = reportPage;
	}
	public String getOwnerMailId() {
		return ownerMailId;
	}
	public void setOwnerMailId(String ownerMailId) {
		this.ownerMailId = ownerMailId;
	}
	/**
	 * @return the adminComment
	 */
	public String getAdminComment() {
		return adminComment;
	}
	/**
	 * @param adminComment the adminComment to set
	 */
	public void setAdminComment(String adminComment) {
		this.adminComment = adminComment;
	}
	/**
	 * @return the adminAction
	 */
	public String getAdminAction() {
		return adminAction;
	}
	/**
	 * @param adminAction the adminAction to set
	 */
	public void setAdminAction(String adminAction) {
		this.adminAction = adminAction;
	}
	
	/**
	 * @return the compliantId
	 */
	public Integer getCompliantId() {
		return compliantId;
	}
	/**
	 * @param compliantId the compliantId to set
	 */
	public void setCompliantId(Integer compliantId) {
		this.compliantId = compliantId;
	}
	public String getMailTemplateId() {
		return mailTemplateId;
	}
	public void setMailTemplateId(String mailTemplateId) {
		this.mailTemplateId = mailTemplateId;
	}
	public String getSharedByUserName() {
		return SharedByUserName;
	}
	public void setSharedByUserName(String sharedByUserName) {
		SharedByUserName = sharedByUserName;
	}
	public Integer getComplaintUserId() {
		return complaintUserId;
	}
	public void setComplaintUserId(Integer complaintUserId) {
		this.complaintUserId = complaintUserId;
	}
	
}
