package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import com.musicnotes.apis.dao.interfaces.IPaymentDao;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnSubscription;
import com.musicnotes.apis.domain.MnSubscriptionMulti;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SendMail;

public class PaymentDaoImpl extends BaseDaoImpl implements IPaymentDao
{
	Logger logger = Logger.getLogger(PaymentDaoImpl.class);
	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

	@Override
	public String makePayment(MnSubscription subscription, String payUserId, String payOrRenewal) 
	{
		String status = "";
		try {
			// for trial user
			if(logger.isDebugEnabled()){logger.debug("makePayment method called payUserId: "+payUserId);}
			MnUsers mnUsers = null;
			Query query2 = new Query(Criteria.where("userId").is(subscription.getUserId()));
			mnUsers = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);

			if (mnUsers != null && !mnUsers.equals("")) {
				Date today = new Date();
				String todayDate = formatter.format(today);
				Integer subId = 0;
				
				Query existQuery=new Query(Criteria.where("txnNo").is(subscription.getTxnNo()));
				MnSubscription checkExist=mongoOperations.findOne(existQuery,MnSubscription.class,JavaMessages.Mongo.MNSubscription);
				if(checkExist==null)
				{
				List<MnSubscription> subUsers = mongoOperations.findAll(MnSubscription.class,JavaMessages.Mongo.MNSubscription);
				if (subUsers != null && subUsers.size() != 0) {
					subId = subUsers.size() + 1;
				} else {
					subId = 1;
				}
				subscription.setSubId(subId);
				subscription.setUserName(mnUsers.getUserName());
				subscription.setEmailId(mnUsers.getEmailId());
				subscription.setSubDate(todayDate);

				String sDate = null, eDate = null, days = null;
				Calendar c = Calendar.getInstance();
				String offerType1[] = subscription.getSubType().split("-");
				String value1 = offerType1[1];

				if (mnUsers.getUserLevel().equalsIgnoreCase("Expired"))
				{
					sDate = formatter.format(c.getTime());
					c.add(Calendar.DATE, Integer.parseInt(value1)); // number of days to add
					eDate = formatter.format(c.getTime());
				} else {
					String endDate = mnUsers.getEndDate();
					c.setTime(formatter.parse(endDate));
					c.add(Calendar.DATE, 1); // for start date
					sDate = formatter.format(c.getTime());
					c.add(Calendar.DATE, Integer.parseInt(value1) - 1); // for end date
					eDate = formatter.format(c.getTime());
				}

				subscription.setSubStartDate(sDate);
				subscription.setSubEndDate(eDate);
				subscription.setSubDays(value1);

				mongoOperations.insert(subscription,JavaMessages.Mongo.MNSubscription);
				status = "success";
				
				 // entry in subscription multi table //
				if (payUserId != null && !payUserId.equals("")) 
				{
					String[] users = payUserId.split(",");
					for (int i = 0; i < users.length; i++) 
					{
						Query query4 = new Query(Criteria.where("userId").is(Integer.parseInt(users[i])));
						MnUsers mnUserMulti = mongoOperations.findOne(query4,MnUsers.class, JavaMessages.Mongo.MNUSERS);
						if (mnUserMulti != null) 
						{
							MnSubscriptionMulti multi = new MnSubscriptionMulti();
							Integer Id = 0;
							List<MnSubscription> mulUsers = mongoOperations.findAll(MnSubscription.class,JavaMessages.Mongo.MNSubscriptionMulti);
							if (mulUsers != null && mulUsers.size() != 0) 
							{
								Id = mulUsers.size() + 1;
							} else {
								Id = 1;
							}
							multi.setId(Id);
							multi.setEmailId(mnUserMulti.getEmailId());
							multi.setSubId(subId);
							multi.setUserId(mnUserMulti.getUserId());
							multi.setPaidUserId(mnUsers.getUserId());

							if (mnUserMulti.getUserLevel().equalsIgnoreCase("Expired")) 
							{
								sDate = formatter.format(c.getTime());
								c.add(Calendar.DATE, Integer.parseInt(value1)); // number of days to add
								eDate = formatter.format(c.getTime());
								days = value1;
							} else {
								String endDate = mnUserMulti.getEndDate();
								c.setTime(formatter.parse(endDate));
								c.add(Calendar.DATE, 1); // for start date
								sDate = formatter.format(c.getTime());
								c.add(Calendar.DATE, Integer.parseInt(value1)); // for end date
								eDate = formatter.format(c.getTime());
								days = value1;
							}
							multi.setStartDate(sDate);
							multi.setEndDate(eDate);
							multi.setStatus("A");
							mongoOperations.insert(multi,JavaMessages.Mongo.MNSubscriptionMulti);
							
							String usrLevel=findUserLevel(mnUsers.getStartDate(),eDate);
							// update in mnUser table
							Update update = new Update();
							update.set("userLevel", usrLevel);
							update.set("endDate", eDate);
							update.set("status", "A");
							
							if(payOrRenewal!=null && payOrRenewal.equalsIgnoreCase("Payment"))
							{
								if (subscription.getUserId().equals(Integer.parseInt(users[i]))) 
								{
									update.set("requestedUser", subscription.getPaidFor());
									if (Integer.parseInt(subscription.getPaidFor()) > 1)
										update.set("multipleUsersRequest", true);
								}
							}
							mongoOperations.updateFirst(query4, update,JavaMessages.Mongo.MNUSERS);

							// mail content
							SendMail sendMail = new SendMail();
							String recipients[] = { mnUserMulti.getEmailId() };
							try {
								String message = "<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "
										+ mnUserMulti.getUserFirstName()
										+ ",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Yor are successfully get Premium Musicnote Account.</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Premium type : "
										+ subscription.getSubType()
										+ "</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Total-amount : "
										+ subscription.getTotAmount()
										+ "</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Validity Expires on : "
										+ eDate
										+ "</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
								String subject = "Musicnote Payment";
								sendMail.postEmail(recipients, subject, message);
								if(logger.isDebugEnabled()){logger.debug("Made payment");}
							} catch (Exception e) {
								logger.error("Exception in making payment"+e);
							}
						}
					}
				}
			  }
			}

		} catch (Exception e) {
			logger.error("Error in setUserLevelChanges method :"+ e.getMessage());
		}
		return status;
	}
	
	
	public String findUserLevel(String sDate, String eDate)
	{
		String level="";
		try
		{
			if(logger.isDebugEnabled()){logger.debug("finduserLevel method called "+sDate +" "+eDate);}
			Date sdate = formatter.parse(sDate);
			Date edate = formatter.parse(eDate);
			
			int days=(int)((edate.getTime() - sdate.getTime()) / (1000 * 60 * 60 * 24));
			double year=days/365;
			
			if(year<=2)
				level="Premium-Silver";
			else if(year>2 && year<=4)
				level="Premium-Gold";
			else if(year>4)
				level="Premium-Platinum";
			
		}catch (Exception e) {
			logger.error("Exception during fetching userLevel"+e.getMessage());
		}
		if(logger.isDebugEnabled()){logger.debug("Got userLevel");}
		return level;
	}

	@Override
	public String updatePaidNo(String userId, String paidNo) {
		String status="";
		try{
			if(logger.isDebugEnabled()){logger.debug("updatePaidNo method called "+userId+"paidNo: "+paidNo);}
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnSubscription mnsubscription=mongoOperations.findOne(query,MnSubscription.class,JavaMessages.Mongo.MNSubscription);
			
			if(mnsubscription!=null){
			Update update=new Update();
			update.set("paidFor",paidNo);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNSubscription);
			
			Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnUsers mnusers=mongoOperations.findOne(query1,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if(mnusers!=null){
			Update update1=new Update();
			update1.set("multipleUsersRequest",true);
			update1.set("requestedUser",paidNo);
			mongoOperations.updateFirst(query1, update1,JavaMessages.Mongo.MNUSERS);
			}
			}
			if(logger.isDebugEnabled()){logger.debug("Updated paid No"+userId);}
			
		}catch(Exception e){
			logger.error("Exception while updating payment number"+e.getMessage());
		}
		return status;
	}

	@Override
	public String renewalPayment(MnSubscription subscription, String payUserId) 
	{
		String status="";
		try
		{
			// for trial user
		if(logger.isDebugEnabled()){logger.debug("renewalPayment method called payUserId: "+payUserId);}
		MnUsers mnUsers=null;
		MnUsers mnUserMulti=null;
		Query query2 = new Query(Criteria.where("userId").is(subscription.getUserId()));
		mnUsers = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		
		if(mnUsers!=null && !mnUsers.equals(""))
		{
			Date today=new Date();
			String todayDate=formatter.format(today);
			
			Integer subId = 0;
				List<MnSubscription> subUsers = mongoOperations.findAll(MnSubscription.class,JavaMessages.Mongo.MNSubscription);
				if (subUsers != null && subUsers.size() != 0)
				{
					subId=subUsers.size()+1;
				}
				else
				{
					subId = 1;
				}
				
				
				subscription.setSubId(subId);
				subscription.setUserName(mnUsers.getUserName());
				subscription.setEmailId(mnUsers.getEmailId());
				subscription.setSubDate(todayDate);
				
				String offerType[]=subscription.getSubType().split("-");
				String value=offerType[1];
				
				String sDate=null,eDate=null,days=null;
				Calendar c = Calendar.getInstance();
				
				if(mnUsers.getUserLevel().equalsIgnoreCase("Expired"))
				{
					sDate=formatter.format(c.getTime()); 
					c.add(Calendar.DATE, Integer.parseInt(value));  // number of days to add
					eDate = formatter.format(c.getTime()); 
				
				}else
				{
					String endDate=mnUsers.getEndDate();
					c.setTime(formatter.parse(endDate));
					c.add(Calendar.DATE, 1); // for start date
					sDate = formatter.format(c.getTime());
					c.add(Calendar.DATE, Integer.parseInt(value)-1);  // for end date
					eDate = formatter.format(c.getTime()); 
				}
				
				
				subscription.setSubStartDate(sDate);
				subscription.setSubEndDate(eDate);
				subscription.setSubDays(value);
				
				mongoOperations.insert(subscription,JavaMessages.Mongo.MNSubscription);
				status="success";
				
				String [] users=payUserId.split(",");
				
				for(int i=0;i<users.length;i++)
				{
					Query query4 = new Query(Criteria.where("userId").is(Integer.parseInt(users[i])));
					mnUserMulti = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
					if(mnUserMulti!=null)
					{
						// entry in subscription multi table
					MnSubscriptionMulti multi=new MnSubscriptionMulti();
					Integer Id = 0;
					List<MnSubscription> mulUsers = mongoOperations.findAll(MnSubscription.class,JavaMessages.Mongo.MNSubscriptionMulti);
					if (mulUsers != null && mulUsers.size() != 0)
					{
						Id=mulUsers.size()+1;
					}
					else
					{
						Id = 1;
					}
					multi.setId(Id);
					multi.setEmailId(mnUserMulti.getEmailId());
					multi.setSubId(subId);
					multi.setUserId(mnUserMulti.getUserId());
					multi.setPaidUserId(mnUsers.getUserId());
					
					String offerType1[]=subscription.getSubType().split("-");
					String value1=offerType1[1];
					
					if(mnUserMulti.getUserLevel().equalsIgnoreCase("Expired"))
					{
						sDate=formatter.format(c.getTime()); 
						c.add(Calendar.DATE,Integer.parseInt(value1));  // number of days to add
						eDate = formatter.format(c.getTime()); 
						days=value1;
					}else
					{
						String endDate=mnUserMulti.getEndDate();
						c.setTime(formatter.parse(endDate));
						c.add(Calendar.DATE, 1); // for start date
						sDate = formatter.format(c.getTime());
						c.add(Calendar.DATE, Integer.parseInt(value1));  // for end date
						eDate = formatter.format(c.getTime()); 
						days=value1;
						
					}
					multi.setStartDate(sDate);
					multi.setEndDate(eDate);
					multi.setStatus("A");
					mongoOperations.insert(multi,JavaMessages.Mongo.MNSubscriptionMulti);
					
					// update in mnUser table
					Update update=new Update();
					update.set("userLevel", "Premium-Silver");
					update.set("endDate", eDate);
					update.set("status", "A");
					mongoOperations.updateFirst(query4, update, JavaMessages.Mongo.MNUSERS);
					
					// mail content
					SendMail sendMail=new SendMail(); 	
					String recipients[]={mnUserMulti.getEmailId()};
					try{
						if(logger.isDebugEnabled()){logger.debug("renewed Payment"+payUserId);}
						String message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "+mnUserMulti.getUserFirstName()+",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Yor are successfully get Premium Musicnote Account.</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Premium type : "+subscription.getSubType()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Total-amount : "+subscription.getTotAmount()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Validity Expires on : "+eDate+"</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
						String subject="Musicnote Payment Renewal";
						sendMail.postEmail(recipients,subject ,message);
					}catch(Exception e){
						
					}
				}
				}
		}
		
	}catch (Exception e) {
		logger.error("Excdeption during renewing payment"+e);
	}
		return status;
	}
	
	public String getOfferMessageForPayment(String userId){
		String status="";
		List<MnMailConfiguration> mailList=new ArrayList<MnMailConfiguration>();
		StringBuffer jsonStr = new StringBuffer("[");
		try{
			if(logger.isDebugEnabled()){logger.debug("getOffermessageForpayment method called "+userId);}
			Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnUsers mnusers=mongoOperations.findOne(query1,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if(mnusers!=null){
				String userLevel=mnusers.getUserLevel();
				if(userLevel.contains("Premium")==true){
					
					Query query3 = new Query(Criteria.where("status").is("A").orOperator(
							Criteria.where("userLevel").is(userLevel.toLowerCase()),
							Criteria.where("userLevel").is("all"),
							Criteria.where("userLevel").is("premium")));
					
					mailList=mongoOperations.find(query3,MnMailConfiguration.class,JavaMessages.Mongo.MNMailConfiguration);
					if(mailList!=null && !mailList.isEmpty()){
						for(MnMailConfiguration maillist:mailList){
							jsonStr.append("{\"offerMsg\" : \"" +  maillist.getMailMessage() + "\",");
							jsonStr.append("\"subject\" : \"" + maillist.getMailSubject()+ "\",");
							jsonStr.append("\"amountDays\" : \"" + maillist.getAmountDays() + "\"},");
						}
						jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
						jsonStr.append("]");
					}
				}else if(userLevel.contains("Trial")==true)
				{
					Query query3 = new Query(Criteria.where("status").is("A").orOperator(
							Criteria.where("userLevel").is(userLevel.toLowerCase()),
							Criteria.where("userLevel").is("all"),
							Criteria.where("userLevel").is("trial")));
					mailList=mongoOperations.find(query3,MnMailConfiguration.class,JavaMessages.Mongo.MNMailConfiguration);
					
					if(mailList!=null && !mailList.isEmpty()){
						for(MnMailConfiguration maillist:mailList){
							jsonStr.append("{\"offerMsg\" : \"" +  maillist.getMailMessage() + "\",");
							jsonStr.append("\"subject\" : \"" + maillist.getMailSubject()+ "\",");
							jsonStr.append("\"amountDays\" : \"" + maillist.getAmountDays() + "\"},");
						}
						jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
						jsonStr.append("]");
					
					}
				}
				else if(userLevel.contains("Expired")==true)
				{
					Query query4 = new Query(Criteria.where("status").is("A").orOperator(
							Criteria.where("userLevel").is(userLevel.toLowerCase()),
							Criteria.where("userLevel").is("all")));
					mailList=mongoOperations.find(query4,MnMailConfiguration.class,JavaMessages.Mongo.MNMailConfiguration);
					
					if(mailList!=null && !mailList.isEmpty()){
						for(MnMailConfiguration maillist:mailList){
							jsonStr.append("{\"offerMsg\" : \"" +  maillist.getMailMessage() + "\",");
							jsonStr.append("\"subject\" : \"" + maillist.getMailSubject()+ "\",");
							jsonStr.append("\"amountDays\" : \"" + maillist.getAmountDays() + "\"},");
						}
						jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
						jsonStr.append("]");
					
					}
				}
				
			}
		}catch(Exception e){
			logger.error("Exception while fetching message offer for the specified userlevel"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Got offer message for payment page"+userId);}
		status=jsonStr.toString();
		return status;
	}
	
}
