package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.IAdminDao;
import com.musicnotes.apis.domain.MnAdminComplaintMailTemplate;
import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnAdminFiles;
import com.musicnotes.apis.domain.MnAdminSecurityQuestion;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnBlog;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnComplaintAction;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnSecurityQuestions;
import com.musicnotes.apis.domain.MnUserLogs;

import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;


public class AdminDaoImpl extends BaseDaoImpl implements IAdminDao{
	
	Logger logger = Logger.getLogger(AdminDaoImpl.class);

	public String updateMacheckDefaultConfiguration(){
		if(logger.isDebugEnabled())
		logger.debug("updateMacheckDefaultConfiguration method called:  ");
		String status="";
		MnAdminConfiguration adminconfig;
		try
		{
		Query query=new Query(Criteria.where("status").is("A"));
		adminconfig=mongoOperations.findOne(query,MnAdminConfiguration.class,JavaMessages.Mongo.MNAdminConfiguration);
		if(adminconfig==null){
			status="absent";
		}else{
			status="present";
		}
		
		}catch (Exception e) {
			logger.error("Exception while updateMacheckDefaultConfiguration", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateMacheckDefaultConfiguration method called:  status:  "+status);
		return status;
	}
	
	public String addDefaultConfiguration(MnAdminConfiguration configuration){
		if(logger.isDebugEnabled())
			logger.debug("addDefaultConfiguration dao impl method called:  userId:  "+configuration.getUserId());
		String status="";
		List<MnAdminConfiguration> adminConfig=new ArrayList<MnAdminConfiguration>();
		MnAdminConfiguration admin=null;
		try{
			Query query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));

			adminConfig=mongoOperations.find(query,MnAdminConfiguration.class,JavaMessages.Mongo.MNAdminConfiguration);
			if(adminConfig.size()==0){
				configuration.setConfigId("1");
			}else{
				Integer id=(adminConfig.size()+1);
				Integer configId=adminConfig.size();
				Query query5=new Query(Criteria.where("configId").is(configId.toString()));
				Update update=new Update();
				update.set("status", "I");
				mongoOperations.updateFirst(query5,update,JavaMessages.Mongo.MNAdminConfiguration);
				admin=mongoOperations.findOne(query5,MnAdminConfiguration.class,JavaMessages.Mongo.MNAdminConfiguration);
				if(admin!=null){
					configuration.setMailConfiguration(admin.isMailConfiguration());	
				}
				configuration.setConfigId(id.toString());
			}
			mongoOperations.insert(configuration,JavaMessages.Mongo.MNAdminConfiguration);
			
			
		}catch(Exception e){
			logger.error("Exception while addDefaultConfiguration", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for addDefaultConfiguration dao impl method called:  userId:  "+configuration.getUserId());
		return status;
		
	}
	
	@Override
	public String checkExistingUserLevel(String userLevel,Integer mailNo){
		if(logger.isDebugEnabled())
			logger.debug("checkExistingUserLevel dao impl method called userLevel: "+userLevel+"  mailNo:  "+mailNo);
		String status="";
		try
		{
		List<MnMailConfiguration> mailList=null;
		Query query2=null;
		if(mailNo!=null && !mailNo.equals(0))
			query2=new Query(Criteria.where("userLevel").is(userLevel).and("mailNo").ne(mailNo).and("status").is("A"));
		else
			query2=new Query(Criteria.where("userLevel").is(userLevel).and("status").is("A"));
					
		mailList=mongoOperations.find(query2, MnMailConfiguration.class, JavaMessages.Mongo.MNMailConfiguration);
		if(mailList!=null && !mailList.isEmpty())
		{
			status="userLevelExists";
		}else
		{
			status="empty";
		}
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("exception in checkExistingUserLevel method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for checkExistingUserLevel dao impl method called userLevel: "+userLevel+"  mailNo:  "+mailNo+"  status:  "+status);
		return status;
	}
	
	@Override
	public String updateMailConfigurations(MnMailConfiguration configuration) {
		if(logger.isDebugEnabled())
			logger.debug("updateMailConfigurations dao impl method called:  userId:  "+configuration.getUserId());
		String status="";
		try
		{
			///////////// ramaraj //////////////
			Query query3=new Query(Criteria.where("status").is("A").and("mailNo").is(configuration.getMailNo()));
			MnMailConfiguration single=mongoOperations.findOne(query3, MnMailConfiguration.class, JavaMessages.Mongo.MNMailConfiguration);
			{
				if(single!=null && !single.equals(""))
				{
					Update update=new Update();
					update.set("userLevel", configuration.getUserLevel());
					update.set("intervel", configuration.getIntervel());
					update.set("startDate", configuration.getStartDate());
					update.set("endDate", configuration.getEndDate());
					update.set("mailSubject", configuration.getMailSubject());
					update.set("mailMessage", configuration.getMailMessage());
					update.set("userMessage" ,configuration.getUserMessage());
					update.set("userSubject",configuration.getUserSubject());
					update.set("amountDays", configuration.getAmountDays());
					update.set("status", "A");
					mongoOperations.updateFirst(query3, update, JavaMessages.Mongo.MNMailConfiguration);
				}
			}
			
			
		
			
		}catch (Exception e) {
			logger.error("exception in updateMailConfigurations method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for updateMailConfigurations dao impl method called:  userId:  "+configuration.getUserId());
		return status;
	
	}
	
	
	@Override
	public String deletMailConfigure(String mailNo) {
		if(logger.isDebugEnabled())
			logger.debug("deletMailConfigure dao impl method called:  mailNo:  "+mailNo);
		String status="";
		try
		{
			Query query1=new Query(Criteria.where("mailNo").is(Integer.parseInt(mailNo)));
			Update update=new Update();
			update.set("status","I");
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNMailConfiguration);
			status="deleted";
		}catch (Exception e) {
			status="error";
			logger.error("Exception at deletMailConfigure method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for deletMailConfigure dao impl method called:  mailNo:  "+mailNo+ "status:  "+status );
		return status;
		
	}
	
	@Override
	public List<MnMailConfiguration> getMailConfiguration() 
	{
		if(logger.isDebugEnabled())
		logger.debug("getMailConfiguration method called");
		List<MnMailConfiguration> configuration=null;
		try
		{
			Query query=new Query(Criteria.where("status").is("A"));
			configuration=mongoOperations.find(query, MnMailConfiguration.class, JavaMessages.Mongo.MNMailConfiguration);
			if(configuration!=null){
				return configuration;
			}
		}catch (Exception e) {
			logger.error("Exception at getMailConfiguration method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getMailConfiguration method called");
		return configuration;
	}
	
	@Override
	public String updateMailNotification(Boolean userLevel,Boolean checkFlag){
		if(logger.isDebugEnabled())
			logger.debug("updateMailNotification dao impl method called:  userLevel:  "+userLevel+"  checkFlag:  "+checkFlag);
		String status="";
		MnAdminConfiguration adminconfig; 
		try
		{
			Query query=new Query(Criteria.where("status").is("A"));
			adminconfig=mongoOperations.findOne(query,MnAdminConfiguration.class,JavaMessages.Mongo.MNAdminConfiguration);
			
			if(checkFlag==true){
			
			if(adminconfig==null){
				status="ConfigurationAbsent";
			}else{
				Boolean value=adminconfig.isMailConfiguration();
				status=value.toString();
			}
			
			}else if(checkFlag==false){
				
			Query query1=new Query(Criteria.where("status").is("A"));
			if(adminconfig==null){
				status="ConfigurationAbsent";
			}else{
			Update update=new Update();
			update.set("mailConfiguration", userLevel);
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNAdminConfiguration);
			status="successfully inserted";
			}
			
			}
			
			
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("Exception while updateMailNotification", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for updateMailNotification dao impl method called: userLevel:  "+userLevel+"  checkFlag:  "+checkFlag+" status:  "+status);
		return status;
	}

public MnAdminConfiguration viewDefaultConfiguration(){
		if(logger.isDebugEnabled())
		logger.debug("viewDefaultConfiguration dao impl method called: ");
		String status="";
		MnAdminConfiguration admin = null;
		try
		{
		Query query=new Query(Criteria.where("status").is("A"));
		admin=mongoOperations.findOne(query, MnAdminConfiguration.class,JavaMessages.Mongo.MNAdminConfiguration);
		if(admin!=null){
		return admin;	
		}
		
		}catch (Exception e) {
			logger.error("Exception while viewDefaultConfiguration", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for viewDefaultConfiguration dao impl method called:");
		return admin;
		
	}
	
@Override
public String updateDefaultConfiguration(String configId,String paymentAmountDays,String status1,String uploadLimit,String userId){
	if(logger.isDebugEnabled())
		logger.debug("updateDefaultConfiguration dao impl: configId: "+configId);
	String status="";
	try
	{
	Date dateToday=new Date();
	SimpleDateFormat formatter=new SimpleDateFormat("mm/dd/yyyy");
	String todayDate=formatter.format(dateToday);	
	List<String> adminList=new ArrayList<String>();
	String[] split=paymentAmountDays.split(",");
	for(int i=0;i<split.length;i++){
	 adminList.add(split[i]);
	}
	Query query=new Query(Criteria.where("configId").is(configId).and("status").is("A"));
	Update update=new Update();
	update.set("paymentAmountDays", adminList);
	update.set("userId", userId);
	update.set("uploadLimit", uploadLimit);
	update.set("status", status1);
	update.set("startDate", todayDate);
	mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNAdminConfiguration);
	status="updated Successfully";
	
	}catch (Exception e) {
		logger.error("Exception while updateDefaultConfiguration", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for updateDefaultConfiguration dao impl method : configId: "+configId);
	return status;
	
}

@Override
public String createMailConfigurations(MnMailConfiguration mailConfiguration) 
{
	if(logger.isDebugEnabled())
		logger.debug("createMailConfigurations dao impl method called:  userId:  "+mailConfiguration.getUserId());
	String status="";
	Query query;
	Integer mailNo=0;
	try
	{
		query=new Query();
		query.with(new Sort(Sort.Direction.ASC, "_id"));

		List<MnMailConfiguration> listMail = mongoOperations.find(query,MnMailConfiguration.class,JavaMessages.Mongo.MNMailConfiguration);
		
		if (listMail != null && listMail.size() != 0)
		{
			mailNo=listMail.size()+1;
		}
		else
		{
			mailNo = 1;
		}
		mailConfiguration.setMailNo(mailNo);
		
			mongoOperations.insert(mailConfiguration,JavaMessages.Mongo.MNMailConfiguration);
            status="Successfully Inserted";
	}catch (Exception e) {
		logger.error("exception in createMailConfigurations method :"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for createMailConfigurations dao impl method called:  userId:  "+mailConfiguration.getUserId()+"  mailNo:  "+mailNo);
	return status;
}
@Override
public List<MnUsers> fetchUserDetailsBasedLevelRole(MnUsers user) {
	if(logger.isDebugEnabled())
		logger.debug("fetchUserDetailsBasedLevelRole method called");
	List<MnUsers> mnUserList1 = null;
	Query query = null;
	try
	{
		String userLevel=user.getUserLevel();
		String userRole=user.getUserRole();
		if((userLevel.contains("all")==true)&&(userRole.contains("select")==true)){
			query = new Query();
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if(((userLevel.contains("select")==true)&&(userRole.contains("select")!=true))||((userLevel.contains("all")==true)&&(userRole.contains("select")!=true))){
			query = new Query(Criteria.where("userRole").is(user.getUserRole()));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if((userRole.contains("select")==true)&&(userLevel.contains("select")!=true)&&(userLevel.contains("Active")!=true)&&(userLevel.contains("Expired")!=true)&&(userLevel.equals("Trial")!=true)&&(userLevel.equals("Premium")!=true)){
			query = new Query(Criteria.where("userLevel").is(user.getUserLevel()));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if((userLevel.contains("Expired")==true)&&(userRole.contains("select")!=true)){
			query = new Query(Criteria.where("userRole").is(user.getUserRole()).and("userLevel").is("Expired"));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if((userLevel.contains("Expired")==true)&&(userRole.contains("select")==true)){
			query = new Query(Criteria.where("userLevel").is("Expired"));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if((userLevel.contains("Active")==true)&&(userRole.contains("select")!=true)){
			query = new Query(Criteria.where("userRole").is(user.getUserRole()).and("status").is("A"));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		else if((userLevel.contains("Active")==true)&&(userRole.contains("select")==true)){
			query = new Query(Criteria.where("status").is("A"));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}else if((userLevel.equals("Trial")==true)&&(userRole.contains("select")==true)){
			 query = new Query(Criteria.where("userLevel").exists(true).orOperator(
						Criteria.where("userLevel").is("Trial-Start"),
						Criteria.where("userLevel").is("Trial-End"),
						Criteria.where("userLevel").is("Trial-Middle")));
				
			mnUserList1 = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}else if((userLevel.equals("Trial")==true)&&(userRole.contains("select")!=true)){
			 query = new Query(Criteria.where("userLevel").exists(true).and("userRole").is(user.getUserRole()).orOperator(
						Criteria.where("userLevel").is("Trial-Start"),
						Criteria.where("userLevel").is("Trial-End"),
						Criteria.where("userLevel").is("Trial-Middle")));
				
			mnUserList1 = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		else if((userLevel.equals("Premium")==true)&&(userRole.contains("select")==true)){
			query = new Query(Criteria.where("userLevel").exists(true).orOperator(
					Criteria.where("userLevel").is("Premium-Gold"),
					Criteria.where("userLevel").is("Premium-Platinum"),
					Criteria.where("userLevel").is("Premium-Silver")));
			mnUserList1 = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}else if((userLevel.equals("Premium")==true)&&(userRole.contains("select")!=true)){
			query = new Query(Criteria.where("userLevel").exists(true).and("userRole").is(user.getUserRole()).orOperator(
					Criteria.where("userLevel").is("Premium-Gold"),
					Criteria.where("userLevel").is("Premium-Platinum"),
					Criteria.where("userLevel").is("Premium-Silver")));
			mnUserList1 = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		else{
			query = new Query(Criteria.where("userLevel").is(user.getUserLevel()).and("userRole").is(user.getUserRole()));
			mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching student details in fetchUserDetailsBasedLevelRole:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserDetailsBasedLevelRole dao impl method called");
	return mnUserList1;
}
@Override
public List<MnComplaints> fetchComplaintDetailsForAdmin(String userComplaint) {
	if(logger.isDebugEnabled())
		logger.debug("fetchComplaintDetailsForAdmin dao impl method called: userComplaint: "+userComplaint);
	List<MnComplaints> mnComplaints = null;
	Query query = null;
	try
	{
		if((userComplaint.contains("all")==true)){
			query = new Query(Criteria.where("reportPage").is("crowd"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		
		else if((userComplaint.contains("resolve")==true)){
			query = new Query(Criteria.where("status").is("I").and("reportPage").is("crowd").and("adminAction").is("Resolved"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		else if((userComplaint.contains("unResolve")==true)){
			query = new Query(Criteria.where("status").is("A").and("reportPage").is("crowd").and("adminAction").is("-"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}else if((userComplaint.contains("hold")==true)){
			query = new Query(Criteria.where("status").is("A").and("reportPage").is("crowd").and("adminAction").is("Hold"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}else if((userComplaint.contains("delete")==true)){
			query = new Query(Criteria.where("status").is("I").and("reportPage").is("crowd").and("adminAction").is("Rejected"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching student details fetchComplaintDetailsForAdmin:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchComplaintDetailsForAdmin dao impl method called: userComplaint: "+userComplaint);
	return mnComplaints;
}


@Override
public List<MnComplaints> fetchNoteComplaintDetailsForAdmin(String userComplaint) {
	if(logger.isDebugEnabled())
		logger.debug("fetchNoteComplaintDetailsForAdmin dao impl method called userComplaint:  "+userComplaint);
	List<MnComplaints> mnComplaints = null;
	Query query = null;
	try
	{
		if((userComplaint.contains("all")==true)){
			query = new Query(Criteria.where("reportPage").ne("crowd"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		
		else if((userComplaint.contains("resolve")==true)){
			query = new Query(Criteria.where("status").is("I").and("reportPage").ne("crowd"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		else if((userComplaint.contains("unResolve")==true)){
			query = new Query(Criteria.where("status").is("A").and("reportPage").ne("crowd"));
			query.with(new Sort(Sort.Direction.DESC, "compliantId"));
			mnComplaints = mongoOperations.find(query, MnComplaints.class, JavaMessages.Mongo.MNCOMPLAINTS);
		}
		
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching student details fetchNoteComplaintDetailsForAdmin:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchNoteComplaintDetailsForAdmin dao impl method called userComplaint:  "+userComplaint);
	return mnComplaints;
}

@Override
public  List<MnNoteDetails> getComplaintNote(String listId, String noteId, String userId) {
	List<MnNoteDetails> noteDetail=null;
	Query query1=null;
	if(logger.isDebugEnabled())
		logger.debug("getComplaintNote method called in Impl userId:  "+userId+" noteId: "+noteId);
	try {
		query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId.trim())).and("noteId").is(Integer.parseInt(noteId.trim())).and("status").is("A"));
		noteDetail = mongoOperations.find(query1, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
		if((noteDetail!=null)&&(!noteDetail.isEmpty())){
			return noteDetail;
		}
		
	} catch (Exception e) {
		logger.error("Exception while getComplaintNote details In Impl !", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getComplaintNote method called in Impl userId:  "+userId+" noteId: "+noteId);
	return noteDetail;

}
@Override
public  String getComplaintDeleteNote(String listId, String noteId, String userId, String noteName, String adminAction, String adminComment,String actionFrom,String compliantId) 
{
	if(logger.isDebugEnabled())
		logger.debug("getComplaintDeleteNote method called in Impl userId:  "+userId+" noteId: "+noteId);
	List<MnComplaints> complaintStatus=null;
	Set<String> updateNoteDetails=new TreeSet<String>();
	String tempNoteDetails = "";
	String status="";
	String shareUserId="";
	try {
		
		// delete the note from crowd or delete description from crowd
		if(adminAction.equalsIgnoreCase("Delete"))
		{
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			if((mnList!=null)&&(!mnList.equals(""))){
			for (String note : mnList.getMnNotesDetails()) {
				JSONObject jsonObject = new JSONObject(note);
				if ((note.contains("\"noteId\":\"" + noteId + "\""))&&(note.contains("\"access\":\"public\""))) {
					if(actionFrom.equalsIgnoreCase("note"))
						note = note.replace("\"access\":\"public\"","\"access\":\"private\"");
					else{
						if(jsonObject.has("publicDescription"))
						{
							String desc=(String) jsonObject.get("publicDescription");
							if(desc.indexOf("\"") != -1){
								desc = desc.replace("\"", "\\\"");
									}
							note = note.replace("\"publicDescription\":\""+ desc + "\"", "\"publicDescription\":\"\"");
						}
						else
						{
							jsonObject.put("publicDescription", "");
							note=jsonObject.toString();
						}
					}
					tempNoteDetails = note;
					//get userId for shared note crowd
					shareUserId=(String) jsonObject.get("publicUser");
				}
				updateNoteDetails.add(note);
			}
			if (tempNoteDetails != null && !tempNoteDetails.isEmpty()){
				Update update = new Update();
				update.set("mnNotesDetails", updateNoteDetails);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			}
			
			}
		}
		
		// entry in compliant action table
		MnComplaintAction action=new MnComplaintAction();
		action.setCompliantId(Integer.parseInt(compliantId));
		if(shareUserId!=null)
		{
			action.setUserId(Integer.parseInt(shareUserId));
		}
		else
		{
			action.setUserId(Integer.parseInt(userId));
		}
		action.setNoteName(noteName);
		action.setNoteId(noteId);
		action.setListId(listId);
		action.setAdminComment(adminComment);
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
		action.setDate(format.format(date));
		action.setDeleteFor(actionFrom);
		insertAdminActionTable(action);
		status="success";
		
	} catch (Exception e) {
		logger.error("Exception while getComplaintDeleteNote details In Impl !", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getComplaintDeleteNote method called in Impl userId:  "+userId);
	return status;

}

// used to insert the admin action detail in complaint action table
public void insertAdminActionTable(MnComplaintAction action)
{
	if(logger.isDebugEnabled())
		logger.debug("insertAdminActionTable method called in Impl action:  "+action);
	Integer actionId=0;
	try
	{
	Query query=new Query();
	query.with(new Sort(Sort.Direction.ASC, "_id"));
	
	List<MnComplaintAction> listCompliant = mongoOperations.find(query,MnComplaintAction.class,JavaMessages.Mongo.MNCOMPLAINTACTION);
	if (listCompliant != null && listCompliant.size() != 0)
	{
		for (MnComplaintAction lastAction : listCompliant)
		{
			if (actionId <= lastAction.getActionId()) 
				actionId = lastAction.getActionId() + 1;
		}
	}
	else
	{
		actionId = 1;
	}
	
	action.setActionId(actionId);
	mongoOperations.insert(action,JavaMessages.Mongo.MNCOMPLAINTACTION);
	logger.info("Action inserted");
	}catch(Exception e)
	{
		logger.error("Exception insertAdminActionTable :::"+e.getMessage());
	}

}

public void sendingMailOnReportAction(String ComplaintId,MnComplaints complaint,String deletType)
{
	if(logger.isDebugEnabled())
		logger.debug("sendingMailOnReportAction method called in Impl");
	List<MnComplaintAction> mnComplaintActionList=null;
	HashSet<Integer> userIdSet=new HashSet<Integer>();
	MnUsers users=null;
	Query query = new Query(Criteria.where("compliantId").is(Integer.parseInt(ComplaintId)));
	mnComplaintActionList = mongoOperations.find(query, MnComplaintAction.class,JavaMessages.Mongo.MNCOMPLAINTACTION);
	if(!mnComplaintActionList.isEmpty())
	{
		for(MnComplaintAction mnComplaintAction:mnComplaintActionList)
		{
			userIdSet.add(mnComplaintAction.getUserId());
		}
	}
	if(!userIdSet.isEmpty())
	{
		for(Integer uniqueUserId:userIdSet)
		{
			query = new Query(Criteria.where("userId").is(uniqueUserId).and("notificationFlag").is("yes"));
			users = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
				// sending mail to note user
				SendMail sendMail=new SendMail();
				String recipients[]={users.getEmailId()};//from client to user
			try
			{
				String message=null,subject=null;
				
				//message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+"  Action taken on your "+deletType+" : '"+ complaint.getNoteName()+"', based on the Complaint : "+userComplaint +" given by :"+ complaint.getUserName() +"<br> Admin comment is :"+complaint.getAdminComment()+MailContent.sharingNotification2 +""+MailContent.signature;
				message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+" "+complaint.getAdminComment()+
						MailContent.signature2;
				subject="Action taken against your "+deletType;
				logger.info("message :"+message);
				sendMail.postEmail(recipients, subject,message);
			}
			catch(Exception e)
			{
				logger.error("Exception while sendingMailOnReportAction : ", e);
			}
		}
	}
}
	
	// sending mail to complined user(this Step is removed)
	/*try
	{
		SendMail sendMail1=new SendMail();
		String recipient[]={complaint.getOwnerMailId()};//from client to user
		String message=MailContent.sharingNotification+complaint.getUserName() +MailContent.sharingNotification1+"  Action taken on your complaint : '"+userComplaint +"' on the note : "+complaint.getNoteName() +MailContent.sharingNotification2 +""+MailContent.signature;
		String subject="Action taken against your Complaint";
		logger.info("message 1:"+message);
		sendMail1.postEmail(recipient, subject,message);
		}catch(Exception e)
		{
			logger.error("Exception while complaintReportMail : ", e);
		}*/

@Override
public String fetchUserDetailsForChartView(MnUsers user) {
	if(logger.isDebugEnabled())
		logger.debug("fetchUserDetailsForChartView method called in Impl ");
	List<MnUsers> allUserList = null;
	List<MnUsers> PremiumUserList = null;
	List<MnUsers> TrialUserList = null;
	List<MnUsers> ExpiredUserList = null;
	Query query = new Query();
	Query query1 = null;
	Query query2 = null;
	Query query3 = null;
	String status="";
	try
	{
			allUserList = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			query1 = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
					Criteria.where("userLevel").is("Premium-Gold"),
					Criteria.where("userLevel").is("Premium-Silver"),
					Criteria.where("userLevel").is("Premium-Platinum")));
			PremiumUserList = mongoOperations.find(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			query2 = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
					Criteria.where("userLevel").is("Trial-Start"),
					Criteria.where("userLevel").is("Trial-End"),
					Criteria.where("userLevel").is("Trial-Middle")));
			TrialUserList = mongoOperations.find(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			TrialUserList = mongoOperations.find(query2, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			query3 = new Query(Criteria.where("userLevel").is("Expired"));
			ExpiredUserList = mongoOperations.find(query3, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
			if((allUserList!=null)&&(!allUserList.isEmpty())){
				if((PremiumUserList!=null)&&(!PremiumUserList.isEmpty())&&(TrialUserList.isEmpty())&&(ExpiredUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"PremiumUserList\":\""+PremiumUserList.size()+"\"}";
				}else if((TrialUserList!=null)&&(!TrialUserList.isEmpty())&&(PremiumUserList.isEmpty())&&(ExpiredUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"TrialUserList\":\""+TrialUserList.size()+"\"}";
				}
				else if((ExpiredUserList!=null)&&(!ExpiredUserList.isEmpty())&&(PremiumUserList.isEmpty())&&(TrialUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"ExpiredUserList\":\""+ExpiredUserList.size()+"\"}";
				}else if((ExpiredUserList!=null)&&(!ExpiredUserList.isEmpty())&&(PremiumUserList.isEmpty())&&(TrialUserList!=null)&&(!TrialUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"TrialUserList\":\""+TrialUserList.size()+"\",\"ExpiredUserList\":\""+ExpiredUserList.size()+"\"}";
				}else if((ExpiredUserList!=null)&&(!ExpiredUserList.isEmpty())&&(TrialUserList.isEmpty())&&(PremiumUserList!=null)&&(!PremiumUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"PremiumUserList\":\""+PremiumUserList.size()+"\",\"ExpiredUserList\":\""+ExpiredUserList.size()+"\"}";
				}else if((PremiumUserList!=null)&&(!PremiumUserList.isEmpty())&&(ExpiredUserList.isEmpty())&&(TrialUserList!=null)&&(!TrialUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"TrialUserList\":\""+TrialUserList.size()+"\",\"PremiumUserList\":\""+PremiumUserList.size()+"\"}";
				}else if((allUserList!=null)&&(!allUserList.isEmpty())&&(PremiumUserList!=null)&&(!PremiumUserList.isEmpty())&&(TrialUserList!=null)&&(!TrialUserList.isEmpty())&&(ExpiredUserList!=null)&&(!ExpiredUserList.isEmpty())){
					status="{\"allUserList\":\""+allUserList.size()+"\",\"PremiumUserList\":\""+PremiumUserList.size()+"\",\"TrialUserList\":\""+TrialUserList.size()+"\",\"ExpiredUserList\":\""+ExpiredUserList.size()+"\"}";
				}
			}
			else{
				status="";
			}
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching student details in fetchUserDetailsForChartView:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserDetailsForChartView dao impl method called");
	return status;
}


@Override
public String deleteComments(String cId, String listId, String noteId,String compliantId,String adminComment) {
	Set<String> updateNoteDetails=new TreeSet<String>();
	if(logger.isDebugEnabled())
		logger.debug("deleteComments method called in Impl noteId:  "+noteId+"  complaintId:  "+compliantId );
	MnComments comments;
	try {

		// update in mncomments table
		
		Query query = new Query(Criteria.where("cId").is(Integer.parseInt(cId)));
		 comments = mongoOperations.findOne(query,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
		 Update update = new Update();
		
		 if(comments.getCommLevel().equals("P/I"))
		 {
			 update.set("commLevel", "I"); 
		 }
		 else
		 {
			 update.set("status", "I");
		 }
		
		mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNCOMMETS);
		 comments = mongoOperations.findOne(query,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
		Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

		
		// updates in Mnlist talble
		MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
		String tempNoteDetails = null;
		String removeNoteDeatils = null;
		String oldComments = "";
		String newComment = "";
		for (String note : mnList.getMnNotesDetails()) {
			
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						oldComments = (String) jsonObject.get("pcomments");

						if (oldComments != null && !oldComments.isEmpty()) {
							newComment = oldComments;
							newComment = newComment.replace(cId + ",", "");
							newComment = newComment.replace("," + cId, "");
							newComment = newComment.replace(cId, "");
							if (newComment.length() <= 2) {
								newComment = "";
							}
						} else {
							newComment = "";
						}

						if (note.contains("\"pcomments\":\"\"")) {
							note = note.replace("\"pcomments\":\"\"","\"pcomments\":\"" + newComment + "\"");
						} else {
							note = note.replace("\"pcomments\":\"" + oldComments+ "\"", "\"pcomments\":\"" + newComment+ "\"");
						}
						tempNoteDetails = note;
					} catch (Exception e) {
						logger.error("Exception while converting json to Note Attached file In Impl !",e);
					}
			}
			
			updateNoteDetails.add(note);
		}
		
		if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
			mnList.getMnNotesDetails().remove(removeNoteDeatils);

		if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
			mnList.getMnNotesDetails().add(tempNoteDetails);

		Update update2 = new Update();
		update2.set("mnNotesDetails", updateNoteDetails);
		mongoOperations.updateFirst(query2, update2,JavaMessages.Mongo.MNLIST);
		
		// updates in mnnote details table
		Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
		MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
		if(mnNoteDetails!= null ){
			Update update1 = new Update();
			List<Integer> cmtIdList = new ArrayList<Integer>();
			
				if(newComment.equals("")){
					mnNoteDetails.setPcomments(new ArrayList<Integer>());
				}else{
					if(newComment.indexOf("[") != -1){
						newComment = newComment.substring(newComment.indexOf("[")+1,newComment.length());
					}
					if(newComment.indexOf("]") != -1){
						newComment = newComment.substring(0,newComment.indexOf("]"));
					}
					String[] splitCmtIds = newComment.split(",");
					if(splitCmtIds.length > 0 && !splitCmtIds[0].equals("")){
						for(String str : splitCmtIds){
							cmtIdList.add(Integer.parseInt(str.trim()));
						}
					}
					if(!cmtIdList.isEmpty()){
						mnNoteDetails.setPcomments(cmtIdList);
					}
				}
				
				update1.set("pcomments",mnNoteDetails.getPcomments());
				
			mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			
			
			// add entry admin complaintaction table
			// entry in compliant action table
			MnComplaintAction complaintAction=new MnComplaintAction();
			complaintAction.setCompliantId(Integer.parseInt(compliantId));
			//complaintAction.setNoteName(noteName);
			complaintAction.setUserId(comments.getUserId());
			complaintAction.setNoteId(noteId);
			complaintAction.setListId(listId);
			complaintAction.setAdminComment(adminComment);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
			complaintAction.setDate(format.format(date));
			complaintAction.setDeleteFor("Comment");
			complaintAction.setCommentId(cId);
			insertAdminActionTable(complaintAction);
		}
		
	} catch (Exception e) {
		logger.error("Exception while deleteComments  In Impl !", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for deleteComments method called in Impl noteId:  "+noteId+"  complaintId:  "+compliantId);
	return cId;
}
@Override
public  String adminDeleteReportComment(String cId, String listId, String noteId,String adminAction, String adminComment) {
List<MnComplaints> complaintStatus=null;
	String status="";
	Query query1=null;
	if(logger.isDebugEnabled())
		logger.debug("adminDeleteReportComment method called in Impl :commentId: "+cId+" noteId: "+noteId);
	try {
			
			query1 =new Query(Criteria.where("listId").is(listId.trim()).and("noteId").is(noteId.trim()).and("commentId").is(cId));
			complaintStatus = mongoOperations.find(query1, MnComplaints.class,JavaMessages.Mongo.MNCOMPLAINTS);
			if((complaintStatus!=null)&&(!complaintStatus.isEmpty())){
				for(MnComplaints complaint:complaintStatus){
					if(complaint.getStatus().equals("A")){
						Query query3 =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("commentId").is(complaint.getCommentId()));
						Update update1 = new Update();
						update1.set("status", "I");
						update1.set("adminComment", adminComment);
						update1.set("adminAction", adminAction);
						complaint.setAdminAction(adminAction);
						complaint.setAdminComment(adminComment);
						//sendingMailOnReportAction(complaint,"comment");
						mongoOperations.updateMulti(query3, update1,JavaMessages.Mongo.MNCOMPLAINTS);
					}
				}
				
				status="success";
			}else{
				status="error";
			}
		
	} catch (Exception e) {
		logger.error("Exception while adminDeleteReportComment details In Impl !", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for adminDeleteReportComment method called in Impl :commentId: "+cId+" noteId: "+noteId);
	return status;

}

@Override
public String deleteAttachmentInCrowd(String fileName, String userId,String noteId,String listId, String noteName, String adminComment, String action,String compliantId) {
	
	String status="";
	Query query1=null;
	MnList mnList = null;
	JSONObject jsonObject;
	String attachFile;
	String tempNoteDetails = null;
	String removeNoteDeatils = null;
	List<Integer> stringList = null;
	Integer attachId=null;
	String flag = "";
	if(logger.isDebugEnabled())
		logger.debug("admin deleteAttachmentInCrowd method called in Impl :fileName: "+fileName+" userId: "+userId);
	try {
			query1 =new Query(Criteria.where("fileName").is(fileName).and("listId").is(Integer.parseInt(listId)).and("noteId").is(noteId).and("status").is("A"));
			List<MnAttachmentDetails> details  = mongoOperations.find(query1,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if(details!= null && !details.isEmpty()){
				
				for(MnAttachmentDetails detail:details){
					Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
					 mnList = mongoOperations.findOne(query2, MnList.class, JavaMessages.Mongo.MNLIST);
					 if(mnList!= null){
						 for (String note : mnList.getMnNotesDetails()) {
							 if(note.contains("\"noteId\":\"" + noteId + "\"")){
							 jsonObject = new JSONObject(note);
							 attachFile = (String) jsonObject.get("privateAttachFilePath");
							 
								 if (attachFile != null && !attachFile.trim().equals("")) {
									 removeNoteDeatils = note;
									 String oldAttaches = attachFile;
										
									 if(attachFile.contains("["))
									 	attachFile = attachFile.replace("[", "");
									 if(attachFile.contains("]"))
									 	attachFile = attachFile.replace("]", "");
									 
									 String[] array = attachFile.split(",");
									 attachId=detail.getAttachId();
									 stringList = new ArrayList<Integer>();
									 for(String s: array){
									 	stringList.add(Integer.parseInt(s.trim()));
									 }if(stringList.indexOf(detail.getAttachId()) != -1){
										 stringList.remove(stringList.indexOf(detail.getAttachId()));
									 }
									 
									 String newAttached = "";
									 if(stringList!= null && !stringList.isEmpty()){
									 	newAttached = stringList.toString();
									 }
									 note = note.replace("\"privateAttachFilePath\":\""+oldAttaches+"\"","\"privateAttachFilePath\":\"" +newAttached.trim()+ "\"");
									 tempNoteDetails = note;
								 } 
								 }
							 }
						 if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
								mnList.getMnNotesDetails().remove(removeNoteDeatils);

							if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
								mnList.getMnNotesDetails().add(tempNoteDetails);

							Update update2 = new Update();
							update2.set("mnNotesDetails", mnList.getMnNotesDetails());
							mongoOperations.updateFirst(query2, update2,JavaMessages.Mongo.MNLIST);
						 
						 }
					}
				
				}
				
					// update in mnNote details table
					Query detailsQuery = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
					MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
					if(mnNoteDetails!= null ){
						Update update1 = new Update();
						if(stringList!= null && !stringList.isEmpty() ){
							mnNoteDetails.setPrivateAttachFilePath(stringList);
						}else{
							mnNoteDetails.setPrivateAttachFilePath(new ArrayList<Integer>());
						}
						update1.set("privateAttachFilePath",mnNoteDetails.getPrivateAttachFilePath());
						
						mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
					}
					
					// entry in compliant action table
					MnComplaintAction complaintAction=new MnComplaintAction();
					complaintAction.setCompliantId(Integer.parseInt(compliantId));
					complaintAction.setUserId(Integer.parseInt(userId));
					complaintAction.setNoteName(noteName);
					complaintAction.setNoteId(noteId);
					complaintAction.setListId(listId);
					complaintAction.setAdminComment(adminComment);
					Date date = new Date();
					DateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
					complaintAction.setDate(format.format(date));
					complaintAction.setDeleteFor("Attachement");
					complaintAction.setAttachId(attachId);
					insertAdminActionTable(complaintAction);
					
					flag = "success";
			}
		
		
	catch (Exception e) {
		logger.error("Exception while admin deleteAttachmentInCrowd details In Impl !", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for deleteAttachmentInCrowd dao impl method called userId:  "+userId+"  fileName:  "+fileName);
	return flag;
}

@Override
public String deleteAttachmentInBothNoteCrowd(String fileName, String userId,
		String noteId, String listId, String noteName, String adminComment, String action) {
	if(logger.isDebugEnabled())
		logger.debug(" admin deleteAttachmentInBothNoteCrowd dao impl method called userId:  "+userId+"fileName:  "+fileName);
	Date todayDate = new Date();
	Date todayTime = new Date();
	String flag = "";
	Query query1 = null;
	JSONObject jsonObject;
	String attachFile;
	String crowdAttachFile;
	boolean deleteFlag=false;
	MnList mnList = null;
	String tempNoteDetails = null;
	String removeNoteDeatils = null;
	List<Integer> stringList = null;
	List<Integer> stringListForCrowd = null;
	try{
		Query query3 = new  Query(Criteria.where("fileName").is(fileName).and("status").is("A"));
		List<MnAttachmentDetails> details  = mongoOperations.find(query3,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
		if(details!= null && !details.isEmpty()){
			//if( details.size() == 1){
				for(MnAttachmentDetails detail:details){
				 query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
				 mnList = mongoOperations.findOne(query1, MnList.class, JavaMessages.Mongo.MNLIST);
				 if(mnList!= null){
					 for (String str : mnList.getMnNotesDetails()) {
						 jsonObject = new JSONObject(str);
						 attachFile = (String) jsonObject.get("attachFilePath");
						 crowdAttachFile = (String) jsonObject.get("privateAttachFilePath");
						 if(str.contains("\"noteId\":\"" + noteId + "\"")){
							 if (attachFile != null && !attachFile.trim().equals("") && crowdAttachFile != null && !crowdAttachFile.trim().equals("") ) {
								 removeNoteDeatils = str;
								 String oldAttaches = attachFile;
								 String oldCrowdAttaches = crowdAttachFile;
								 if(attachFile.contains("["))
								 	attachFile = attachFile.replace("[", "");
								 if(attachFile.contains("]"))
								 	attachFile = attachFile.replace("]", "");
								 
								 String[] array = attachFile.split(",");
								 stringList = new ArrayList<Integer>();
								 
								 if(crowdAttachFile.contains("["))
									 crowdAttachFile = crowdAttachFile.replace("[", "");
									 if(crowdAttachFile.contains("]"))
										 crowdAttachFile = crowdAttachFile.replace("]", "");
									 
									 String[] array1 = crowdAttachFile.split(",");
								 
								 
								 stringListForCrowd = new ArrayList<Integer>();
								 for(String s: array){
								 	stringList.add(Integer.parseInt(s.trim()));
								 }
								 for(String s1: array1){
									 stringListForCrowd.add(Integer.parseInt(s1.trim()));
									 }
								 if(stringList.indexOf(detail.getAttachId()) != -1){
									 stringList.remove(stringList.indexOf(detail.getAttachId()));
								 }
								 if(stringListForCrowd.indexOf(detail.getAttachId()) != -1){
									 stringListForCrowd.remove(stringListForCrowd.indexOf(detail.getAttachId()));
								 }
								 
								 String newAttached = "";
								 String newAttachedCrowd = "";
								 if(stringList!= null && !stringList.isEmpty()){
								 	newAttached = stringList.toString();
								 }
								 if(stringListForCrowd!= null && !stringListForCrowd.isEmpty()){
									 newAttachedCrowd = stringListForCrowd.toString();
									 }
								 
								 str = str.replace("\"attachFilePath\":\""+oldAttaches+"\"","\"attachFilePath\":\"" +newAttached.trim()+ "\"");
								 str = str.replace("\"privateAttachFilePath\":\""+oldCrowdAttaches+"\"","\"privateAttachFilePath\":\"" +newAttachedCrowd.trim()+ "\"");
								 tempNoteDetails = str;
							 }
						 }else{
							 if (attachFile != null && !attachFile.trim().equals("") && crowdAttachFile != null && !crowdAttachFile.trim().equals("") ) {
								 if(attachFile.contains("["))
									attachFile = attachFile.replace("[", "");
								 if(attachFile.contains("]"))
									attachFile = attachFile.replace("]", "");
									 
								 String[] array = attachFile.split(",");
								 if(crowdAttachFile.contains("["))
									 crowdAttachFile = crowdAttachFile.replace("[", "");
									 if(crowdAttachFile.contains("]"))
										 crowdAttachFile = crowdAttachFile.replace("]", "");
									 
									 String[] array1 = crowdAttachFile.split(",");
								 
								 
								 stringListForCrowd = new ArrayList<Integer>();
								 stringList = new ArrayList<Integer>();
								 for(String s: array){
								 	stringList.add(Integer.parseInt(s.trim()));
								 }
								 for(String s1: array1){
									 stringListForCrowd.add(Integer.parseInt(s1.trim()));
									 }
								 if((stringList.indexOf(detail.getAttachId()) != -1)&& (stringListForCrowd.indexOf(detail.getAttachId()) != -1)){
									 deleteFlag=true;
									 flag = "otherNote";
									 break;
								 }
							 }
						 }
					 }
				 }
			}
				/*else if ( details.size() > 1){
				 flag = "otherList";
				 deleteFlag=true;
			}*/
		}else{
			deleteFlag=true;
		}
		if (!deleteFlag && !flag.equals("otherList") && !flag.equals("otherNote")) {
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update2 = new Update();
			update2.set("mnNotesDetails", mnList.getMnNotesDetails());
			mongoOperations.updateFirst(query1, update2,JavaMessages.Mongo.MNLIST);

			Query query = new Query(Criteria.where("userId").is(userId).and("fileName").is(fileName));
			Update update = new Update();

			update.set("status", "I");
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MnFiles);
			
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(stringList!= null && !stringList.isEmpty() && stringListForCrowd!= null && !stringListForCrowd.isEmpty()){
					mnNoteDetails.setAttachFilePath(stringList);
					mnNoteDetails.setPrivateAttachFilePath(stringListForCrowd);
				}else{
					mnNoteDetails.setAttachFilePath(new ArrayList<Integer>());
					mnNoteDetails.setPrivateAttachFilePath(new ArrayList<Integer>());
				}
				update1.set("attachFilePath",mnNoteDetails.getAttachFilePath());
				update1.set("privateAttachFilePath",mnNoteDetails.getPrivateAttachFilePath());
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			

			Query query2 = new Query(Criteria.where("attachId").is(details.get(0).getAttachId()).and("status").is("A"));
			update.set("endDate",dateFormat.format(todayDate));
			update.set("endTime",timeFormat.format(todayTime));
			mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
			
			Query query5 =new Query(Criteria.where("listId").is(listId.trim()).and("noteId").is(noteId.trim()).and("noteName").is(noteName));
			List<MnComplaints> complaintStatus = mongoOperations.find(query5, MnComplaints.class,JavaMessages.Mongo.MNCOMPLAINTS);
			if((complaintStatus!=null)&&(!complaintStatus.isEmpty())){
				for(MnComplaints complaint:complaintStatus){
					if(complaint.getStatus().equals("A")){
						Query query6 =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("noteName").is(complaint.getNoteName()).and("commentId").is("note").and("reportPage").is("crowd"));
						Update update1 = new Update();
						update1.set("status", "I");
						update1.set("adminComment", adminComment);
						update1.set("adminAction", action);
						complaint.setAdminAction(action);
						complaint.setAdminComment(adminComment);
						mongoOperations.updateMulti(query6, update1,JavaMessages.Mongo.MNCOMPLAINTS);
						//if(action.equalsIgnoreCase("Delete"))
						//sendingMailOnReportAction(complaint,"note");
					}
				}
				
				//flag="success";
			}
			
			
			
			
			
			flag = "success";
			
			//MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));
		}
		else{
			flag = "error";
		}
	}
	catch (Exception e)
	{
		flag = "error";
		logger.error("Exception in deleteAttachmentInBothNoteCrowd :", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for admin deleteAttachmentInBothNoteCrowd dao impl method called userId:  "+userId+"fileName:  "+fileName);
	return flag;
}


@Override
public String adminStatusReport(String listId, String noteId, String userId,String noteName, String adminAction, String adminComment, String compliantId,String mailTemplateId) {
	if(logger.isDebugEnabled())
		logger.debug("adminStatusReport dao impl method called userId:  "+userId);
	String status="";
	Query query=null;
	try
	{
		Query query1 =new Query(Criteria.where("listId").is(listId.trim()).and("noteId").is(noteId.trim()).and("noteName").is(noteName).and("compliantId").is(Integer.parseInt(compliantId)));
		MnComplaints complaint = mongoOperations.findOne(query1, MnComplaints.class,JavaMessages.Mongo.MNCOMPLAINTS);
		//fetch from MnComplaintAction 
		Query adminActionQuery=new Query(Criteria.where("listId").is(listId.trim()).and("noteId").is(noteId.trim()).and("noteName").is(noteName).and("deleteFor").is("Note"));
		MnComplaintAction mnComplaintAction=mongoOperations.findOne(adminActionQuery, MnComplaintAction.class,JavaMessages.Mongo.MNCOMPLAINTACTION);
		if((complaint!=null)&&(!complaint.equals("")))
		{
			if(adminAction.equals("Hold"))
			{
				query =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("noteName").is(complaint.getNoteName()).and("compliantId").is(Integer.parseInt(compliantId)));
				Update update1 = new Update();
				update1.set("status", "A");
				update1.set("adminComment", adminComment);
				update1.set("adminAction", adminAction);
				update1.set("mailTemplateId",mailTemplateId);
				complaint.setAdminAction(adminAction);
				complaint.setAdminComment(adminComment);
				mongoOperations.updateMulti(query, update1,JavaMessages.Mongo.MNCOMPLAINTS);
				status="success";
			}
			else if(adminAction.equals("Rejected"))
			{
				query =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("noteName").is(complaint.getNoteName()).and("compliantId").is(Integer.parseInt(compliantId)));
				Update update1 = new Update();
				update1.set("status", "I");
				update1.set("mailTemplateId",mailTemplateId);
				update1.set("adminComment", adminComment);
				update1.set("adminAction", adminAction);
				complaint.setAdminAction(adminAction);
				complaint.setAdminComment(adminComment);
				mongoOperations.updateMulti(query, update1,JavaMessages.Mongo.MNCOMPLAINTS);
				status="success";
			}
			else if(adminAction.equals("Resolved"))
			{
				if(mnComplaintAction!=null && !mnComplaintAction.equals(""))
				{
					query =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("noteName").is(complaint.getNoteName()).and("adminAction").ne("Resolved"));
				}
				else
				{
					query =new Query(Criteria.where("listId").is(complaint.getListId()).and("noteId").is(complaint.getNoteId()).and("noteName").is(complaint.getNoteName()).and("compliantId").is(Integer.parseInt(compliantId)));
				}
				Update update1 = new Update();
				update1.set("status", "I");
				update1.set("mailTemplateId",mailTemplateId);
				update1.set("adminComment", adminComment);
				update1.set("adminAction", adminAction);
				complaint.setAdminAction(adminAction);
				complaint.setAdminComment(adminComment);
				mongoOperations.updateMulti(query, update1,JavaMessages.Mongo.MNCOMPLAINTS);
				sendingMailOnReportAction(compliantId,complaint,"note");
				status="success";
			}
		}
		}catch(Exception e){
			status="error";
			logger.error("Exception in adminStatusReport :", e);
		}
	if(logger.isDebugEnabled())
		logger.debug("Return response for adminStatusReport dao impl method called userId:  "+userId);
	return status;


	}

@Override
public String addMailTemplates(String templateName, String templateText) {
	if(logger.isDebugEnabled())
		logger.debug(" addMailTemplates dao impl method called templateName:  "+templateName);
	String status="";
	Integer templateId=1;
	MnAdminComplaintMailTemplate mailTemplate=new MnAdminComplaintMailTemplate();
	try{
		Query query=new Query();
		query.with(new Sort(Sort.Direction.ASC, "_id"));

		
		List<MnAdminComplaintMailTemplate> listTemplate = mongoOperations.find(query,MnAdminComplaintMailTemplate.class,JavaMessages.Mongo.MNADMINCOMPLAINTMAILTEMPLATE);
		if (listTemplate != null && listTemplate.size() != 0)
		{
			for (MnAdminComplaintMailTemplate templates : listTemplate)
			{
				if (templateId <= templates.getTemplateId()) 
					templateId = templates.getTemplateId() + 1;
			}
		}
		else
		{
			templateId = 1;
		}
		mailTemplate.setTemplateId(templateId);
		mailTemplate.setTemplateName(templateName);
		mailTemplate.setTemplateText(templateText);
		mailTemplate.setStatus("A");
	
		mongoOperations.insert(mailTemplate,JavaMessages.Mongo.MNADMINCOMPLAINTMAILTEMPLATE);
		status= "success";
		
	}catch(Exception e){
		status= "error";
		logger.error("Exception while addMailTemplates details :" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for addMailTemplates dao impl method called templateName:  "+templateName);
	return status;
}
@Override
public String updateMailTemplates(String templateName, String templateText,String templateId) {
	if(logger.isDebugEnabled())
		logger.debug(" updateMailTemplates dao impl method called templateId:  "+templateId);
	String status="";
	MnAdminComplaintMailTemplate mailTemplate=new MnAdminComplaintMailTemplate();
	try{
		
		Query query6 =new Query(Criteria.where("templateId").is(Integer.parseInt(templateId)));
		Update update1 = new Update();
		update1.set("templateName", templateName);
		update1.set("templateText", templateText);
		mailTemplate.setTemplateName(templateName);
		mailTemplate.setTemplateText(templateText);
		mongoOperations.updateMulti(query6, update1,JavaMessages.Mongo.MNADMINCOMPLAINTMAILTEMPLATE);
		status= "success";
		
	}catch(Exception e){
		status= "error";
		logger.error("Exception while inserting updateMailTemplates details :" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for updateMailTemplates dao impl method called templateId:  "+templateId);
	return status;
}
@Override
public List<MnAdminComplaintMailTemplate> fetchMailTemplates(String userId) {
	if(logger.isDebugEnabled())
		logger.debug("fetchMailTemplates method called in Impl userId:  "+userId);
	List<MnAdminComplaintMailTemplate> allTemplateList = null;
	Query query = new Query();
	
	String status="";
	try
	{
		query = new Query(Criteria.where("status").is("A"));
		query.with(new Sort(Sort.Direction.ASC, "_id"));

		allTemplateList = mongoOperations.find(query,MnAdminComplaintMailTemplate.class,JavaMessages.Mongo.MNADMINCOMPLAINTMAILTEMPLATE);	
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching fetchMailTemplates:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchMailTemplates method called in Impl userId:  "+userId);
	return allTemplateList;
}
@Override
public String adminAddSecurityQuestions(String question) {
	if(logger.isDebugEnabled())
		logger.debug("adminAddSecurityQuestions method called in Impl question:  "+question);
	String status="";
	String questionId="Q1";
	Integer id=1;
	MnAdminSecurityQuestion Questions=new MnAdminSecurityQuestion();
	try{
		Query query=new Query();
		query.with(new Sort(Sort.Direction.ASC, "_id"));

		List<MnAdminSecurityQuestion> securityQuestions = mongoOperations.find(query,MnAdminSecurityQuestion.class,JavaMessages.Mongo.MNADMINSECURITYQUESTION);
		if (securityQuestions != null && securityQuestions.size() != 0)
		{
			for (MnAdminSecurityQuestion Question : securityQuestions)
			{
				if (id <= Question.getqId()) {
					id = Question.getqId() + 1;
					String value=Integer.toString(id);
					questionId = "Q"+value;
				}
			}
		}
		else
		{
			id=1;
			questionId = "Q1";
		}
		Questions.setQuestionId(questionId);
		Questions.setqId(id);
		Questions.setQuestion(question);
		Questions.setStatus("A");
	
		mongoOperations.insert(Questions,JavaMessages.Mongo.MNADMINSECURITYQUESTION);
		status= "success";
		
	}catch(Exception e){
		status= "error";
		logger.error("Exception while inserting adminAddSecurityQuestions details :" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for adminAddSecurityQuestions method called in Impl question:  "+question);
	return status;
}
@Override
public List<MnAdminSecurityQuestion> fetchsecurityQuestionView(String userId) {
	if(logger.isDebugEnabled())
		logger.debug("fetchsecurityQuestionView method called in Impl userId:  "+userId);
	List<MnAdminSecurityQuestion> allsecurityQuestionList = null;
	Query query = new Query();
	
	String status="";
	try
	{
		query = new Query(Criteria.where("status").is("A"));
		query.with(new Sort(Sort.Direction.ASC, "_id"));

		allsecurityQuestionList = mongoOperations.find(query,MnAdminSecurityQuestion.class,JavaMessages.Mongo.MNADMINSECURITYQUESTION);	
	}
	catch (Exception e)
	{
		logger.error("Exception while fetching fetchsecurityQuestionView:" + e);

	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchsecurityQuestionView method called in Impl userId:  "+userId);
	return allsecurityQuestionList;
}
@Override
public String updateSecurityQuestions(String qId, String QuestionId,String question,String type) {
	if(logger.isDebugEnabled())
		logger.debug(" updateSecurityQuestions method called QuestionId:  "+QuestionId);
	String status="";
	MnAdminSecurityQuestion SecurityQuestion=new MnAdminSecurityQuestion();
	try{
		
		Query query6 =new Query(Criteria.where("qId").is(Integer.parseInt(qId)).and("questionId").is(QuestionId));
		Update update1 = new Update();
		if(type.equals("update"))
		{
			update1.set("question", question);
			SecurityQuestion.setQuestion(question);
		}
		else
		{
			update1.set("status", "I");
		}
		mongoOperations.updateMulti(query6, update1,JavaMessages.Mongo.MNADMINSECURITYQUESTION);
		status= "success";
		
	}catch(Exception e){
		status= "error";
		logger.error("Exception while updateSecurityQuestions details :" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for updateSecurityQuestions method called QuestionId:  "+QuestionId);
	return status;
}
//fetch complaint Details from MnComplaintAction table
@Override
public List<MnComplaintAction> fetchComplaints() {
	if(logger.isDebugEnabled())
		logger.debug("fetchComplaint method called in Impl ");
	List<MnComplaintAction> mnComplaintAction=null;
	try
	{
		Aggregation aggregation = Aggregation.newAggregation( 
			Aggregation.project("userId","compliantId"),
			Aggregation.group("userId").addToSet("userId").as("userId").count().as("compliantId"));
		
		AggregationResults<MnComplaintAction> result= this.mongoOperations.aggregate(aggregation, JavaMessages.Mongo.MNCOMPLAINTACTION, MnComplaintAction.class);
		mnComplaintAction = result.getMappedResults();
		mnComplaintAction=mongoOperations.findAll(MnComplaintAction.class,JavaMessages.Mongo.MNCOMPLAINTACTION);
	}
	catch (Exception e)
	{
		logger.error("Exception while fetchComplaint:" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchComplaint method called in Impl ");
	return mnComplaintAction;
}
//fetch Complaint user details from user List
@Override
public MnUsers fetchUserForComplaint(Integer userId) {
	if(logger.isDebugEnabled())
		logger.debug("fetchUserForComplaint method called in Impl userId:  "+userId);
	MnUsers mnUsers = null;
	Query query = null;
	try
	{
		
		query = new Query(Criteria.where("userId").is(userId));
		mnUsers = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
	}
	catch (Exception e)
	{
		logger.error("Exception while fetchUserForComplaint:" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserForComplaint method called in Impl userId:  "+userId);
	return mnUsers;
}
//insert new blocked user details
@Override
public String insertBlockedUser(MnBlockedUsers mnBlockedUsers) {
	if(logger.isDebugEnabled())
		logger.debug("while insertBlockedUser method for AdminDaoImpl: ");	
		String status="";
		Query query=null;
		Integer blockedId = 0;
		try
		{
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnBlockedUsers> listUser = mongoOperations.find(query,MnBlockedUsers.class,JavaMessages.Mongo.MNBLOCKEDUSERS);
			if (listUser != null && listUser.size() != 0)
			{
				for (MnBlockedUsers lastUser : listUser)
				{
					if (blockedId <= lastUser.getBlockedId()) 
						blockedId = lastUser.getBlockedId() + 1;
				}
			}
			else
			{
				blockedId = 1;
			}
			mnBlockedUsers.setBlockedId(blockedId);
			mongoOperations.insert(mnBlockedUsers,JavaMessages.Mongo.MNBLOCKEDUSERS);
			//sending mail to BlockedUsers method
			sendingMailOnAdminBlockUser(mnBlockedUsers);
			status="success";
		}
		catch (Exception e) {
			logger.error("Exception While insertBlockedUser:"+e.getMessage());
			status="failed";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for while insertBlockedUser method for AdminDaoImpl: ");
	return status;
}
//Update Blocked User Details
@Override
public String updateBlockedUser(MnBlockedUsers mnBlockedUsers) {
	if(logger.isDebugEnabled())
		logger.debug("while updateBlockedUser method for AdminDaoImpl: ");	
		String status="";
		try
		{
			Query query=new Query(Criteria.where("blockedId").is(mnBlockedUsers.getBlockedId()));
			Update update = new Update();
			update.set("endBlockedDate", mnBlockedUsers.getEndBlockedDate());
			update.set("status", mnBlockedUsers.getStatus());
			update.set("crowdShareFlag", mnBlockedUsers.getCrowdShareFlag());
			if(mnBlockedUsers.getStatus().equals("A"))
			{
				update.set("blockedDate", mnBlockedUsers.getBlockedDate());
				update.set("blockedLevel", mnBlockedUsers.getBlockedLevel());
				if(!mnBlockedUsers.getNumberOfDays().equals("fullyBlocked"));
				{
					update.set("numberOfDays", mnBlockedUsers.getNumberOfDays());
				}
				update.set("endBlockedDate" , mnBlockedUsers.getEndBlockedDate());
			}
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNBLOCKEDUSERS);
			//sending mail to BlockedUsers method
			sendingMailOnAdminBlockUser(mnBlockedUsers);
			status= "success";
		}
		catch(Exception e)
		{
			logger.error("Exception while updateBlockedUser details:" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for while updateBlockedUser method for AdminDaoImpl: ");	
	return status;
}

//fetch blocked user Details
@Override
public MnBlockedUsers fetchBlockedUserList(Integer userId) {
	if(logger.isDebugEnabled())
		logger.debug("fetchBlockedUser method called in Impl userId:  "+userId);
	MnBlockedUsers mnBlockedUsers = null;
	Query query = null;
	try
	{
		query = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
		mnBlockedUsers = mongoOperations.findOne(query, MnBlockedUsers.class, JavaMessages.Mongo.MNBLOCKEDUSERS);
	}
	catch(Exception e)
	{
		logger.error("Exception while fetchBlockedUser details:" + e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for fetchBlockedUser method called in Impl userId:  "+userId);
	return mnBlockedUsers;
}

//sending mail to Blocked Users 
public void sendingMailOnAdminBlockUser(MnBlockedUsers mnBlockedUsers)
{
	if(logger.isDebugEnabled())
		logger.debug("sendingMailOnAdminBlockUser method called in Impl");
	SendMail sendMail=new SendMail();
	String message=null,subject=null,body=null;
	String blockUserLevel="";
	//getMailId from Mnuser
	Query query2=new Query(Criteria.where("userId").is(mnBlockedUsers.getUserId()));
	MnUsers mnUsers=mongoOperations.findOne(query2,MnUsers.class,JavaMessages.Mongo.MNUSERS);
	String mailId=mnUsers.getEmailId();
	String recipients[]={mailId};
	String date=mnBlockedUsers.getBlockedDate();
	try
	{
		if(mnBlockedUsers.getNumberOfDays().equals("10"))
		{
			blockUserLevel="upto "+mnBlockedUsers.getEndBlockedDate().substring(0, 11);
		}
		else if(mnBlockedUsers.getNumberOfDays().equals("20"))
		{
			blockUserLevel="upto "+mnBlockedUsers.getEndBlockedDate().substring(0, 11);
		}
		else
		{
			//blockUserLevel=mnBlockedUsers.getNumberOfDays();
		}
		if(mnBlockedUsers.getStatus().equals("A"))
		{
			if(!mnBlockedUsers.getBlockedLevel().equals("Crowd"))
			{
				if(blockUserLevel!=null && blockUserLevel!="")
				{
					body="Your account is temporarily suspended due to a violation of our Terms of Service for Date "+date+" "+blockUserLevel+".";
				}
				else
				{
					body="Your account is temporarily suspended due to a violation of our Terms of Service for Date "+date+".";
				}
				subject="Admin blocked your login ID";
			}
			else
			{
				body="Your account is temporarily suspended for crowd share due to a violation of our Terms of Service for Date "+date+". ";
				subject="Admin blocked your CrowdShare";
			}
			
		}
		else if(mnBlockedUsers.getStatus().equals("I"))
		{
			if(!mnBlockedUsers.getBlockedLevel().equals("Crowd"))
			{
				body="Your account is now activated for accessing Musicnote.";
				subject="Admin Un blocked your login ID";
			}
			else
			{
				body="Your account is now activated for sharing a note on crowd. ";
				subject="Admin Un blocked your CrowdShare";
			}
		}
		message=MailContent.sharingNotification+mnBlockedUsers.getUserName()+MailContent.sharingNotification1+" "+body+MailContent.signature2;
		sendMail.postEmail(recipients, subject,message);
		logger.info("mailId"+recipients);
	}
	catch(Exception e)
	{
		logger.error("Exception while sendingMailOnAdminBlockUser dao :" + e);
	}
}
//get Admin file details
@Override
public List<MnAdminFiles> getMnAdminFilesDetails() {
	if(logger.isDebugEnabled())
		logger.debug("getMnAdminFilesDetails method :");
	List<MnAdminFiles> mnAdminFileList=null;
	Query query=null;
	try 
	{
		query=new Query(Criteria.where("status").is("A"));
		mnAdminFileList=mongoOperations.find(query,MnAdminFiles.class, JavaMessages.Mongo.MNADMINFILES);
	}
	catch (Exception e)
	{
		logger.error("Exception in getMnAdminFilesDetails impl!"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getMnAdminFilesDetails method :");
	return mnAdminFileList;
}
//insert file Details for admin
@Override
public String insertFileDetailsForAdmin(MnAdminFiles mnAdminFiles) {
	if(logger.isDebugEnabled())
		logger.debug("while insertFileDetailsforAdmin method for AdminDaoImpl: name:  "+mnAdminFiles.getAdminFileName());
		String status="";
		Query query=null;
		Integer fileId = 0;
		try
		{
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnAdminFiles> listFiles = mongoOperations.find(query,MnAdminFiles.class,JavaMessages.Mongo.MNADMINFILES);
			if (listFiles != null && listFiles.size() != 0)
			{
				for (MnAdminFiles lastUser : listFiles)
				{
					if (fileId <= lastUser.getAdminFileId()) 
						fileId = lastUser.getAdminFileId() + 1;
				}
			}
			else
			{
				fileId = 1;
			}
			mnAdminFiles.setAdminFileId(fileId);
			mongoOperations.insert(mnAdminFiles,JavaMessages.Mongo.MNADMINFILES);
			List<MnUsers> listUser = mongoOperations.findAll(MnUsers.class,JavaMessages.Mongo.MNUSERS);
			Update update =new Update();
			Query updateQuery=new Query();
			if(!listUser.isEmpty())
			{
				updateQuery.with(new Sort(Sort.Direction.ASC, "_id"));
				update.set("adminNotificationFlag",true);
				mongoOperations.updateMulti(updateQuery, update, JavaMessages.Mongo.MNUSERS);
			}
			status="success";
		}
		catch (Exception e) {
			logger.error("Exception While insertFileDetailsforAdmin User:"+e.getMessage());
			status="failed";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for while insertFileDetailsforAdmin method for AdminDaoImpl: name:  "+mnAdminFiles.getAdminFileName());
	return status;
}
//delete file for admin
@Override
public String deleteUploadedFile(MnAdminFiles mnAdminFiles) 
{
	if(logger.isDebugEnabled())
		logger.debug("deleteUploadedFile method called:");
		String status="";
		Query query =null;
		List<MnAdminFiles> mnAdminFileList=null;
		Integer adminFileId= 0;
		MnAdminFiles adminFiles=null;
		try 
		{
			Query queryList=new Query(Criteria.where("adminFileId").gt(mnAdminFiles.getAdminFileId()));
			mnAdminFileList  = mongoOperations.find(queryList,MnAdminFiles.class,JavaMessages.Mongo.MNADMINFILES);
			query =new Query(Criteria.where("adminFileId").is(mnAdminFiles.getAdminFileId()));
			adminFiles=mongoOperations.findOne(query, MnAdminFiles.class, JavaMessages.Mongo.MNADMINFILES);
			status = adminFiles.getAdminFilePath();
			/*Update update = new Update();
			update.set("deleteDate", mnAdminFiles.getDeleteDate());
			update.set("status", "I");
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNADMINFILES);*/
			mongoOperations.remove(query, MnAdminFiles.class, JavaMessages.Mongo.MNADMINFILES);
			mongoOperations.remove(queryList, MnAdminFiles.class, JavaMessages.Mongo.MNADMINFILES);
			for(MnAdminFiles mnAdmin:mnAdminFileList)
			{
				Query queryFileId=new Query();
				query.with(new Sort(Sort.Direction.ASC, "_id"));
				List<MnAdminFiles> listUser = mongoOperations.find(queryFileId,MnAdminFiles.class,JavaMessages.Mongo.MNADMINFILES);
				if (listUser != null && listUser.size() != 0)
				{
					for (MnAdminFiles lastUser : listUser)
					{
						if (adminFileId <= lastUser.getAdminFileId()) 
							adminFileId = lastUser.getAdminFileId() + 1;
					}
				}
				else
				{
					adminFileId = 1;
				}
				mnAdmin.setAdminFileId(adminFileId);
				mongoOperations.insert(mnAdmin, JavaMessages.Mongo.MNADMINFILES);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception While deleteUploadedFile dao Impl:"+e.getMessage());
			status="failed";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for deleteUploadedFile method called:");
	return status;
}
//check security question
@Override
public String checkSecurityQuestion(String questionId) {
	if(logger.isDebugEnabled())
		logger.debug("checkSecurityQuestion method called: questionId "+questionId);
		String status="";
		Query query =null;
		MnSecurityQuestions mnSecurityQuestions=null;
		try 
		{
			query =new Query(Criteria.where("questionId").is(questionId));
			mnSecurityQuestions=mongoOperations.findOne(query,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);
			if(mnSecurityQuestions!=null)
			{
				status="failed";
			}
			else
			{
				status="success";
			}
		}
		catch (Exception e)
		{
			logger.error("Exception checkSecurityQuestion:"+e.getMessage());
			status="failed";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for checkSecurityQuestion method  questionId "+questionId);
	return status;
	
}
//get MnUserData for Admin AddPreference
@Override
public List<MnUsers> getMnUserDetails() 
{
	if(logger.isDebugEnabled())
		logger.debug("getMnUserDetails method :");
	List<MnUsers> mnUser = null;
	try
	{
		mnUser=mongoOperations.findAll(MnUsers.class, JavaMessages.Mongo.MNUSERS);
	}
	catch (Exception e) {
		logger.error("Exception in getMnUserDetails impl!"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getMnUserDetails method");
	return mnUser;
}
//Admin add prefrenceflag on MnUserTable
@Override
public String addPrefrenceFlag(String userId,String addPrefrence) 
{
	if(logger.isDebugEnabled())
		logger.debug("addPrefrenceFlag dao impl method :userId:  "+userId);
	Update update = new Update();
	Query query = null;
	boolean userFlag;
	String status="";
	try
	{
		query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
		if(addPrefrence.equals("on"))
		{
			userFlag=true;
		}
		else
		{
			userFlag=false;
		}
		update.set("addUserFlag", userFlag);
		mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
		status="success";
	}
	catch (Exception e) {
		logger.error("Exception in addPrefrenceFlag impl!"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for addPrefrenceFlag dao impl method :userId:  "+userId);
	return status;
}
//get compalintid for crowd complaint delete button block
@Override
public String crowdComplaintDeleteButtonBlock(String complaintId) 
{
	if(logger.isDebugEnabled())
		logger.debug("crowdComplaintDeleteButtonBlock dao method called: complaintId:  "+complaintId);
	String status="";
	Query query =null;
	MnComplaintAction mnComplaintAction=null;
	try
	{
		query= new Query(Criteria.where("compliantId").is(Integer.parseInt(complaintId)).and("deleteFor").is("Note"));
		mnComplaintAction=mongoOperations.findOne(query, MnComplaintAction.class,  JavaMessages.Mongo.MNCOMPLAINTACTION);
		if(mnComplaintAction!=null)
		{
			status="success";
		}
	}
	catch (Exception e)
	{
		logger.error("Exception While crowdComplaintDeleteButtonBlock dao Impl:"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for crowdComplaintDeleteButtonBlock dao method called: complaintId:  "+complaintId);
	return status;
}

@Override
public MnUsers getUserDetailsObject(Integer userId) {
	if(logger.isDebugEnabled())
		logger.debug("getUserDetailsObject method called userId:  "+userId);
		MnUsers mnUser = null;
		Query query = null;
		try{
			query = new Query(Criteria.where("userId").is(userId));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
		}catch (Exception e) {
			logger.error("Exception in getUserDetailsObject impl!"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getUserDetailsObject dao impl userId:  "+userId);
		return mnUser;
}
//get Admin file details for help page
@Override
public List<MnAdminFiles> getUploadedFilesForHelp() {
	if(logger.isDebugEnabled())
		logger.debug("getUploadedFilesForHelp method :");
	List<MnAdminFiles> mnAdminFileList=null;
	try 
	{
		//mnAdminFileList=mongoOperations.findAll(MnAdminFiles.class, JavaMessages.Mongo.MNADMINFILES);
		
		Query query1 = new  Query(Criteria.where("status").is("A"));
		mnAdminFileList  = mongoOperations.find(query1,MnAdminFiles.class,JavaMessages.Mongo.MNADMINFILES);
		
		
	}
	catch (Exception e)
	{
		logger.error("Exception in getUploadedFilesForHelp impl!"+e.getMessage());
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getUploadedFilesForHelp");
	return mnAdminFileList;
}
@Override
public List<MnUsers> fetchUserDetailsBasedOnReportSignUpLevel(String reportLevel,String reportStartDate,String reportEndDate) {
	if(logger.isDebugEnabled())
		logger.debug("fetchUserDetailsBasedOnReportSignUpLevel dao impl method called reportLevel: "+reportLevel);
    List<MnUsers> mnUserList1 = new ArrayList<MnUsers>();;
    Date date = new Date();
    DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
    Query query = null;
    try
    {
        if((reportLevel.contains("signUpReport")==true)&&(reportStartDate.equals("")==true)&&(reportEndDate.equals("")==true)){
            query = new Query();
            mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
        }
        else if((reportLevel.contains("signUpReport")==true)&&(reportStartDate.equals("")!=true) &&(reportEndDate.equals("")==true)){
            reportEndDate=format.format(date);
            query = new Query(Criteria.where("startDate").gte(reportStartDate).lte(reportEndDate));
            mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
        }
        else if((reportLevel.contains("signUpReport")==true)&&(reportStartDate.equals("")!=true) &&(reportEndDate.equals("")!=true)){
            query = new Query(Criteria.where("startDate").gte(reportStartDate).lte(reportEndDate));
            mnUserList1 = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
        }
        
    }
    catch (Exception e)
    {
        logger.error("Exception while fetching details in fetchUserDetailsBasedOnReportSignUpLevel:" + e);

    }
    if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserDetailsBasedOnReportSignUpLevel dao impl method called reportLevel: "+reportLevel);
    return mnUserList1;
}
@Override
public List<MnUserLogs>  fetchUserDetailsBasedOnReportLoginLevel(String reportLevel,String reportStartDate,String reportEndDate){
	if(logger.isDebugEnabled())
		logger.debug("fetchUserDetailsBasedOnReportLoginLevel method called reportLevel:  "+reportLevel);
    List<MnUserLogs> mnUserList1 = null;
    Date date = Calendar.getInstance().getTime();
    DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    Query query = null;
    try
    {
        if((reportLevel.contains("loginReport")==true)&&(reportStartDate.equals("")==true)&&(reportEndDate.equals("")==true)){
            query = new Query();
            mnUserList1 = mongoOperations.find(query, MnUserLogs.class, JavaMessages.Mongo.MNUSERLOGDETAIL);
        }
        else if((reportLevel.contains("loginReport")==true)&&(reportStartDate.equals("")!=true) &&(reportEndDate.equals("")==true)){
            
            reportEndDate=format.format(date);
            query = new Query(Criteria.where("loginTime").gte(reportStartDate).lte(reportEndDate));
            mnUserList1 = mongoOperations.find(query, MnUserLogs.class, JavaMessages.Mongo.MNUSERLOGDETAIL);
        }
        else if((reportLevel.contains("loginReport")==true)&&(reportStartDate.equals("")!=true) &&(reportEndDate.equals("")!=true)){
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date endDate = (Date) formatter.parse(reportEndDate);
            Calendar c = Calendar.getInstance(); 
            c.setTime(endDate); 
            c.add(Calendar.DATE, 1);
            endDate = c.getTime();
            reportEndDate=formatter.format(endDate);
            query = new Query(Criteria.where("loginTime").gte(reportStartDate).lte(reportEndDate));
            mnUserList1 = mongoOperations.find(query, MnUserLogs.class, JavaMessages.Mongo.MNUSERLOGDETAIL);
        }
        
    }
    catch (Exception e)
    {
        logger.error("Exception while fetching details in fetchUserDetailsBasedOnReportLoginLevel:" + e);

    }
    if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserDetailsBasedOnReportLoginLevel method called reportLevel:  "+reportLevel);
    return mnUserList1;
}
@Override
public MnUsers getViewUserDetails(Integer userId) {
    MnUsers mnUserList = null;
    if(logger.isDebugEnabled())
    logger.debug("while fetching user from  getViewUserDetails method: "+userId);
    Query query = null;
    try
    {
        query = new Query(Criteria.where("userId").is(userId));
        mnUserList = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
    }
    catch (Exception e)
    {
        logger.error("Exception getViewUserDetails method :" + e);

    }
    if(logger.isDebugEnabled())
    logger.debug("Return response for getViewUserDetails method : ");
    return mnUserList;
}

@Override
public String checkBlockedUserForAddPreference(String userId) 
{
	if(logger.isDebugEnabled())
	    logger.debug("while  checkBlockedUserforAddPreference method: userId:  "+userId);
	Query query=null;
	MnBlockedUsers mnBlockedUsers=null;
	String status="";
	try
	{
		query=new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
		mnBlockedUsers=mongoOperations.findOne(query, MnBlockedUsers.class, JavaMessages.Mongo.MNBLOCKEDUSERS);
		if(mnBlockedUsers!=null)
		{
			status="success";
		}
	}
	catch(Exception e)
	{
		logger.error("Exception checkBlockedUserforAddPreference :" + e);
	}
	if(logger.isDebugEnabled())
	    logger.debug("Return response for while  checkBlockedUserforAddPreference method: userId:  "+userId+" status:  "+status);
	return status;
}
@Override
public String insertBlogDetails(MnBlog mnBlog) 
{
    if(logger.isDebugEnabled())
        logger.debug("insertBlogDetails method called");
    String status="";
    Query query=null;
    Integer blogId = 0;
    try
    {
        query=new Query();
        query.with(new Sort(Sort.Direction.ASC, "_id"));
        List<MnBlog> mnBlogLis=mongoOperations.find(query, MnBlog.class, JavaMessages.Mongo.MNBLOG);
        if(mnBlogLis!=null && mnBlogLis.size()!=0)
        {
            for(MnBlog lastUser:mnBlogLis)
            {
                if(blogId<=lastUser.getBlogId())
                    blogId=lastUser.getBlogId()+1;
            }
        }
        else
        {
            blogId=1;
        }
        mnBlog.setBlogId(blogId);
        mongoOperations.insert(mnBlog, JavaMessages.Mongo.MNBLOG);
        status="success";
        
    }
    catch(Exception e)
    {
        logger.error("Exception insertBlogDetails :" + e);
    }
    if(logger.isDebugEnabled())
        logger.debug("Return response for insertBlogDetails");
    return status;
}

@Override
public List<MnBlog> getBlogDetails(String status) 
{
    if(logger.isDebugEnabled())
        logger.debug("getBlogDetails method called");
    List<MnBlog> mnBlogList=null;
    Query query=null;
    try
    {
        query=new Query(Criteria.where("status").is(status));
        mnBlogList=mongoOperations.find(query, MnBlog.class, JavaMessages.Mongo.MNBLOG);
    }
    catch(Exception e)
    {
        logger.error("Exception getBlogDetails :" + e);
    }
    if(logger.isDebugEnabled())
        logger.debug("Return response for getBlogDetails");
    return mnBlogList;
}

@Override
public String updateBlogDetails(MnBlog mnBlog) {
	if(logger.isDebugEnabled())
        logger.debug("updateBlogDetails method called");
	 String status="";
	 try {
		 Query query=new Query(Criteria.where("blogId").is(mnBlog.getBlogId()));
			Update update = new Update();
			update.set("blogTitle", mnBlog.getBlogTitle());
			update.set("status", mnBlog.getStatus());
			update.set("blogDescription", mnBlog.getBlogDescription());
			update.set("blogDate", mnBlog.getBlogDate());
			update.set("tempDate", mnBlog.getTempDate());
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNBLOG);
			status="success";
	} catch (Exception e) {
		 logger.error("Exception updateBlogDetails :" + e);
	}
	 if(logger.isDebugEnabled())
	        logger.debug("Return response for updateBlogDetails");
	return status;
}

@Override
public String deleteBlogDetails(String blogId) {
	 if(logger.isDebugEnabled())
	        logger.debug("deleteBlogDetails method called");
	    MnBlog mnBlog=null;
	    Query query=null;
	    String status="";
	    Integer blog=0;
	    try
	    {
	    	blog=Integer.parseInt(blogId);
	        query=new Query(Criteria.where("blogId").is(blog));
	        mnBlog=mongoOperations.findOne(query, MnBlog.class, JavaMessages.Mongo.MNBLOG);
	        if(mnBlog!=null)
	        {
	        	mnBlog.setStatus("I");
	        	Update update = new Update();
	        	update.set("status", mnBlog.getStatus());
	        	mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNBLOG);
	        }
	    }
	    catch(Exception e)
	    {
	        logger.error("Exception deleteBlogDetails :" + e);
	    }
	    if(logger.isDebugEnabled())
	        logger.debug("Return response for deleteBlogDetails");
	    return status;
}



}