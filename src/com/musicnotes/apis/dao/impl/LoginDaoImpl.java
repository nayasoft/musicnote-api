package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.ILoginDao;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnLoginData;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.tokens.TokenGenerator;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.PasswordGenerator;
import com.musicnotes.apis.util.SendMail;


public class LoginDaoImpl extends BaseDaoImpl implements ILoginDao
{
	Logger logger = Logger.getLogger(LoginDaoImpl.class);

	@Override
	public String CheckLogin(String userName)
	{
		if(logger.isDebugEnabled())logger.debug("Checklogin method called for"+userName);

		MnUsers user = null;
		String userCheckId = "";
		try
		{
			Query query = new Query(Criteria.where("userName").is(userName.toLowerCase()));
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (user != null)
			{
				userCheckId=""+user.getUserId();
			}
			else
			{
				userCheckId="0";

			}
		}
		catch (Exception e)
		{
			logger.error("Exception occured when Checklogin method called for "+userName+" : " + e);
		}
		if(logger.isDebugEnabled())logger.debug("checkLogin terminated and returned userId for "+userName);

		return userCheckId;
	}
	@Override
	public String checkFaceBookLogin(String userName, String emailId,
			String facebookId,HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("checkFaceBookLogin method called for"+userName);

		MnUsers user = null;
		String userCheckId = "";
		Query usernameQuery=null;
		try
		{
			if(userName.indexOf(" ")!=-1){
				userName=userName.replaceAll(" ","");
			}
			Query query1 = new Query(Criteria.where("facebookId").is(facebookId));
			user = mongoOperations.findOne(query1, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (user != null){
				usernameQuery=new Query(Criteria.where("status").is("A").and("userName").is(userName.toLowerCase()));
				userCheckId=getValidUser(user,emailId,usernameQuery,request);
			}else{
				Query query = new Query(Criteria.where("emailId").is(emailId));
				user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
				if (user != null)
				{
					if(user.getFacebookId()== null){
						if (facebookId != null && !facebookId.equals("")){
						Update update=new Update();
						update.set("facebookId",facebookId);
						mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
						}
					}
					usernameQuery=new Query(Criteria.where("status").is("A").and("userName").is(userName.toLowerCase()));
					userCheckId=getValidUser(user,emailId,usernameQuery,request);
				//userCheckId=""+user.getUserId();
				}else
				{
					userCheckId="{\"userId\":\"0\"}";

				}
			}
			
		}
		catch (Exception e)
		{
			logger.error("Exception occured when checkFaceBookLogin method called for "+userName+" : " + e);
		}
		if(logger.isDebugEnabled())logger.debug("checkFaceBookLogin terminated and returned userId for userName>> "+userName);

		return userCheckId;
	}
	@Override
	public String getLogin(String userName, String password,HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("getLogin entered with userName : "+userName);

	String checkUser="";
	Set<String> userIdSet = new HashSet<String>();
	Set<String> sharedIdSet = new HashSet<String>();
	MnUsers user;
	Query query,usernameQuery=null;
	boolean studentCheck=false; 
	boolean adminVideoFlag=false;
	try{
		
		// check if user enter username or email
		userName=userName.toLowerCase();
		usernameQuery=new Query(Criteria.where("status").is("A").and("emailId").is(userName));
		user = mongoOperations.findOne(usernameQuery, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if(user==null)
		{
			usernameQuery=new Query(Criteria.where("status").is("A").and("userName").is(userName));
			user = mongoOperations.findOne(usernameQuery, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		
		// check if the user is available in db insert intime in db
		if (user != null)
		{
			byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), password);
			if (Arrays.equals(encryptedPassword, user.getPassword()))
			{
				checkUser=getValidUser(user,password,usernameQuery,request);
			}
			else
			{
				checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"0\"}";
				logger.error("Login unsuccessfull due to wrong password : "+userName);
			}
			
		
		}else{
			checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"0\"}";
			logger.error("Login unsuccessfull due to wrong username : "+userName);
		}
	}catch(Exception e){
		logger.error("error while getLogin method called for "+userName+":"+e);
	}
	if(logger.isDebugEnabled())logger.debug("getLogin has terminated and returned with user details And note/event sharing details for "+userName);

	return checkUser;
}

	
	public String getValidUser(MnUsers user,String Tokenvalue,Query usernameQuery,HttpServletRequest request){
		String checkUser="";
		Set<String> userIdSet = new HashSet<String>();
		Set<String> sharedIdSet = new HashSet<String>();
		Query query;
		boolean studentCheck=false; 
		boolean adminVideoFlag=false;
		//Check if user is Blocked or not
		try{
		//usernameQuery=new Query(Criteria.where("status").is("A").and("emailId").is(user.getUserName()));
		String status=validateBlockedList(user.getUserId());
		if(status.equals("unBlocked"))
		{
		JSONObject arbitraryPayload = new JSONObject();
		try {
		    arbitraryPayload.put("some", "arbitrary");
		    arbitraryPayload.put("data", "here");
		} catch (JSONException e) {
		    e.printStackTrace();
		}   
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();        
		String date = df.format(today);
		TokenGenerator tokenGenerator=new  TokenGenerator(user.getUserName()+""+Tokenvalue+""+date);
		String token = tokenGenerator.createToken(arbitraryPayload);
		
		MnUsersToken usersToken=new MnUsersToken();
		usersToken.setUserId(user.getUserId());
		usersToken.setLoginTime(date);
		usersToken.setTokens(token);
		usersToken.setCreatedDateTime(date);
		usersToken.setIpaddress(request.getRemoteHost()+request.getRemoteAddr());
		mongoOperations.insert(usersToken, JavaMessages.Mongo.MNUsersToken);
		// update user login status if added students only
		if(user.getUserRole().equalsIgnoreCase("Music Student") && user.getAddedUserStatus()!=null && user.getAddedUserStatus().equalsIgnoreCase("Inactive"))
		{
			Update update=new Update();
			update.set("addedUserStatus","Active");
			mongoOperations.updateFirst(usernameQuery, update, JavaMessages.Mongo.MNUSERS);
			studentCheck=true; // this flag used to check student via add usser to navigate profile page
		}
		
		// used to update adminNotification flag
		if(user.isAdminNotificationFlag())
		{
			Update update=new Update();
			update.set("adminNotificationFlag",false);
			mongoOperations.updateFirst(usernameQuery, update, JavaMessages.Mongo.MNUSERS);
			adminVideoFlag=true; // this flag used to check student via add user to navigate profile page
		}

		
		//if(mailflag) 
		//{
		
		// check if the mail flag is true or not
		if(user.isMailCheckFlag())
				{
				if(user.isRequestPending()){ // check if any one shared or copied via email
					
					// sharing book level note level
					Query query2 = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("status").is("E"));
					List<MnNotesSharingDetails> mnNotesSharingDetailsList = mongoOperations.find(query2, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
					if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
						for(MnNotesSharingDetails details : mnNotesSharingDetailsList){
							if(!userIdSet.contains(details.getUserId())){
								userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
							}
						}
					}
					
					// copy note level
					query = new Query(Criteria.where("emailId").in(user.getEmailId()).and("sharingLevel").is("copy").and("status").is("P"));
					List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.find(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
					
					for(MnNonUserSharingDetails details :mnNonUserSharingDetailsList){
						if(!userIdSet.contains(details.getUserId())){
							userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
						}
					}
					
				}
				
				//get normal sharing event to show at the time of login
				Query shareId = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("sharingStatus").is("P").and("status").is("E"));
				List<MnEventsSharingDetails> eventList = mongoOperations.find(shareId, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				
				if(eventList!=null && !eventList.isEmpty())
				{
					for(MnEventsSharingDetails details:eventList)
					{
						sharedIdSet.add(details.getUserId()+"~"+details.getListId()+"~"+details.getNoteId());
					}
				}
				
				if(userIdSet.isEmpty()){
					if(user.getEmailId()!=null && !user.getEmailId().isEmpty()){
						checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\",\"userMailId\":\""+user.getEmailId()+"\"}";
					}else{
						checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\",\"userMailId\":\"\"}";
					}
				}else{
					if(user.getEmailId()!=null && !user.getEmailId().isEmpty()){
						checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\""+userIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\",\"userMailId\":\""+user.getEmailId()+"\"}";
					}else{
						checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\""+userIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\",\"userMailId\":\"\"}";
					}
				}
				
				/* commented by ramaraj this is executed only for student via adding in contact page
				if(userIdSet.isEmpty() && user != null && user.getUserFirstName()==null &&user.getUserLastName()==null){
					studentCheck=true;
					checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";	
				}*/
				
			}else
				{
					checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\"mailcheck\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
				}
		// commented by ramaraj for removing code was handled data issue
		/*}
		else
		{
			if(user.isRequestPending())
			{
				// sharing book level note level
				Query query2 = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("status").is("E"));
				List<MnNotesSharingDetails> mnNotesSharingDetailsList = mongoOperations.find(query2, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
					for(MnNotesSharingDetails details : mnNotesSharingDetailsList){
						if(!userIdSet.contains(details.getUserId())){
							userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
						}
					}
				}
				
				// copy note level
				query = new Query(Criteria.where("emailId").in(user.getEmailId()).and("sharingLevel").is("copy").and("status").is("P"));
				List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.find(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
				
				for(MnNonUserSharingDetails details :mnNonUserSharingDetailsList){
					if(!userIdSet.contains(details.getUserId())){
						userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
					}
				}
			}
			
			//get normal sharing event to show at the time of login
			
			Query shareId = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("sharingStatus").ne("A"));
			List<MnEventsSharingDetails> eventList = mongoOperations.find(shareId, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			
			if(eventList!=null && !eventList.isEmpty())
			{
				for(MnEventsSharingDetails details:eventList)
				{
					sharedIdSet.add(details.getUserId()+"~"+details.getListId()+"~"+details.getNoteId());
				}
			}
			
			
			if(userIdSet.isEmpty()){
				checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
			}else{
				checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\""+userIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
			}
			if(userIdSet.isEmpty() && user != null && user.getUserFirstName()==null &&user.getUserLastName()==null){	
				studentCheck=true;
				checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
			}
		}*/
		}
		else if(status.equals("blocked"))
		{
			checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"blocked\"}";
			logger.error("Login unsuccessfull for blocked user : "+user.getUserName());
		}
		}catch(Exception e)
		{
			logger.error("error while getValidUser method called for "+user.getUserName()+": " + e);
		}
		return checkUser;
		
	}

	@SuppressWarnings("null")
	@Override
	public String unsubcribeNotification(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has entered for "+mnUsers.getUserName());
		String userName=mnUsers.getEmailId().toLowerCase();
		String password=mnUsers.getPasswordText();
		MnUsers user = null;
		String userCheckId = "";
		try
		{
			Query query = new Query(Criteria.where("emailId").is( mnUsers.getEmailId().toLowerCase()));
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
			byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), password);
			if ((user != null)&&(Arrays.equals(encryptedPassword, user.getPassword())))
			{
				Query query1=new Query(Criteria.where("userId").is((user.getUserId())));
			
				Update update=new Update();
				update.set("notificationFlag","no");
				
				mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
				
				userCheckId="success";
				
			}
			else
			{
				userCheckId="nonuser";

			}
			writeLog(user.getUserId(), "A", "Unsubcribed Notification From All the activities", "settingss", null, null, null, null);
		}
		catch (Exception e)
		{
			logger.error("error while unsubcribeNotification method called for "+mnUsers.getUserName()+": " + e);
		}
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has terminated with success/fail message"+userCheckId);

		return userCheckId;
	}

	@Override
	public String updateLoginCheck(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("updateLoginCheck has entered for update mail Flag::"+mnUsers.getEmailId());

		MnUsers user = new MnUsers();
	String userCheckId = "";
	try
	{
		//Query query = new Query(Criteria.where("emailId").is( mnUsers.getEmailId()).and("mailCheckId").is(mnUsers.getMailCheckId()));
				Query query = new Query(Criteria.where("mailCheckId").is(mnUsers.getMailCheckId()).orOperator(
						Criteria.where("userName").is(mnUsers.getEmailId().toLowerCase()),
						Criteria.where("emailId").is(mnUsers.getEmailId().toLowerCase())));
		user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if (user != null)
		{
			Query query1=new Query(Criteria.where("userId").is((user.getUserId())));
		
			Update update=new Update();
			update.set("mailCheckFlag",true);
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
			
			userCheckId="success";
			if(logger.isDebugEnabled())logger.debug("verification code for user : "+mnUsers.getUserName()+" successfully verified");
		}
		else
		{
			userCheckId="nonuser";
			logger.error("Error in verification code for user : "+mnUsers.getUserName()+ " code : "+mnUsers.getMailCheckId());

		}
	}
	catch (Exception e)
	{
		logger.error("Exception in verification code for user : "+mnUsers.getUserName()+ " code : "+mnUsers.getMailCheckId() +" Exception : "+e);
	}
	if(logger.isDebugEnabled())logger.debug("updateLoginCheck has terminated with updated user details for "+mnUsers.getUserName());

	return userCheckId;
	}
	
	@Override
	public String updateLoginCheckViaFBLogin(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("updateLoginCheck has entered for update mail Flag::FacebookId:"+mnUsers.getFacebookId());

		MnUsers user = new MnUsers();
	String userCheckId = "";
	try
	{
		//Query query = new Query(Criteria.where("emailId").is( mnUsers.getEmailId()).and("mailCheckId").is(mnUsers.getMailCheckId()));
				Query query = new Query(Criteria.where("mailCheckId").is(mnUsers.getMailCheckId()).orOperator(
						Criteria.where("facebookId").is(mnUsers.getFacebookId())));
		user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if (user != null)
		{
			Query query1=new Query(Criteria.where("userId").is((user.getUserId())));
		
			Update update=new Update();
			update.set("mailCheckFlag",true);
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
			
			userCheckId="success";
			if(logger.isDebugEnabled())logger.debug("verification code for user : "+mnUsers.getFacebookId()+" successfully verified");
		}
		else
		{
			userCheckId="nonuser";
			logger.error("Error in verification code for Facebook user : "+mnUsers.getFacebookId()+ " code : "+mnUsers.getMailCheckId());

		}
	}
	catch (Exception e)
	{
		logger.error("Exception in verification code for Facebook user : "+mnUsers.getFacebookId()+ " code : "+mnUsers.getMailCheckId() +" Exception : "+e);
	}
	if(logger.isDebugEnabled())logger.debug("updateLoginCheck has terminated with updated user details for "+mnUsers.getFacebookId());

	return userCheckId;
	}
	
	@Override
	public String resetVerificationCode(MnUsers user)
	{
		if(logger.isDebugEnabled())logger.debug("resetVerificationCode method called for mailId:"+user.getEmailId());

		//MnUsers user = new MnUsers();
	String userCheckId = "";
	try
	{
		//Query query = new Query(Criteria.where("emailId").is(user.getEmailId().toLowerCase()));
		Query query = new Query(Criteria.where("status").is("A").orOperator(Criteria.where("userName").is(user.getEmailId().toLowerCase()),Criteria.where("emailId").is(user.getEmailId().toLowerCase())));

		MnUsers users = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		
		if (users != null)
		{
			Query query1=new Query(Criteria.where("emailId").is((users.getEmailId().toLowerCase())).and("status").is("A"));
		
			Update update=new Update();
			update.set("mailCheckId",user.getMailCheckId());
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
			
				SendMail sendMail=new SendMail(); 	
				SendMail sendMail1=new SendMail();
				String recipients[]={users.getEmailId().toLowerCase()};
				String userLevel=users.getUserLevel();
				try{
				String message=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
					"<br> User Name : " + users.getEmailId().toLowerCase() +
					"<br> Verification Code : " + user.getMailCheckId() +
					"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
					"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
					MailContent.signature;
		
				String messageForTrailUser=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
					"<br> User Name : " + users.getEmailId().toLowerCase() +
					"<br> Verification Code : " + user.getMailCheckId() +
					"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
					"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
					MailContent.signature;
				//Sending mail to inform users that they are on trail period
				if(userLevel.equals("Trial-Start")){
					sendMail1.postEmail(recipients, users.getUserFirstName()+" welcome to Musicnote! ",messageForTrailUser);	
				}else{
					sendMail.postEmail(recipients, users.getUserFirstName()+" welcome to Musicnote! ",message);
				}
				if(logger.isDebugEnabled())
					logger.debug("Return response for resetVerificationCode method for mailId: "+user.getEmailId());
				
				
				userCheckId="success";
				}catch(Exception e){
					logger.error("Exception occured in resetVerificationCode methodfor mailId: "+user.getEmailId()+":"+e);
				}
			
			
		}
		else
		{
			userCheckId="nonuser";

		}
	}
	catch (Exception e)
	{
		logger.error("error while resetVerificationCode method callled for "+user.getEmailId()+":" + e);
	}
	if(logger.isDebugEnabled())logger.debug("resetVerificationCode has terminated with updated user details for "+user.getEmailId());

	return userCheckId;
	}
	
	
	@Override
	public String doUserLoginDetails(MnUserLogs mnUserLogs)
	{
		if(logger.isDebugEnabled())logger.debug("Users Login details has entered for "+mnUserLogs.getUserId());

		try
		{
			mongoOperations.insert(mnUserLogs, JavaMessages.Mongo.MNUSERLOGDETAIL);
		}
		catch (Exception e)
		{
			logger.error("error while doUserLoginDetails method called for "+mnUserLogs.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())logger.debug("doUserLoginDetails has inserted and returned success for "+mnUserLogs.getUserId());

		return "success";
	}

	@Override
	public String loginAuthentication(String userId, String inTime, String role) {
		if(logger.isDebugEnabled())logger.debug("loginAuthendication has entered "+userId+" inTime: "+inTime);

		MnLoginData loginDetails = null;
		String flag=""; 

		try
		{
			Query query = new Query(Criteria.where("inTime").is(inTime));
			loginDetails = mongoOperations.findOne(query, MnLoginData.class, JavaMessages.Mongo.MNLoginData);
		}
		catch (Exception e)
		{
			logger.error("loginAuthentication Info   :" + e);
		}
		if (loginDetails != null && loginDetails.getOutTime().equals("0"))
		{
			if(role.equals("Administrator")){
				flag="true";
			}else{
				flag="false";
			}
			if(logger.isDebugEnabled())logger.debug("loginAuthendication has terminated with returned of userId/Admin-yes for "+userId);

			return loginDetails.getUserId().toString()+"&"+flag;
		}
		else
		{
			if(logger.isDebugEnabled())logger.debug("loginAuthendication has terminated with returned zero for "+userId);

			return "0";
		}
	}

	@Override
	public String toGenerateTokens(MnUsersToken mnUsersToken) {
		if(logger.isDebugEnabled())logger.debug("Users token generations has entered for "+mnUsersToken.getUserId());
		String status="success";
		try
		{
			mongoOperations.insert(mnUsersToken, JavaMessages.Mongo.MNUsersToken);
		}
		catch (Exception e)
		{
			status="fail";
			logger.error("error while toGenerateTokens method called for "+mnUsersToken.getUserId()+":"+e);
		}
		if(logger.isDebugEnabled())logger.debug("User Token details has been changed and returned success for user"+mnUsersToken.getUserId());

		return status;
	}

}
