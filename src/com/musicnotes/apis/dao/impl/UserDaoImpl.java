package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import com.musicnotes.apis.dao.interfaces.IUserDao;
import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnAdminSecurityQuestion;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnInvitedUsers;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnSecurityQuestions;
import com.musicnotes.apis.domain.MnSubEventDetails;
import com.musicnotes.apis.domain.MnSubscription;
import com.musicnotes.apis.domain.MnSubscriptionMulti;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.tokens.TokenGenerator;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.PasswordGenerator;
import com.musicnotes.apis.util.RandomPassGenerator;
import com.musicnotes.apis.util.SendMail;


public class UserDaoImpl extends BaseDaoImpl implements IUserDao
{
	Logger logger = Logger.getLogger(UserDaoImpl.class);
	DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss a");

	@Override
	public String createUser(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())
			logger.debug("while creating a createUser method for UserDaoImpl: userName:  "+mnUsers.getUserName());	

		Query query=null;
		Integer userId = 0;
		Set<String> sharedIdSet = new HashSet<String>();
		String responseText= ",\"requestPending\":"+false; 
		Date date = new Date();
		try
		{
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnUsers> listUser = mongoOperations.find(query,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (listUser != null && listUser.size() != 0)
			{
				for (MnUsers lastUser : listUser)
				{
					if (userId <= lastUser.getUserId()) 
						userId = lastUser.getUserId() + 1;
				}
			}
			else
			{
				userId = 1000;
			}
			mnUsers.setUserId(userId);
			// password is generated based on userId
			mnUsers.setPassword(PasswordGenerator.encoder(mnUsers.getUserId().toString(),mnUsers.getPasswordText()));
			
			List<MnNotesSharingDetails> mnNotesSharingDetailsList =  new ArrayList<MnNotesSharingDetails>();
			List<MnEventsSharingDetails> mnEventsSharingDetailsList = new ArrayList<MnEventsSharingDetails>();
			Set<String> userIdSet = new HashSet<String>();
			query = new Query(Criteria.where("emailId").in(mnUsers.getEmailId()));
			List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.find(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS); 
			if(mnNonUserSharingDetailsList != null && !mnNonUserSharingDetailsList.isEmpty())
			{
				mnUsers.setRequestPending(true);
				responseText = ",\"requestPending\":"+true;
				int scharedEventId = 0;
				List<MnEventsSharingDetails> sharingDetailsList = mongoOperations.findAll(MnEventsSharingDetails.class, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				if(sharingDetailsList!= null &&  mnNonUserSharingDetailsList.size() != 0){
					scharedEventId = sharingDetailsList.size()+1;
				}else{
					scharedEventId = 1;
				}
				
				for(MnNonUserSharingDetails details :mnNonUserSharingDetailsList){
					if(!userIdSet.contains(details.getUserId())){
						userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
					}
					if(!(details.getSharingLevel().equals("copy")))
					{
					MnNotesSharingDetails notesSharingDetails = new MnNotesSharingDetails();
					notesSharingDetails.setListId(details.getListId());
					notesSharingDetails.setListType(details.getListType());
					notesSharingDetails.setNoteId(details.getNoteId());
					notesSharingDetails.setNoteLevel(details.getNoteLevel());
					if(details.getNoteId() == 0){
						notesSharingDetails.setSharingLevel("book");
					}else{
						notesSharingDetails.setSharingLevel("note");
					}
					notesSharingDetails.setStatus("E");
					notesSharingDetails.setShareAllContactFlag(false);
					notesSharingDetails.setSharedDate(formatter.format(date));
					notesSharingDetails.setSharingGroupId(0);
					notesSharingDetails.setSharingUserId(mnUsers.getUserId());
					notesSharingDetails.setUserId(details.getUserId());
					mnNotesSharingDetailsList.add(notesSharingDetails);
					if(details.getListType().equals("schedule")){		
						MnEventsSharingDetails eventsSharingDetails = new MnEventsSharingDetails();
						
						eventsSharingDetails.setUserId(details.getUserId());
						eventsSharingDetails.setSharingUserId(mnUsers.getUserId());
						eventsSharingDetails.setListId(details.getListId());
						eventsSharingDetails.setNoteId(details.getNoteId());
						eventsSharingDetails.setNoteLevel(details.getNoteLevel());
						eventsSharingDetails.setListType(details.getListType());
						eventsSharingDetails.setEventSharedId(scharedEventId);
						eventsSharingDetails.setStatus("E");
						eventsSharingDetails.setShareAllContactFlag(false);
						eventsSharingDetails.setSharingGroupId(0);
						if(details.getNoteId() == 0){
							eventsSharingDetails.setSharingLevel("book");
						}else{
							eventsSharingDetails.setSharingLevel("note");
						}
						eventsSharingDetails.setSharingStatus("P");
						mnEventsSharingDetailsList.add(eventsSharingDetails);
						scharedEventId++;
					}	
				   }
				}
			}
			
			// update in Invited user table
			Query query3 = new Query(Criteria.where("invitedMailID").is(mnUsers.getEmailId()).and("status").is("A"));
			MnInvitedUsers exist = mongoOperations.findOne(query3, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			if(exist!=null)
			{
				Update inviteUpdate=new Update();
				inviteUpdate.set("userCreated", true);
				mongoOperations.updateFirst(query3, inviteUpdate, JavaMessages.Mongo.MNInvitedUsers);
			}
				
			
			mongoOperations.insert(mnUsers,JavaMessages.Mongo.MNUSERS);
			multiUserPay(mnUsers.getEmailId());
			
			if(mnNotesSharingDetailsList!=null && !mnNotesSharingDetailsList.isEmpty() ){
				mongoOperations.insert(mnNotesSharingDetailsList,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}
			if(mnEventsSharingDetailsList!= null && !mnEventsSharingDetailsList.isEmpty()){
				mongoOperations.insert(mnEventsSharingDetailsList, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			}
			
			Query shareId = new Query(Criteria.where("sharingUserId").is(mnUsers.getUserId()).and("sharingStatus").ne("A"));
			List<MnEventsSharingDetails> eventList = mongoOperations.find(shareId, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			
			
			if(eventList!=null && !eventList.isEmpty())
			{
				for(MnEventsSharingDetails details:eventList)
				{
					sharedIdSet.add(details.getUserId()+"~"+details.getListId()+"~"+details.getNoteId());
				}
			}
			String username=mnUsers.getUserName();
			if(userIdSet.isEmpty()){
				responseText = responseText+",\"userIdSet\":\"\",\"username\":\""+username+"\"";
			}else{
				responseText = responseText+",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"userIdSet\":\""+userIdSet.toString()+"\",\"username\":\""+username+"\"";
			}
			logger.debug("Return response for creatuser method : userId:  "+userId);
			return "{\"userId\": \""+userId+"\""+responseText+",\"username\":\""+username+"\"}";
		}
		catch (Exception e)
		{
			logger.error("Exception while createUser :" + e);
			return "0";
		}
		
	}
	
	public void multiUserPay(String mailId)
	{
		if(logger.isDebugEnabled())
			logger.debug("multiUserPay method for UserDaoImpl: mailId:  "+mailId);
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try
		{
			Query query4 = new Query(Criteria.where("invitedMailID").is(mailId).and("status").is("A"));
			MnInvitedUsers userCheck = mongoOperations.findOne(query4, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			if(userCheck!=null)
			{
				Query query3=new Query(Criteria.where("subId").is(userCheck.getPaidSubId()));
				MnSubscription subscription=mongoOperations.findOne(query3,MnSubscription.class,JavaMessages.Mongo.MNSubscription);
				if(subscription!=null)
				{
					
					MnSubscriptionMulti multi=new MnSubscriptionMulti();
					Integer Id = 0;
					List<MnSubscription> mulUsers = mongoOperations.findAll(MnSubscription.class,JavaMessages.Mongo.MNSubscriptionMulti);
					if (mulUsers != null && mulUsers.size() != 0)
					{
						Id=mulUsers.size()+1;
					}
					
					String todayDate=null,endDate=null;
					int day=Integer.parseInt(subscription.getSubDays());
				if(userCheck.getUserSelectDate().equalsIgnoreCase("Signup-Date"))
				{
					Calendar today = Calendar.getInstance();
					todayDate=formatter.format(today.getTime());
					today.add(Calendar.DATE,30);
					endDate=formatter.format(today.getTime());
				}else
				{
					Calendar today = Calendar.getInstance();
					todayDate=subscription.getSubDate();
					today.setTime(formatter.parse(todayDate));
					today.add(Calendar.DATE, day);
					endDate=formatter.format(today.getTime());
				}
					multi.setId(Id);
					multi.setEmailId(mailId);
					multi.setSubId(subscription.getSubId());
					multi.setPaidUserId(subscription.getUserId());
					multi.setStartDate(todayDate);
					multi.setEndDate(endDate);
					multi.setStatus("A");
					
					Query query2=new Query(Criteria.where("emailId").is(mailId));
					MnUsers mnusers1=mongoOperations.findOne(query2,MnUsers.class,JavaMessages.Mongo.MNUSERS);
					if(mnusers1!=null && !mnusers1.equals(""))
					{
						Update update=new Update();
						update.set("endDate", endDate);
						update.set("userLevel", "Premium-Silver");
					mongoOperations.updateFirst(query2, update, JavaMessages.Mongo.MNUSERS);
					}
					multi.setUserId(mnusers1.getUserId());
					mongoOperations.insert(multi,JavaMessages.Mongo.MNSubscriptionMulti);
				}
			}
			
		}
		catch (Exception e) {
			logger.error("Exception while multiUserPay :" + e);
		}
		
	}
	
	@Override
	public MnUsers checkInviteuser(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())
			logger.debug("while check invite user for checkInviteuser method : userId:  "+mnUsers.getUserId());	
		MnUsers requestedUserCheck=null;
		MnFriends mnFriends=new MnFriends();
		try{
			Query query4 = new Query(Criteria.where("invitedMailID").is(mnUsers.getEmailId()).and("status").is("A"));
			MnInvitedUsers userCheck = mongoOperations.findOne(query4, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			if(userCheck!=null){
				Query query5 = new Query(Criteria.where("userId").is(userCheck.getUserId()).and("status").is("A"));
				requestedUserCheck = mongoOperations.findOne(query5, MnUsers.class, JavaMessages.Mongo.MNUSERS);
					int requestedId=requestedUserCheck.getUserId();
					String requestedUserName=requestedUserCheck.getUserName();
					String status="R";
					Date requestedDate=new Date();
					int currentId=mnUsers.getUserId();
					mnFriends.setRequestedUserId(requestedId);
					mnFriends.setRequestedUserName(requestedUserName);
					mnFriends.setStatus(status);
					mnFriends.setUserId(currentId);
					mnFriends.setRequestedDate(requestedDate);
					mongoOperations.insert(mnFriends, JavaMessages.Mongo.MNFRIEND);
					
		}
		
		}catch(Exception e){
			logger.error("Exception while checkInviteuser :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for checkInviteuser method : userId:  "+mnUsers.getUserId());
		return requestedUserCheck;
		}
	
	public String feedback(MnFeedbacks user){
		if(logger.isDebugEnabled())
		logger.debug("while user feedback for userFeedback method:");
		Date date = new Date();
		try{
			
			if(user!=null){
                Query query=new Query(Criteria.where("userId").is((user.getUserId())));
                MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
                user.setUserMailId(mnUsers.getEmailId());
				user.setDate(formatter.format(date));
				mongoOperations.insert(user,JavaMessages.Mongo.MNUSERFEEDBACK);
				return "success";
			}
		}catch(Exception e){
			logger.error("Exception while userFeedback :" + e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for userFeedback method : ");
		return "";
	}
	@Override
	public MnUsers getViewUserDetails(String userId) {
		MnUsers mnUserList = null;
		MnSecurityQuestions security=null;
		if(logger.isDebugEnabled())
		logger.debug("while fetching user from  getViewUserDetails method: "+userId);
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mnUserList = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception while getViewUserDetails :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getViewUserDetails method : userId:  "+userId);
		return mnUserList;
	}
	
	
	@Override
	public MnSecurityQuestions getSecurityQuestion(String userId) {
		MnSecurityQuestions security=null;
		if(logger.isDebugEnabled())
		logger.debug("while fetching user from  getSecurityQuestion method: userId: "+userId);
		try
		{
			
			Query query1=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			security=mongoOperations.findOne(query1,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);
			if(security!=null){
				
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while getSecurityQuestions :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getSecurityQuestions method : ");
		return security;
	}

	@Override
	public List<MnUsers> fetchUserDetails(String roleId) {
		if(logger.isDebugEnabled())
			logger.debug("fetchUserDetails method called");
		List<MnUsers> mnUserList = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userRole").is(roleId));
			mnUserList = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);

		}
		catch (Exception e)
		{
			logger.error("Exception while fetchUserDetails :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchUserDetails method : ");
		return mnUserList;
	}

	@Override
	public MnUsers memberList(Integer userId) {
		MnUsers memberList = null;
		if(logger.isDebugEnabled())
		logger.debug("while fetching memberList method: "+userId);

		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(userId));
			memberList = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);

		}
		catch (Exception e)
		{
			logger.error("Exception while fetching memberList :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for memberList method : ");
		return memberList;
	}

	@Override
	public String updateUser(MnUsers user) {
		if(logger.isDebugEnabled())
			logger.debug("while updating updateUser method: userId:  "+user.getUserId());
		try
		{
			MnUsers mnUsers=null;
			boolean passUpdate=false;
			Query query=new Query(Criteria.where("userId").is((user.getUserId())));
			mnUsers = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			Random random = new Random();
			 String[] startEndPart={"AZ","BX","CY","DB","EL","FM","GO","HU","IP","JR"};
			 String[] part1={"8j","9f","6y","5r","3h","4k","2d","1v","0o","7x"};
			 String[] part2={"*6","*9","*5","@1","@2","@3","K9","B6","w7","X5"};
			 String[] part3={"R0","*W","K@","YT","VZ","@S","X9","1F","M7","5S"};
			 String randomPass=startEndPart[random.nextInt(10)]+part1[random.nextInt(10)]+part2[random.nextInt(10)]+part3[random.nextInt(10)];
			
			Update update=new Update();
			
			// check if mailId was changed to send temp password
			if(mnUsers!=null)
			{
				if(!mnUsers.getUserRole().equals("Music Student")){
					if(!user.getTempEmail().toLowerCase().equals(user.getEmailId().toLowerCase()))
					{
					/*String randomPass = RandomPassGenerator.getRandomPassword();
					user.setPasswordText(randomPass);
					byte[] encryptedPassword = PasswordGenerator.encoder(mnUsers.getUserId().toString(), randomPass);
					user.setPassword(encryptedPassword);
					update.set("password",user.getPassword());*/
						user.setMailCheckId(randomPass);
						 update.set("mailCheckId",randomPass);
						 update.set("mailCheckFlag",false);
						 passUpdate=true;
					}
				}
				else if(mnUsers.getUserRole().equals("Music Student") && (mnUsers.getEmailId()==null || mnUsers.getEmailId().equals(""))&& (user.getEmailId()==null || user.getEmailId().equals("")))
				{
					update.set("mailCheckFlag",true);
				}
				else if(mnUsers.getUserRole().equals("Music Student") && (mnUsers.getEmailId()==null || mnUsers.getEmailId().equals(""))&& (user.getEmailId()!=null || !user.getEmailId().equals("")))
				{
					user.setMailCheckId(randomPass);
					update.set("mailCheckId",randomPass);
					update.set("mailCheckFlag",false);
					passUpdate=true;
				}
				else if(mnUsers.getUserRole().equals("Music Student") && (mnUsers.getEmailId()!=null || !mnUsers.getEmailId().equals(""))&& (user.getEmailId()==null || user.getEmailId().equals("")))
				{
					update.set("mailCheckFlag",true);
				}
				else
				{
					if(mnUsers.getEmailId()==null && user.getEmailId()!=null && !user.getEmailId().equals(""))
					{
						/*String randomPass = RandomPassGenerator.getRandomPassword();
						user.setPasswordText(randomPass);
						byte[] encryptedPassword = PasswordGenerator.encoder(mnUsers.getUserId().toString(), randomPass);
						user.setPassword(encryptedPassword);
						update.set("password",user.getPassword());*/
						user.setMailCheckId(randomPass);
						update.set("mailCheckId",randomPass);
						update.set("mailCheckFlag",false);
						passUpdate=true;
					}
					else if(user.getEmailId()!=null && !user.getTempEmail().toLowerCase().equals(user.getEmailId().toLowerCase()) && !user.getEmailId().equals(""))
					{
						/*String randomPass = RandomPassGenerator.getRandomPassword();
						user.setPasswordText(randomPass);
						byte[] encryptedPassword = PasswordGenerator.encoder(mnUsers.getUserId().toString(), randomPass);
						user.setPassword(encryptedPassword);
						update.set("password",user.getPassword());*/
						user.setMailCheckId(randomPass);
						update.set("mailCheckId",randomPass);
						update.set("mailCheckFlag",false);
						passUpdate=true;
					}
						
				}
			}
			update.set("userFirstName",user.getUserFirstName());
			update.set("userName",user.getUserName());
			update.set("userLastName",user.getUserLastName());
			update.set("contactNumber",user.getContactNumber());
			update.set("emailId",user.getEmailId().toLowerCase());
			update.set("instrument",user.getInstrument());
			update.set("skillLevel",user.getSkillLevel());
			update.set("publicShareWarnMsgFlag",user.isPublicShareWarnMsgFlag());
			update.set("fbLogoutFlag",user.isFbLogoutFlag());
			update.set("cropFlag",mnUsers.isCropFlag());
			if(mnUsers.getUserRole().equals("Music Student")){
				
			if(user.getEmailId()!="" || user.getEmailId()!=null){
				update.set("notificationFlag","yes");	
			}else{
				update.set("notificationFlag","no");	
			}
			}else{
			update.set("notificationFlag",user.getNotificationFlag());
			}
			update.set("noteCreateBasedOn",user.getNoteCreateBasedOn());
			update.set("favoriteMusic",user.getFavoriteMusic());
			update.set("timeZone", user.getTimeZone());
			update.set("url", user.getUrl());
			update.set("description", user.getDescription());
			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
			writeLog(user.getUserId(), "A", "Updated profile information", "profile", "", "", null, null);
			logger.debug("Return response for updateUser method : ");
			
			  if(passUpdate)
			    {
			    	SendMail sendMail=new SendMail(); 
					String recipients[]={user.getEmailId().toLowerCase()};
					try{
						String message=MailContent.sharingNotification+user.getUserFirstName()+" "+user.getUserLastName()+MailContent.sharingNotification1+"\nYour login details for Music Notes," +
								"<br> User Name : " + user.getEmailId().toLowerCase() + "<br> Verification Code : " + user.getMailCheckId() + "<br> \n Enter the above verification code during login"+MailContent.sharingNotification2;
						
						sendMail.postEmail(recipients, "Musicnote Notification",message);
					}catch(Exception e){
						logger.error("Exception while updateUser method: ", e);
					}
			
			    }
			
			return mnUsers.getUserRole()+"~"+user.getEmailId();
		}
		catch (Exception e)
		{
			logger.error("Exception while updateUser detail :" + e);
		}
		
		return "Exception while updating  user detail";
	}
	@Override
	public List<MnUsers> getAllActiveUserDetailsForSearch(MnUsers user) {
		if(logger.isDebugEnabled())
			logger.debug("getAllActiveUserDetailsForSearch method called:  userId:  "+user.getUserId());
		List<MnUsers> mnUserList = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("status").is("A").and("userId").nin(user.getUserId()));
			mnUserList = mongoOperations.find(query,MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception while getAllActiveUserDetailsForSearch :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getAllActiveUserDetailsForSearch method called:  userId:  "+user.getUserId());
		return mnUserList;
	}

	@Override
	public MnUsers getExitsEmail(String emailId) {
		if(logger.isDebugEnabled())
			logger.debug("getExitsEmail method called: emailId:  "+emailId);
	MnUsers mnUser = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("status").is("A").orOperator(
					Criteria.where("userName").is(emailId.toLowerCase()),
					Criteria.where("emailId").is(emailId.toLowerCase())));
			//query = new Query(Criteria.where("emailId").is(emailId));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);

		}
		catch (Exception e)
		{
			logger.error("Exception while getExitsEmail details :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getExitsEmail method called");
		return mnUser;
	}
	
	@Override 
	public String resetPassword(Integer userId, String newPassWord,String oldPassWord){
		if(logger.isDebugEnabled())
			logger.debug("resetPassword method called:  userId:  "+userId);
		MnUsers user = null;
		String status = "";
		MnSecurityQuestions mnSecurityQuestions=null;
		String questionUserId="";
		String message="";
		try{
			Query query = new Query(Criteria.where("userId").is(userId));			
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			//block resetpassword in not mailId and Security Question
			Query queryUserId = new Query(Criteria.where("userId").is(userId));
			mnSecurityQuestions=mongoOperations.findOne(queryUserId,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);
			if(mnSecurityQuestions!=null)
			{
				questionUserId=mnSecurityQuestions.getUserId();
			}
			if(user.getEmailId()!=null || questionUserId!=null)
			{
				byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), oldPassWord);
				if (Arrays.equals(encryptedPassword, user.getPassword()))
				{
					updatePassword(userId,PasswordGenerator.encoder(user.getUserId().toString(),newPassWord));
					status = "success";
					
					//send mail on Reset Password.
					SendMail sendMail=new SendMail();
					if(user.getEmailId()!=null)
					{
						String recipients[]={user.getEmailId().toLowerCase()};
						if(user.getUserFirstName()!=null && !user.getUserFirstName().isEmpty()){
							message=MailContent.sharingNotification+user.getUserFirstName()+" "+user.getUserLastName()+MailContent.sharingNotification1+"\nYour Musicnote password was changed successfully. If you believe you received this message in error, please contact the Musicnote team." +
									MailContent.sharingNotification2;
						}else if(user.getUserName()!=null && !user.getUserName().isEmpty()){
							message=MailContent.sharingNotification+user.getUserName()+MailContent.sharingNotification1+"\nYour Musicnote password was changed successfully. If you believe you received this message in error, please contact the Musicnote team." +
									MailContent.sharingNotification2;
						}
						String subject="Password change successful";
						sendMail.postEmail(recipients, subject,message);
					}
				}else{
					status = "wrong";
				}
			}
			else
			{
				status="notSuccess";
			}
		}catch (Exception e) {
			logger.error("Exception while resetPassword in impl :" + e);
			status="wrong";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for resetPassword method called:  userId:  "+userId+"  status  :"+status);
		return status;
	}
	

	public void updatePassword(Integer userId ,byte[] passWord) {
		if(logger.isDebugEnabled())
		logger.debug("updatePassword method called: userId: "+userId);
		try{
			
			Update update=new Update();
			update.set("password", passWord);		
			Query query = new Query(Criteria.where("userId").is(userId));
			mongoOperations.updateMulti(query, update,JavaMessages.Mongo.MNUSERS);
			
		}catch (Exception e) {
			logger.error("Exception while updatePassword  : " + e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updatePassword method called:  ");
	}
	public String forgetPassword(String emailId){
		if(logger.isDebugEnabled())
			logger.debug("forgetPassword method called:  emailId: "+emailId);
		MnUsers user = null;
		String status ="";
		try{
			
			//Query query = new Query(Criteria.where("emailId").is(emailId.toLowerCase()));	
			Query query = new Query(Criteria.where("status").is("A").orOperator(
					Criteria.where("userName").is(emailId.toLowerCase()),
					Criteria.where("emailId").is(emailId.toLowerCase())));	
			user = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(user!= null ){
				String randomPass = RandomPassGenerator.getRandomPassword();
				byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), randomPass);
				updatePassword(user.getUserId(),encryptedPassword);	
				status = "success";
				SendMail sendMail=new SendMail(); 	
				String recipients[]={user.getEmailId().toLowerCase()};
				try{
					String message=MailContent.sharingNotification+user.getUserFirstName()+" "+user.getUserLastName()+MailContent.sharingNotification1+"\nYou are receiving this email because you were unable to log in using your existing Musicnote user name and password. Simply use the temporary password below and you can reset your password after you log in to your account." +
							"<br><br> User Name : " + user.getEmailId() + "<br> Password : " + randomPass + MailContent.sharingNotification2;
					
					sendMail.postEmail(recipients, "Password recovery for Musicnote",message);
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				status = "invalid";
			}
		}catch (Exception e) {
			logger.error("Exception while forgetPassword : " + e);
			status = "invalid";
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for forgetPassword method called:   emailId: "+emailId+"  status:  "+status);
		return status;
	}

	@Override
	public List<MnUsers> getTeacherDetrails()
	{
		if(logger.isDebugEnabled())
		logger.debug("getTeacherDetrails method called:  ");
		List<MnUsers> mnUserList = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userRole").is("Teacher"));
			mnUserList = mongoOperations.find(query,MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception while getTeacherDetrails :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Resturn response for getTeacherDetrails method called:  ");
		return mnUserList;
	}

	@Override
	public MnUsers updateEmail(String emailId,String userId) {
		if(logger.isDebugEnabled())
			logger.debug("updateEmail method called "+"userId: "+userId+" emailId"+emailId);
	MnUsers mnUser = null;
		Query query = null;
		try
		{
			if(emailId!=null && !emailId.equals(""))
			{
			query = new Query(Criteria.where("emailId").is(emailId));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while updateEmail :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for updateEmail method called:  "+"userId: "+userId+" emailId"+emailId);
		return mnUser;
	}

	@Override
	public MnUsers updateProfilePath(String userId, String filePath,boolean cropFlag) {
		if(logger.isDebugEnabled())
		logger.debug("updateProfilePath method called:  userId: "+userId);
		MnUsers mnUser = null;
		Query query1 = null;
        try{
			
			Update update=new Update();
			update.set("filePath", filePath);
			update.set("cropFlag",cropFlag);
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNUSERS);
			query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mnUser = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
		}catch (Exception e) {
			logger.error("Exception while updateProfilePath  : " + e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateProfilePath method called:  ");
		return mnUser;
	
	}

	@Override
	public MnUsers ExitsUser(String userName) {
		if(logger.isDebugEnabled())
		logger.debug("ExitsUser method called:  userName: "+userName);
	    MnUsers mnUser = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userName").is(userName.toLowerCase()));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception while ExitsUser :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for ExitsUser method called");
		return mnUser;
	}
	@Override
	public MnUsers ExitsUsers(String userName) {
		if(logger.isDebugEnabled())
			logger.debug("ExitsUser method called:  userName:  "+userName);
	    MnUsers mnUser = null;
		Query query = null;
		try
		{
			//query = new Query(Criteria.where("userName").is(userName.toLowerCase()).orOperator(Criteria.where("emailId").is(emailId.toLowerCase())));
			
			query = new Query(Criteria.where("status").is("A").orOperator(
					Criteria.where("userName").is(userName.toLowerCase()),
					Criteria.where("emailId").is(userName.toLowerCase())));
			
			
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(mnUser!=null)
			{
				String status=validateBlockedList(mnUser.getUserId());
				if(status.equals("blocked"))
				{
					mnUser.setUserName("blocked");
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while ExitsUser :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for ExitsUser method called");
		return mnUser;
	}

	@Override
	public MnUsers updateUserName(String userName, String userId) {
		if(logger.isDebugEnabled())
			logger.debug("updateUserName method called: userId: "+userId+" userName: "+userName);
	MnUsers mnUser = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userName").is(userName));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);

		}
		catch (Exception e)
		{
			logger.error("Exception while updateUserName :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return reponse for updateUserName method called:  userId: "+userId+" userName: "+userName);
		return mnUser;
	}

	@Override
	public Map<String, String> fetchUserName(String recipients,String listType) 
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchUserName method called:  "+recipients);
		Map<String,String> map=new HashMap<String, String>();
		String []mailIds=recipients.split(",");
		for (int i = 0; i < mailIds.length; i++)
		{
			boolean mailNotify=false;
			
			Query query = new Query(Criteria.where("emailId").is(mailIds[i]).and("notificationFlag").is("yes"));
			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			mailNotify=getUserMailNotification(mnUsers.getUserId(),listType);
			if(mailNotify)
			{
			map.put(mailIds[i], mnUsers.getUserFirstName()+"~"+mnUsers.getUserId().toString());
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserName method called:  ");
		return map;
	}
	
	
	public String getTrailEndMailValues(){
		if(logger.isDebugEnabled())
		logger.debug("getTrailEndMailValues method called:  ");
		List<MnUsers> user=new ArrayList<MnUsers>();
		String status ="";
		DateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

		try{
			Query query = new Query(Criteria.where("status").is("A"));	
			user = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(user!= null ){
				for(MnUsers user1:user){
				String endDate=user1.getEndDate();
				String[] traildate = endDate.split("/");
				String endmonth = traildate[0];
				String endday = traildate[1];
				String endyear  = traildate[2];
				Date currentdate=new Date();
				String currentDate=formatter1.format(currentdate);
				String[] currDate = currentDate.split("/");
				String currentMonth = currDate[0];
				String currentDay = currDate[1];
				String currentYear  = currDate[2];
				int currentday1=Integer.parseInt(currentDay);
				int currentmonth1=Integer.parseInt(currentMonth);
				int currentyear1=Integer.parseInt(currentYear);
				int endday1=Integer.parseInt(endday);
				int endmonth1=Integer.parseInt(endmonth);
				int endyear1=Integer.parseInt(endyear);
				calendar1.set(endyear1,endmonth1,endday1);
		        calendar2.set(currentyear1, currentmonth1, currentday1);
		        Date enddate = calendar1.getTime();
		        Date current = calendar2.getTime();
		        long diff=enddate.getTime()-current.getTime(); 
		        int noofdays=(int)(diff/(1000*24*60*60)); 
				if((noofdays == 0)||(noofdays == 1)||(noofdays == 2)){
				
					status = "success";
					SendMail sendMail=new SendMail(); 	
					String recipients[]={user1.getEmailId().toLowerCase()};
					try{
						String message="trail period remaing day(s):"+noofdays;
						sendMail.postEmail(recipients, "Trail period",message);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		}catch(Exception e){
			logger.error("Exception in getTrailEndMailValues method :"+ e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getTrailEndMailValues method called:  ");
		return null;
	}
	
	@Override
	public String addSubscription(MnUsers mnUsers) 
	{
		String status="";
	
		return status;
		
	}
	
	@Override
	public String getTrailcount(String userId) 
	{
		if(logger.isDebugEnabled())
			logger.debug("getTrailcount method called:  userId:  "+userId);
		MnUsers user;
		String trailcount = "";
		DateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		try {
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A").orOperator(
							Criteria.where("userLevel").is("Trial-Start"),
							Criteria.where("userLevel").is("Trial-End"),
							Criteria.where("userLevel").is("Trial-Middle")));
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
			if (user != null) 
			{
			String startDate = user.getStartDate();
			String[] traildate = startDate.split("/");
			String startmonth = traildate[0];
			String startday = traildate[1];
			String startyear = traildate[2];
			Date currentdate = new Date();
			String currentDate = formatter1.format(currentdate);
			String[] currDate = currentDate.split("/");
			String currentMonth = currDate[0];
			String currentDay = currDate[1];
			String currentYear = currDate[2];
			int currentday1 = Integer.parseInt(currentDay);
			int currentmonth1 = Integer.parseInt(currentMonth);
			int currentyear1 = Integer.parseInt(currentYear);
			int endday1 = Integer.parseInt(startday);
			int endmonth1 = Integer.parseInt(startmonth);
			int endyear1 = Integer.parseInt(startyear);
			calendar1.set(endyear1, endmonth1, endday1);
			calendar2.set(currentyear1, currentmonth1, currentday1);
			Date enddate = calendar1.getTime();
			Date current = calendar2.getTime();
			long diff =  current.getTime()-enddate.getTime();
			int noofdays = (int) (diff / (1000 * 24 * 60 * 60));
			if (noofdays >= 0 && noofdays <30) {
				noofdays=noofdays+1;
				trailcount = Integer.toString(noofdays);
			} else {
				trailcount = "Expired";
			}} else {
				logger.debug("counting trail days : false");
			}
		} catch (Exception e) {
			logger.error("Exception while getTrailcount :"+ e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getTrailcount method called: userId: "+userId);
		return trailcount;
	}
	
	
	@Override
	public String sendMailNotificationsForAutomatically() 
	{
		if(logger.isDebugEnabled())
		logger.debug("sendMailNotificationsForAutomatically method called");
		String status="";
		SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
		try
		{
			List <MnMailConfiguration> configuration=null;
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, 1);  // number of days to add
			String end = formatter.format(c.getTime());
			Query query=new Query(Criteria.where("endDate").lt(end).and("status").is("A"));
			configuration=mongoOperations.find(query, MnMailConfiguration.class, JavaMessages.Mongo.MNMailConfiguration);
			
			if(configuration!=null && !configuration.equals(""))
			{
				status=configurationMailForUsers(configuration); // call to user mail configuration settings
			}else
			{
				status=defaultMailTrialUsers(); // call to default mail settings
			}
			
		}catch (Exception e) {
			logger.error("Exception in sendMailNotificationsForAutomatically method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for sendMailNotificationsForAutomatically method called");
		return status;
		
	}
	
	
	private String configurationMailForUsers(List <MnMailConfiguration> configuration) 
	{
		String status="";
		if(logger.isDebugEnabled())
		logger.debug("configurationMailForUsers method called");
		try
		{
		for(MnMailConfiguration mnconfig:configuration){
			List<MnUsers> mnUser=findUsersForMail(mnconfig.getUserLevel());
			
			if(mnUser!=null && !mnUser.isEmpty())
			{
				sendingMail(mnUser, mnconfig.getMailMessage(),mnconfig.getMailSubject(), mnconfig.getIntervel());
			}
		}	
		}catch (Exception e) {
			logger.error("Exception in configurationMailForUsers method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for configurationMailForUsers method");
		return status;
		
	}

	private void sendingMail(List<MnUsers> mnUser, String message, String interval, String subject) 
	{
		if(logger.isDebugEnabled())
		logger.debug("sendingMail  method called "+subject);
		SendMail sendmail=new SendMail();
		int intervalDay=0;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try
		{
			if(interval.equalsIgnoreCase("Daily"))
			{
				for(MnUsers usr:mnUser)
				{
					String emailId[]={usr.getEmailId()};
					String fullMessage=MailContent.sharingNotification+usr.getUserFirstName()+MailContent.sharingNotification1+message+" "+MailContent.sharingNotification2;
					subject="Musicnote!!!";
					sendmail.postEmail(emailId, subject, fullMessage);
				}
				
			}
			else
			{
				if(interval.equalsIgnoreCase("Weekly"))
					intervalDay=7;
				else if(interval.equalsIgnoreCase("10-Days"))
					intervalDay=10;
				else
					intervalDay=30;
				
				for(MnUsers usr:mnUser)
				{
					Calendar cal = Calendar.getInstance();
					Date secondDate = cal.getTime();
					Date firstDate = formatter.parse(usr.getStartDate());
			        long diff = secondDate.getTime() - firstDate.getTime();
			        int days=(int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			        if(days % intervalDay==0)
			        {
			        	
			        String fullMessage=MailContent.sharingNotification+usr.getUserFirstName()+MailContent.sharingNotification1+message+" "+MailContent.sharingNotification2;
					subject="Musicnote!!!";
					String emailId[]={usr.getEmailId()};
					sendmail.postEmail(emailId, subject, fullMessage);
			        }
				}
			}
		}catch (Exception e) {
			logger.error("Exception in sendingMail method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
	logger.debug("Return response for sendingMail  method called");
	}
	
	public List<MnUsers> findUsersForMail(String userLevel)
	{
		if(logger.isDebugEnabled())
		logger.debug("findUsersForMail  method called userLevel: "+userLevel);
		List<MnUsers> mnUser=new ArrayList<MnUsers>();
		try
		{
			Query query;
			if(userLevel.equalsIgnoreCase("Trial"))
			{
				query = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
						Criteria.where("userLevel").is("Trial-Start"),
						Criteria.where("userLevel").is("Trial-End"),
						Criteria.where("userLevel").is("Trial-Middle")));
			}
			else if(userLevel.equalsIgnoreCase("Premium"))
			{
				query = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
						Criteria.where("userLevel").is("premium-gold"),
						Criteria.where("userLevel").is("premium-silver"),
						Criteria.where("userLevel").is("premium-platinum")));
				
			}else if(userLevel.equalsIgnoreCase("All")){
				query = new Query(Criteria.where("userLevel").exists(true).orOperator(
						Criteria.where("status").is("I"),
						Criteria.where("status").is("A")));
			}
			else if(userLevel.equalsIgnoreCase("Expired")){
				query = new Query(Criteria.where("userLevel").exists(true).and("status").is("I"));
			}
			{
				query = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").and("userLevel").is(userLevel));
			}
			mnUser = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
		}catch (Exception e) {
			logger.error("Exception in findUsersForMail method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for findUsersForMail  method called");
		return mnUser;
		
	}
	
	public String defaultMailTrialUsers() 
	{
		if(logger.isDebugEnabled())
		logger.debug("defaultMailTrialUsers  method called");
		try
		{
		List<MnUsers> mnUser,mnUser1=new ArrayList<MnUsers>();
		SendMail sendmail=new SendMail();
		String subject="Trail Message-Expired";
		String message="Trail e-mail-Expired";
		
		Query query3 = new Query(Criteria.where("userLevel").is("Expired"));
		mnUser1 = mongoOperations.find(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		for(MnUsers usr:mnUser1){
			String emailId[]={usr.getEmailId()};
			sendmail.postEmail(emailId, subject, message);
		}
		
		Query query2 = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
				Criteria.where("userLevel").is("Trial-Start"),
				Criteria.where("userLevel").is("Trial-End"),
				Criteria.where("userLevel").is("Trial-Middle")));
	
		mnUser = mongoOperations.find(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		
		for(MnUsers mnStr:mnUser){
			String startDate=mnStr.getStartDate();
			String endDate=mnStr.getEndDate();
			String email[]={mnStr.getEmailId()};
			String userId=mnStr.getUserId().toString();
			String firstName=mnStr.getUserFirstName();
			String lastName=mnStr.getUserLastName();
			checkUserForMailSend(startDate,endDate,email,userId,firstName,lastName);
		}
		}catch(Exception e){
			logger.error("Exception while defaultMailTrialUsers", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for defaultMailTrialUsers  method called");
		return "success";
	
	}
	
	public String checkUserForMailSend(String startDate,String endDate,String emailId[],String userId,String firstName,String lastName){
		if(logger.isDebugEnabled())
		logger.debug("checkUserForMailSend  method called userId: "+userId);
		try
		{
		SendMail sendmail=new SendMail();
		String subject="Trail Message";
		String message=MailContent.sharingNotification+firstName+" "+lastName+MailContent.sharingNotification1+"\nYour Musicnote is due to expire soon , Get premium account now" +
		MailContent.signature;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date dateApi=new Date();
		String currentDate=formatter.format(dateApi);
		
		if(!(startDate.compareToIgnoreCase(currentDate)>0)){
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(startDate));
		c.add(Calendar.DATE, 10);  // number of days to add
		String startDate1 = sdf.format(c.getTime()); 
		if(startDate1.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c1 = Calendar.getInstance();
		c1.setTime(sdf.parse(startDate));
		c1.add(Calendar.DATE, 20);  // number of days to add
		String startDate2 = sdf.format(c1.getTime()); 
		if(startDate2.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c2 = Calendar.getInstance();
		c2.setTime(sdf.parse(startDate));
		c2.add(Calendar.DATE, 25);  // number of days to add
		String startDate3 = sdf.format(c2.getTime()); 
		if(startDate3.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c3 = Calendar.getInstance();
		c3.setTime(sdf.parse(startDate));
		c3.add(Calendar.DATE, 26);  // number of days to add
		String startDate4 = sdf.format(c3.getTime()); 
		if(startDate4.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c4 = Calendar.getInstance();
		c4.setTime(sdf.parse(startDate));
		c4.add(Calendar.DATE, 27);  // number of days to add
		String startDate5 = sdf.format(c4.getTime()); 
		if(startDate5.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c5 = Calendar.getInstance();
		c5.setTime(sdf.parse(startDate));
		c5.add(Calendar.DATE, 28);  // number of days to add
		String startDate6 = sdf.format(c5.getTime()); 
		if(startDate6.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c6 = Calendar.getInstance();
		c6.setTime(sdf.parse(startDate));
		c6.add(Calendar.DATE, 29);  // number of days to add
		String startDate7 = sdf.format(c6.getTime()); 
		if(startDate7.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		
		Calendar c7 = Calendar.getInstance();
		c7.setTime(sdf.parse(startDate));
		c7.add(Calendar.DATE, 30);  // number of days to add
		String startDate8 = sdf.format(c7.getTime()); 
		if(startDate8.equals(currentDate)){
			sendmail.postEmail(emailId, subject, message);
		}
		}
		}catch(Exception e){
			logger.error("Exception occured from checkUserForMailSend method :"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for checkUserForMailSend  method called");
		return "";
		
	}
	
	// this method is used to update userLevel automatically...
	@Override
	public String setUserLevelChanges() 
	{
		String status="";
		if(logger.isDebugEnabled())
		logger.debug("setUserLevelChanges  method called");
		try
		{
			// for trial user
		List<MnUsers> mnUser=null;
		Query query2 = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
				Criteria.where("userLevel").is("Trial-Start"),
				Criteria.where("userLevel").is("Trial-End"),
				Criteria.where("userLevel").is("Trial-Middle")));
		
		mnUser = mongoOperations.find(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if(mnUser!=null && !mnUser.isEmpty())
		{
			for(MnUsers mnStr:mnUser)
			{
				status=updateUserLevel(mnStr, "Trial-Middle", 10);
				if(status.equalsIgnoreCase("false"))
				{
					status=updateUserLevel(mnStr, "Trial-End", 25);
					if(status.equalsIgnoreCase("false"))
					{
						status=updateUserLevel(mnStr, "Expired", 31);
					}
				}
			}
		}
		
		// for premium users
		List<MnUsers> mnUser1=null;
		Query query3 = new Query(Criteria.where("userLevel").exists(true).and("status").is("A").orOperator(
				Criteria.where("userLevel").is("premium-gold"),
				Criteria.where("userLevel").is("premium-silver"),
				Criteria.where("userLevel").is("premium-platinum")));
		mnUser1 = mongoOperations.find(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if(mnUser1!=null && !mnUser1.isEmpty())
		{
			for(MnUsers mnStr:mnUser1)
			{
				status=updateUserLevel(mnStr, "Expired", 0);
			}
		}
		
	}catch (Exception e) {
		logger.error("Exception in setUserLevelChanges method :"+e.getMessage());
	}
	if(logger.isDebugEnabled())
	logger.debug("Return response for setUserLevelChanges method called:  ");
		return status;
	
	}
	
	public String updateUserLevel(MnUsers users,String level,int day)
	{
		if(logger.isDebugEnabled())
		logger.debug("updateUserLevel method called:  "+level);
		String status="false";
		String startDate1=null;
		try
		{
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date today=new Date();
			String todayDate=formatter.format(today);
			String startDate=users.getStartDate();
			String endDate=users.getEndDate();
			Calendar c = Calendar.getInstance();
			if(day>0)
			{
				c.setTime(formatter.parse(startDate));
				c.add(Calendar.DATE, day);  // number of days to add
				startDate1 = formatter.format(c.getTime()); 
			}
			else
			{
				c.setTime(formatter.parse(endDate));
				c.add(Calendar.DATE, 1);  // number of days to add
				startDate1 = formatter.format(c.getTime()); 
			}
			if(startDate1.equals(todayDate))
			{
					Query query1=new Query(Criteria.where("userId").is(users.getUserId()));
					Update update=new Update();
					update.set("userLevel",level);
					mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
					status="true";
			}
			
		}catch (Exception e) {
			logger.error("Exception updateUserLevel :  "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateUserLevel method called:  ");
		return status;
	}
	
	// method used for update status of mail configuration automatically
	@Override
	public void inActiveMailconfiguration() {
		if(logger.isDebugEnabled())
		logger.debug("inActiveMailconfiguration method called:  ");
		List<MnMailConfiguration> list=null;
		try
		{
			Query query=new Query(Criteria.where("status").is("A"));
			list=mongoOperations.find(query, MnMailConfiguration.class, JavaMessages.Mongo.MNMailConfiguration);
			
			if(list!=null && !list.isEmpty())
			{
				for(MnMailConfiguration mails:list)
				{
					DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					Date today=new Date();
					String todayDate=formatter.format(today);
					String endDate=mails.getEndDate();
					
					if(endDate.equals(todayDate))
					{
							Query query1=new Query(Criteria.where("mailNo").is(mails.getMailNo()));
							Update update=new Update();
							update.set("status","I");
							mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNMailConfiguration);
					}
					
				}
			
			}
			
		}catch (Exception e) {
			logger.error("Exception inActiveMailconfiguration :  "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for inActiveMailconfiguration method called:  ");
	}
	
	
	
	
	
	
	@Override
	public List<MnUsers> fetchAddusersDetails(String userId) 
	{
		if(logger.isDebugEnabled())
		logger.debug("while sending mail for fetchAddusersDetails method called:  userId: "+userId);
		String status="success";
		List<MnUsers> users=null;
		try
		{
			Query query2 = new Query(Criteria.where("addedByUserId").is(Integer.parseInt(userId)));
			users = mongoOperations.find(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
		}catch(Exception e){
			logger.error("Exception while fetchAddusersDetails", e);
			}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchAddusersDetails method dao impl: userId:  "+userId);
		return users;
		}
	@Override
	public String sendUserCreationMail(String userId) 
	{
		if(logger.isDebugEnabled())
		logger.debug("while sending mail for sendingUserCreationMail method called: userId:  "+userId);
		String status="success";
		try
		{
			Query query2 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnUsers user = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
			if(user!=null)
			{
				SendMail sendMail=new SendMail(); 	
				SendMail sendMail1=new SendMail();
				String recipients[]={user.getEmailId()};
				String userLevel=user.getUserLevel();
				try{
					String message=MailContent.sharingNotification+user.getUserFirstName()+" "+user.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
							"<br> User Name : " + user.getEmailId() +
							"<br> Verification Code : " + user.getMailCheckId() +
							"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
							"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
							MailContent.signature;
					
					String messageForTrailUser=MailContent.sharingNotification+user.getUserFirstName()+" "+user.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
					"<br> User Name : " + user.getEmailId() +
					"<br> Verification Code : " + user.getMailCheckId() +
					"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
					"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
					MailContent.signature;
					//Sending mail to inform users that they are on trail period
					if(userLevel.equals("Trial-Start")){
						sendMail1.postEmail(recipients, user.getUserFirstName()+" welcome to Musicnote! ",messageForTrailUser);	
					}else{
						sendMail.postEmail(recipients, user.getUserFirstName()+" welcome to Musicnote! ",message);
					}
					if(logger.isDebugEnabled())
					logger.debug("Return response for sendingUserCreationMail method : ");
				}catch(Exception e){
					status="error";
					logger.error("Exception occured in"+e);
				}
			
			}
			
		}catch (Exception e) {
			logger.error("Exception while sendUserCreationMail", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for sendUserCreationMail method dao impl: userId:  "+userId);
		return status;
	}
	@Override
	public String addSecurityQuestion(MnSecurityQuestions security){
		if(logger.isDebugEnabled())
			logger.debug("addSecurityQuestion method dao Impl: userId:  "+security.getUserId());
		try{
			Query query=new Query(Criteria.where("userId").is(security.getUserId()));
			MnSecurityQuestions security1=mongoOperations.findOne(query,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);
			if(security1==null){
			mongoOperations.insert(security,JavaMessages.Mongo.MNSecurityQuestion);
			}
		}catch(Exception e){
		logger.error("Exception while addSecurityQuestion", e);	
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for addSecurityQuestion method dao Impl userId : "+security.getUserId());
		return "success";
	}

	@Override
	public MnSecurityQuestions getSecurityQuestions(String userName) {
		if(logger.isDebugEnabled())
			logger.debug("getSecurityQuestions method dao impl:  userName:  "+userName);
		MnSecurityQuestions question=null;
		MnUsers mnuser=null;
		try{
			Query username=new Query(Criteria.where("emailId").is(userName.toLowerCase()));
			mnuser = mongoOperations.findOne(username, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if(mnuser==null){
				Query username1=new Query(Criteria.where("userName").is(userName.toLowerCase()));
				mnuser = mongoOperations.findOne(username1, MnUsers.class,JavaMessages.Mongo.MNUSERS);	
			}
			if(mnuser!=null){
				
				Query query1=new Query(Criteria.where("userId").is(mnuser.getUserId().toString()));
				question=mongoOperations.findOne(query1,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);
				
				if(question!=null)
				{
					query1=new Query(Criteria.where("questionId").is(question.getQuestionId()));
					MnAdminSecurityQuestion admin=mongoOperations.findOne(query1,MnAdminSecurityQuestion.class,JavaMessages.Mongo.MNADMINSECURITYQUESTION);
					if(admin!=null && !admin.equals(""))
					{
					question.setQuestion(admin.getQuestion()+"-"+admin.getQuestionId());
					}
					return question;
				}
				}
			
			
		}catch(Exception e){
			logger.error("Exception while getSecurityQuestions", e);	
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getSecurityQuestions method dao Impl userName:  "+userName);
		return question;
		
	}

	@Override
	public String getAdminSecurityQuestion()
	{
		if(logger.isDebugEnabled())
			logger.debug("getAdminSecurityQuestion method dao Impl:");
		StringBuffer status=new StringBuffer();
		Query query=null;
		try
		{
			query = new Query(Criteria.where("status").is("A"));
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnAdminSecurityQuestion> admin=mongoOperations.find(query,MnAdminSecurityQuestion.class,JavaMessages.Mongo.MNADMINSECURITYQUESTION);
			if(admin!=null && !admin.isEmpty())
			{
				for(MnAdminSecurityQuestion question:admin)
				{
					status=status.append(question.getQuestion()+"-"+question.getQuestionId()+"~");
				}
				status = status.deleteCharAt(status.lastIndexOf("~"));
			}
		}catch(Exception e)
		{
			logger.error("Exception while getAdminSecurityQuestion", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getAdminSecurityQuestion method dao Impl");
		return status.toString();
	}
	
	
	@Override
	public String setResetPassword(String userName, String password) {
		if(logger.isDebugEnabled())
			logger.debug("setResetPassword method dao Impl: userName:  "+userName);
		MnUsers mnuser=null;
		String status="";
		String message="";
		try{
			Query username=new Query(Criteria.where("status").is("A").orOperator(
					Criteria.where("userName").is(userName),
					Criteria.where("emailId").is(userName)));
			mnuser = mongoOperations.findOne(username, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if(mnuser!=null){
				Update update=new Update();
				update.set("password",PasswordGenerator.encoder(mnuser.getUserId().toString(),password));
				mongoOperations.updateFirst(username, update, JavaMessages.Mongo.MNUSERS);
				status="success";
				if(mnuser.getEmailId()!=null && !mnuser.getEmailId().isEmpty()){
					SendMail sendMail=new SendMail(); 	
					String recipients[]={mnuser.getEmailId().toLowerCase()};
					if(mnuser.getUserFirstName()!=null && !mnuser.getUserFirstName().isEmpty()){
						message=MailContent.sharingNotification+mnuser.getUserFirstName()+" "+mnuser.getUserLastName()+MailContent.sharingNotification1+"\nYour Musicnote password was changed successfully. If you believe you received this message in error, please contact the Musicnote team." +
								MailContent.sharingNotification2;
					}else if(mnuser.getUserName()!=null && !mnuser.getUserName().isEmpty()){
						message=MailContent.sharingNotification+mnuser.getUserName()+MailContent.sharingNotification1+"\nYour Musicnote password was changed successfully. If you believe you received this message in error, please contact the Musicnote team." +
								MailContent.sharingNotification2;
					}
					String subject="Password change successful";
					sendMail.postEmail(recipients, subject,message);
				
				}
				
				/*Query username1=new Query(Criteria.where("userName").is(userName));
				mnuser1 = mongoOperations.findOne(username1, MnUsers.class,JavaMessages.Mongo.MNUSERS);*/
				
					
			
			/*Update update=new Update();
			update.set("password",PasswordGenerator.encoder(mnuser.getUserId().toString(),password));
			mongoOperations.updateFirst(username, update, JavaMessages.Mongo.MNUSERS);
			status="success";*/
			}
			else
			{
				status="UserName not found";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		logger.error("Exception while setResetPassword", e);	
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for setResetPassword method dao Impl userName:  "+userName);
		return status;
	}
	@Override
	public String addFriendRequestForAddingStudents(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("addFriendRequest method called");
		mongoOperations.insert(mnFriends, JavaMessages.Mongo.MNFRIEND);
		Boolean mailNotify=false;
		MnUsers mnUser = getUserDetailsObject(mnFriends.getUserId());
		MnUsers users = getUserDetailsObject(mnFriends.getRequestedUserId());
		if(mnUser.getUserFirstName()!=null && !mnUser.getUserFirstName().isEmpty() ){
			writeLog(mnFriends.getRequestedUserId(), "A", "Friend request sent to "+mnUser.getUserFirstName()+" "+mnUser.getUserLastName(), "contact", "","", null, "contact");
		}else{
			writeLog(mnFriends.getRequestedUserId(), "A", "Friend request sent to "+mnUser.getUserName(), "contact", "","", null, "contact");
		}
		
		try{
			mailNotify=getUserMailNotification(mnUser.getUserId(),"contact");
			if(mnUser.getNotificationFlag().equalsIgnoreCase("yes"))
			{
				SendMail sendMail=new SendMail(); 	
				String recipients[]={mnUser.getEmailId()};
				String message=MailContent.sharingNotification+mnUser.getUserFirstName()+MailContent.sharingNotification1+users.getUserFirstName()+" "+users.getUserLastName()+" has sent a request to add you as a contact in Musicnote. To accept their request and add them as one of your contacts please log in to your profile.."+MailContent.sharingNotification2;;
				String subject=users.getUserFirstName()+" wants to add you as a contact in Musicnote";
				sendMail.postEmail(recipients, subject,message);
			}
		
		}catch(Exception e){
			logger.error("Exception in addFriendRequest method:"+e);
		}
					
				if(logger.isDebugEnabled())
				logger.debug("addFriendRequest method successfully returned");
		return "success";
	}

	@Override
	public String setResetPass() 
	{
		if(logger.isDebugEnabled())
			logger.debug("setResetPass method called");
		List<MnUsers> mnUserList = null;
		
		// change columname manually 
		
		/*Query q=new Query();
		Update up=new Update();
		up.rename("Id", "subEventId");
		mongoOperations.updateMulti(q, up, MnSubEventDetails.class, JavaMessages.Mongo.MNESUBVENTS);*/
		
		/*Query q=new Query();
		Update up=new Update();
		up.rename("QuestionId", "questionId");
		mongoOperations.updateMulti(q,up,MnSecurityQuestions.class,JavaMessages.Mongo.MNSecurityQuestion);*/
		// changing username or password manually
		Query query = null;
		try
		{
			/*logger.debug("called successfully ::: ");
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			 //query = new Query(Criteria.where("userId").is(1000));
			mnUserList = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
			for(MnUsers user:mnUserList)
			{
				byte[] encryptedPassword=PasswordGenerator.encoder(user.getUserId().toString(),"pass");
				Update update=new Update();
				Double size=new Double(5120);
				update.set("limitedSize",size);
				update.set("uploadedSize",0);
				Query query1 = new Query(Criteria.where("userId").is(user.getUserId()));
				mongoOperations.updateFirst(query1,update, JavaMessages.Mongo.MNUSERS);
				System.out.println("updated successfully ::: "+user.getUserId());
			}
*/
		}
		catch (Exception e)
		{
			logger.error("Exception while setResetPass :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("setResetPass method successfully returned");
		return "success";
	
	}

	@Override
	public String newToken(HttpServletRequest  request) {
		if(logger.isDebugEnabled())
			logger.debug("newToken method called dao impl");
		String status="";
		
		Query query = null;
		try
		{
			logger.debug("called successfully newToken method::: ");
			JSONObject arbitraryPayload = new JSONObject();
			try {
			    arbitraryPayload.put("some", "arbitrary");
			    arbitraryPayload.put("data", "here");
			} catch (JSONException e) {
			    e.printStackTrace();
			}   
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date today = Calendar.getInstance().getTime();        
			String date = df.format(today);
			TokenGenerator tokenGenerator=new  TokenGenerator(request.getRemoteHost()+""+request.getRemoteAddr()+""+date);
			String token = tokenGenerator.createToken(arbitraryPayload);
			
			MnUsersToken usersToken=new MnUsersToken();
			usersToken.setUserId(null);
			usersToken.setLoginTime(date);
			usersToken.setTokens(token);
			usersToken.setCreatedDateTime(date);
			usersToken.setIpaddress(request.getRemoteHost()+request.getRemoteAddr());
			mongoOperations.insert(usersToken, JavaMessages.Mongo.MNUsersToken);
			
			status="{\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
		}
		catch (Exception e)
		{
			logger.error("Exception while newToken :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for newToken method dao impl status: "+status);
		return status;
	
	}
	@Override
	public String MnMailNotifications(MnMailNotificationList mailList) {

		
		if(logger.isDebugEnabled())
			logger.debug("while data set for MnMailNotifications method: userId:  "+mailList.getUserId());
			try{
				
				if(mailList!=null){
					mongoOperations.insert(mailList,JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
					return "success";
				}
			}catch(Exception e){
				logger.error("Exception while inserting MnMailNotifications details :" + e);
			}
			if(logger.isDebugEnabled())
				logger.debug("Return response for MnMailNotifications method : userId:  "+mailList.getUserId());
			return "";
		}

	@Override
	public Boolean getUserMailNotification(Integer userId, String type) {
		if(logger.isDebugEnabled())
			logger.debug("getUserMailNotification method: userId:  "+userId);
		MnMailNotificationList  mailNotify=null;
		boolean  mailNotification = false;
		String status="";
		try{
				Query query2=new Query(Criteria.where("userId").is(userId));
				mailNotify = mongoOperations.findOne(query2, MnMailNotificationList.class,JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
				if(mailNotify!= null && !mailNotify.equals("")){
				if(type.matches("music")){	
					mailNotification=mailNotify.isNoteBasisMail();
				}
				else if(type.matches("schedule")){
					mailNotification=mailNotify.isNoteBasisMail();
				}
				else if(type.matches("bill")){
					mailNotification=mailNotify.isNoteBasisMail();
				}
				else if(type.matches("contact")){
					mailNotification=mailNotify.isNoteBasisMail();
				}
				else if(type.matches("crowd")){
					mailNotification=mailNotify.isNoteBasisMail();
				}
				}
			
		}catch(Exception e){
			logger.error("Exception while getUserMailNotification :" + e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getUserMailNotification method : ");
		return mailNotification;
		
		
		
		
		
	}

	@Override
	public MnMailNotificationList fetchMailNotification(String userId) {
		if(logger.isDebugEnabled())
			logger.debug("fetchMailNotification method : userId:  "+userId);
		MnMailNotificationList mailNotificationList = null;
		 MnMailNotificationList mail=new MnMailNotificationList();
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mailNotificationList = mongoOperations.findOne(query, MnMailNotificationList.class, JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
           if(mailNotificationList==null)
           {
        	   mail.setContactBasisMail(true);
        	   mail.setCrowdBasisMail(true);
        	   mail.setDueDateMail(true);
        	   mail.setEventBasisMail(true);
        	   mail.setMemoBasisMail(true);
        	   mail.setNoteBasisMail(true);
        	   mail.setUserId(Integer.parseInt(userId));
        	   mailNotificationList=mail;
        	   mongoOperations.insert(mail,JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
        	  
        	   logger.debug("NoteMail====="+mailNotificationList.isNoteBasisMail());
           }
           
		}
		catch (Exception e)
		{
			logger.error("Exception while fetchMailNotification method :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchMailNotification method : userId:  "+userId);
		return mailNotificationList;
	}

	@Override
	public String updateMnMailNotifications(MnMailNotificationList mailNotify) {

		
		if(logger.isDebugEnabled())
			logger.debug("while data set for updateMnMailNotifications method: userId:  "+mailNotify.getUserId());
			try{
				Update update=new Update();
				Query query=new Query(Criteria.where("userId").is((mailNotify.getUserId())));
				MnMailNotificationList notificationList = mongoOperations.findOne(query, MnMailNotificationList.class, JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
				if(notificationList!=null){
					update.set("userId", mailNotify.getUserId());
					update.set("noteBasisMail", mailNotify.isNoteBasisMail());
					update.set("eventBasisMail", mailNotify.isEventBasisMail());
					update.set("memoBasisMail", mailNotify.isMemoBasisMail());
					update.set("contactBasisMail", mailNotify.isContactBasisMail());
					update.set("crowdBasisMail", mailNotify.isCrowdBasisMail());
					update.set("dueDateMail", mailNotify.isDueDateMail());
					mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
				}
			}catch(Exception e){
				logger.error("Exception while updateMnMailNotifications details :" + e);
			}
			if(logger.isDebugEnabled())
				logger.debug("Return response for updateMnMailNotifications method :  userId:  "+mailNotify.getUserId());
			return "success";
		}

	@Override
	public MnUsers limitedUpload(String userId) {
		if(logger.isDebugEnabled())
			logger.debug("limitedUpload method called userId: "+userId);
		MnUsers mnUsers=null;
		try
	   {
			
		
			 Query limitedQuery=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			 mnUsers=mongoOperations.findOne(limitedQuery,MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
	catch (Exception e)
	{
		logger.error("Exception limitedUpload :", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for limitedUpload: userId: "+userId);
		return mnUsers;
	
	}

	@Override
	public String sendMailForAddingUser(Map<String, String> userMapForMail,int addedByUserId) {
		if(logger.isDebugEnabled())
			logger.debug("sendMailForAddingUser for dao Impl: addedByUserId: "+addedByUserId);
		MnUsers mnUsers=null;
		List<String>userListForMail=new ArrayList<String>();
		try{
			if(userMapForMail!=null)
			{
				Query limitedQuery=new Query(Criteria.where("userId").is(addedByUserId));
				 mnUsers=mongoOperations.findOne(limitedQuery,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				for ( String key : userMapForMail.keySet() ) {
					String Username=key;
					String password=userMapForMail.get(key);
				    String userDetails="<tr><td style='border:1px solid black;border-collapse:collapse;'>"+Username+"</td><td style='border:1px solid black;border-collapse:collapse;'>"+password+"</td></tr>";
				    userListForMail.add(userDetails);
				}
				String userListDetails=userListForMail.toString();
				userListDetails=userListDetails.replace(",", "");
				userListDetails=userListDetails.replace("[", "");
				userListDetails=userListDetails.replace("]", "");
				SendMail sendMail=new SendMail(); 	
				String recipients[]={mnUsers.getEmailId()};
				String message=MailContent.sharingNotification+mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName()+MailContent.sharingNotification1+"\nBelow is a list of the users you added to Musicnote. It includes their Username and temporary Password for your reference.</br>" +
						"<br><table style='width:300px;border:1px solid black;border-collapse:collapse;'><tr><td  style='border:1px solid black;border-collapse:collapse;'><b><center>Username</center></b></td><td style='border:1px solid black;border-collapse:collapse;'><b><center>Password</center></b></td></tr>"		
						+userListDetails+"</table>"+MailContent.sharingNotification2;;
				String subject="Added users details in Musicnote";
				sendMail.postEmail(recipients, subject,message);
			}
		
		}catch(Exception e){
			logger.error("Exception in sendMailForAddingUser method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for sendMailForAddingUser for dao Impl: addedByUserId: "+addedByUserId);
		return null;
	}

	@Override
	public String deleteProfilePicture(String userId) 
	{
		if(logger.isDebugEnabled())
			logger.debug("deleteProfilePicture method called: userId :"+userId);
		String status="";
		MnUsers mnUsers=null;
		Query query = null;
		try
		{
			query=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mnUsers=mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(mnUsers!=null)
			{
				status=mnUsers.getFilePath();
				Update update=new Update();
				update.set("filePath", "");
				Query query2 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
				mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNUSERS);
				
			}
		}
		catch(Exception e)
		{
			logger.error("Exception in deleteProfilePicture method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for deleteProfilePicture method: userId :"+userId);
		return status;
	}

	@Override
	public MnUsers getcrowdUserDetails(String userName) {
		MnUsers mnUserList = null;
		MnSecurityQuestions security=null;
		if(logger.isDebugEnabled())
		logger.debug("while fetching user from  getcrowdUserDetails method: "+userName);
		Query query = null;
		try
		{
			userName=userName.trim();
			userName=userName.toLowerCase();
			query = new Query(Criteria.where("userName").is(userName));
			mnUserList = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception while getcrowdUserDetails :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getcrowdUserDetails method ");
		return mnUserList;
	}
}
