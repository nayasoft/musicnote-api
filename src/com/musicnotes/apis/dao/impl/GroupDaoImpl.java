package com.musicnotes.apis.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.IGroupDao;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnInvitedUsers;
import com.musicnotes.apis.domain.MnSubscription;
import com.musicnotes.apis.domain.MnSubscriptionMulti;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;

public class GroupDaoImpl extends BaseDaoImpl implements IGroupDao {

	Logger logger = Logger.getLogger(GroupDaoImpl.class);
	@Override
	public String createGroup(MnGroupDomain mnGroupDomain) {
		Query query=null;
		Integer groupId = 0;
		boolean groupFlag=false;
		try
		{
			if(logger.isDebugEnabled()){logger.debug("createGroup method  is called for userId "+mnGroupDomain.getLoginUserId());}
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnGroupDomain> listGroup = mongoOperations.find(query,MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);

			if (listGroup != null && listGroup.size() != 0)
			{
				MnGroupDomain lastGroup = listGroup.get(listGroup.size() - 1);
				groupId = lastGroup.getGroupId() + 1;
			}
			else
			{
				groupId = 1;
			}
			
			query=new  Query(Criteria.where("loginUserId").is(
					(mnGroupDomain.getLoginUserId())));
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnGroupDomain> listGroups = mongoOperations.find(query,MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			
			for(MnGroupDomain group:listGroups)
			{
				if(group.getGroupName().equalsIgnoreCase(mnGroupDomain.getGroupName()))
				{
					groupFlag=true;
					break;
				}
			}
			
			
			mnGroupDomain.setDefaultGroup("false");
			
			if(!groupFlag)
			{
			mnGroupDomain.setGroupId(groupId);
			
			mongoOperations.insert(mnGroupDomain,JavaMessages.Mongo.MNGROUP);
			
			if(logger.isDebugEnabled()){logger.debug("Group successfully inserted  for userId "+mnGroupDomain.getLoginUserId() );}
			
				writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Created new group: <B>"+mnGroupDomain.getGroupName()+"</B>", "contact", "","", null,  "contact");
				
			return ""+groupId;
			}
			else
			{
				return "0";
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating group for userId"+mnGroupDomain.getLoginUserId()+":"+e);
			return "0";
		}
		
		}
	
	@Override
	public String updateGroup(MnGroupDomain mnGroupDomain) {
		String status="";
		try
		{
			if(logger.isDebugEnabled()){logger.debug("updateGroup method  is called for userId "+mnGroupDomain.getLoginUserId());}
			Query querys=new  Query(Criteria.where("loginUserId").is(
					mnGroupDomain.getLoginUserId()).and("groupName").is(mnGroupDomain.getGroupName().trim()));
			MnGroupDomain listGroups = mongoOperations.findOne(querys,MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			if(listGroups!=null )
			{
				if(listGroups.getGroupId().equals(mnGroupDomain.getGroupId()))
				{
					Query query = new Query(Criteria.where("groupId").is(
							mnGroupDomain.getGroupId()).and("loginUserId").is(mnGroupDomain.getLoginUserId()));
				Update groupUpdate = new Update();

				groupUpdate.set("groupId", mnGroupDomain.getGroupId());
				groupUpdate.set("groupName", mnGroupDomain.getGroupName());
				
				mongoOperations.updateFirst(query, groupUpdate, JavaMessages.Mongo.MNGROUP);
				
				MnUsers mnUser = getUserDetailsObject(Integer.parseInt(mnGroupDomain.getLoginUserId())); 
				writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Updated group name to "+mnGroupDomain.getGroupName(), "contact", "","", null, "contact");

				query = new Query(Criteria.where("groupId").is((mnGroupDomain.getGroupId())).and("status").is("A"));
				List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
				
				Set<Integer> groupMenbers = new HashSet<Integer>();
				groupMenbers.addAll(mnUser.getFollowers());
				
				if(detailsDomain!=null &&  !detailsDomain.isEmpty()){
					for(MnGroupDetailsDomain domain2:detailsDomain){
						groupMenbers.add(domain2.getUserId());
					}
				}
				
				status="success";
				}
				else
				{
					status="error";
				}
			}
			else
			{
				Query query = new Query(Criteria.where("groupId").is(
						mnGroupDomain.getGroupId()).and("loginUserId").is(mnGroupDomain.getLoginUserId()));
				Update groupUpdate = new Update();

				groupUpdate.set("groupId", mnGroupDomain.getGroupId());
				groupUpdate.set("groupName", mnGroupDomain.getGroupName());
				
				mongoOperations.updateFirst(query, groupUpdate, JavaMessages.Mongo.MNGROUP);
				
				MnUsers mnUser = getUserDetailsObject(Integer.parseInt(mnGroupDomain.getLoginUserId())); 
				writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Updated group name as "+mnGroupDomain.getGroupName(), "contact", "","", null, "contact");
						
				query = new Query(Criteria.where("groupId").is((mnGroupDomain.getGroupId())).and("status").is("A"));
				List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
				
				Set<Integer> groupMenbers = new HashSet<Integer>();
				groupMenbers.addAll(mnUser.getFollowers());
				
				if(detailsDomain!=null &&  !detailsDomain.isEmpty()){
					for(MnGroupDetailsDomain domain2:detailsDomain){
						groupMenbers.add(domain2.getUserId());
					}
				}
				if(logger.isDebugEnabled()){logger.debug("Group Updated successfully for user "+mnGroupDomain.getLoginUserId() );}
				status="success";
			}

			

			return status;
		}
		catch (Exception e)
		{
			logger.error("Exception while updating group detail  for userId "+mnGroupDomain.getLoginUserId()+":"+e);
		}
		return "Exception while inserting  group detail";
	}
	
	@Override
	public String updateGroupDetails(MnGroupDomain mnGroupDomain) {
		try
 {
			Query query = new Query(Criteria.where("groupId").is(
					(mnGroupDomain.getGroupId())));
			if(logger.isDebugEnabled()){logger.debug("updateGroupDetails method  is called for userId "+mnGroupDomain.getLoginUserId());}
			
			writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Updated group details", "contact", "","", null, null);

			query = new Query(Criteria.where("groupId").is((mnGroupDomain.getGroupId())).and("status").is("A"));
			List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			
			if(detailsDomain!=null &&  !detailsDomain.isEmpty()){
				Set<Integer> groupMenbers = new HashSet<Integer>();
				for(MnGroupDetailsDomain domain2:detailsDomain){
					groupMenbers.add(domain2.getUserId());
				}
			}
			if(logger.isDebugEnabled()){logger.debug("updated group details for userId "+mnGroupDomain.getLoginUserId());}
			return "success";
		}
		catch (Exception e)
		{
			logger.error("Exception while updating group detail for userId "+mnGroupDomain.getLoginUserId()+":" + e);
		}
		return "Exception while inserting  group detail";
	}
	
	
	
	
	@Override	
	public String deleteGroup(MnGroupDomain mnGroupDomain)
	{
		String flag="false"; 
		try{
			if(logger.isDebugEnabled()){logger.debug("deleteGroup method  is called for userId "+mnGroupDomain.getLoginUserId());}			
			Query groupQuery = new Query(Criteria.where("groupId").is(mnGroupDomain.getGroupId()));
			mongoOperations.findAndRemove(groupQuery, MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			flag="true";
			
			writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Deleted group:"+mnGroupDomain.getGroupName(), "contact", "","", null,  "contact");
			
			Query query = new Query(Criteria.where("groupId").is((mnGroupDomain.getGroupId())).and("status").is("A"));
			
			Update update=new Update();
			update.set("endDate",new Date());
			update.set("status","I");
			mongoOperations.updateMulti(query, update, JavaMessages.Mongo.MNGROUPDETAILS);
			if(logger.isDebugEnabled()){logger.debug("Deleted Group for userId "+mnGroupDomain.getLoginUserId());}
		}catch(Exception e){
			logger.error("Exception while deleting group  for userId "+mnGroupDomain.getLoginUserId()+": ", e);
			flag="false";
		}
		return flag;
	}

	@Override
	public List<MnGroupDomain> SearchGroup(String criteria,String id) {
		List<MnGroupDomain> mnGroupList = null;
		Query query = null;
		try
		{
			if(logger.isDebugEnabled()){logger.debug("SearchGroup method  is called criteria: "+criteria);}
			if(criteria.equalsIgnoreCase("groupId"))
			{
				Integer groupId=Integer.parseInt(id);
				query = new Query(Criteria.where(criteria).is(groupId));	
			}
			else
			{
				query = new Query(Criteria.where(criteria).is(id));
			}
			
			mnGroupList = mongoOperations.find(query, MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
			if(logger.isDebugEnabled()){logger.debug("Searched group");}

		}
		catch (Exception e)
		{
			logger.error("Exception while fetching student details :" + e);

		}
		return mnGroupList;
	}

	@Override
	public String addGroupDetails(MnGroupDetailsDomain domain)
	{
		String status="false";
		Query query;
		Integer gdId ;
		try{
			if(logger.isDebugEnabled()){logger.debug("addGroupDetails method  is called");}
			query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnGroupDetailsDomain> listGroups = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);

			if (listGroups != null && listGroups.size() != 0)
			{
				MnGroupDetailsDomain lastGroup = listGroups.get(listGroups.size() - 1);
				gdId = lastGroup.getGdId() + 1;
			}
			else
			{
				gdId = 1;
			}
			
			domain.setGdId(gdId);
			
			mongoOperations.insert(domain,JavaMessages.Mongo.MNGROUPDETAILS);
			
			Query query1=new Query(Criteria.where("groupId").is(domain.getGroupId()));
			MnGroupDomain groupDomain=mongoOperations.findOne(query1, MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			Update update=new Update();
			update.addToSet("groupDetails",domain.getUserId());
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNGROUP);
			
			writeLog(domain.getOwnerId(), "A",  "Added members to "+groupDomain.getGroupName(), "contact", "","", null, "contact");
			if(logger.isDebugEnabled()){logger.debug("Added group details");}
			status="success";
		}catch (Exception e) {
			logger.error("Exception in adding Group Details"+e);
			e.printStackTrace();
		}
		
		return status;
	}

	@Override
	public String updateGroupDetails(MnGroupDetailsDomain domain)
	{
		String status="false";
		Query query=null;
		try{
			if(logger.isDebugEnabled()){logger.debug("updateGroupDetails method  is called");}
			query=new Query(Criteria.where("groupId").is(domain.getGroupId()).and("userId").is(domain.getUserId()).and("status").is("A"));
			Update update=new Update();
			update.set("endDate",new Date());
			update.set("status","I");
			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNGROUPDETAILS);
			
			Query query1=new Query(Criteria.where("groupId").is(domain.getGroupId()));
			MnGroupDomain groupDomain=mongoOperations.findOne(query1, MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			Update update1=new Update();
			if(groupDomain!=null){
				if(groupDomain.getGroupDetails()!=null && !groupDomain.getGroupDetails().isEmpty()){
					groupDomain.getGroupDetails().remove(domain.getUserId());
					update1.set("groupDetails",groupDomain.getGroupDetails());
					mongoOperations.updateFirst(query1, update1, JavaMessages.Mongo.MNGROUP);
				}
			}
			
			// log 
			MnUsers user = getUserDetailsObject(domain.getUserId());
			
			writeLog(domain.getOwnerId(), "A",  "Updated details of "+user.getUserName(), "contact", "","", null, "contact");
			
			Set<Integer> groupMenbers = new HashSet<Integer>();
			groupMenbers.add(domain.getUserId());
			
			status="success";
			if(logger.isDebugEnabled()){logger.debug("Group Details Updated");}
		}catch (Exception e) {
			logger.error("Exception in updating group details"+e);
		}
		
		return status;
	}

	@Override
	public List<MnGroupDetailsDomain> getGroupDetails(MnGroupDetailsDomain domain)
	{
		List<MnGroupDetailsDomain> mnGroupList = null;
		Query query = null;
		try
		{
			if(logger.isDebugEnabled()){
			logger.debug("getGroupDetails method  is called");
			}
			query=new Query(Criteria.where("userId").is(domain.getUserId()).and("ownerId").is(domain.getOwnerId()).and("status").is("A"));
			
			mnGroupList = mongoOperations.find(query, MnGroupDetailsDomain.class, JavaMessages.Mongo.MNGROUPDETAILS);
			if(logger.isDebugEnabled()){
			logger.debug("Got group details");
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while fetching student details :" + e);

		}
		return mnGroupList;
	}

	@Override
	public String getGroupMembers(String[] groupIds) {
		List<MnGroupDetailsDomain> mnGroupList = null;
		Query query = null;
		List<MnGroupDomain> groupName=null;
		List<Integer> gIds=new ArrayList<Integer>();
		String groupId="";
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("getGroupMembers method  is called");
			}
			for(int i=0; i<groupIds.length;i++)
			{
			query=new Query(Criteria.where("groupId").is(Integer.parseInt(groupIds[i].trim())).and("status").is("A"));
			mnGroupList = mongoOperations.find(query, MnGroupDetailsDomain.class, JavaMessages.Mongo.MNGROUPDETAILS);
			
			if(!(mnGroupList !=null && !mnGroupList.isEmpty()))
				gIds.add(Integer.parseInt(groupIds[i].trim()));
			}
			
			if(gIds != null && !gIds.isEmpty())
			{
				query=new Query(Criteria.where("groupId").in(gIds));
				groupName = mongoOperations.find(query, MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
				int i=0;
				for(MnGroupDomain list:groupName)
				{ 
					if(i==0)
						groupId=list.getGroupName();
					else
						groupId= groupId +","+list.getGroupName();
					i++;
				}
			}else
				groupId="0";
		}
		
		catch (Exception e)
		{
			logger.error("Exception while fetching student details :" + e);

		}
		if(logger.isDebugEnabled()){
			logger.debug("getGroupMembers method  is called");
		}

		return groupId;
	}

	@Override
	public String getGroupSearch(String searchText,Integer userId) {
		List<MnUsers> mnUserList = null;
		Query query = null;
		JSONArray array=new JSONArray();
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("getGroupSearch method  is called "+userId+" searchText: "+searchText);
			}
			query = new Query(Criteria.where("status").is("A").and("userId").nin(userId));
			mnUserList = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(mnUserList!=null && !mnUserList.isEmpty())
			{
				for(MnUsers mnUsers:mnUserList)
				{
					if((mnUsers.getUserFirstName()+mnUsers.getUserLastName()).toLowerCase().contains(searchText.toLowerCase()) || mnUsers.getUserName().toLowerCase().contains(searchText.toLowerCase()))
					{
						if(mnUsers.getUserFirstName()!=null && !mnUsers.getUserFirstName().isEmpty() ){
							array.put(mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName()+" , "+mnUsers.getUserName());
						}else{
							array.put(mnUsers.getUserName());
						}
					}
				}
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while searching group details for userId "+userId+" searchText: "+searchText+":" + e);

		}
		if(logger.isDebugEnabled()){
			logger.debug("Got searched group for"+userId+" searchText: "+searchText);
		}

		return array.toString();
	}

	@Override
	public MnUsers getContactSearch(String searchText) {
		MnUsers mnUsers1=new MnUsers();
		Query query = null;
	
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("getContactSearch method  is called and searchText "+searchText);
			}
			query = new Query(Criteria.where("status").is("A").and("userName").is(searchText.trim()));
			mnUsers1 = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(logger.isDebugEnabled()){
				logger.debug(" Got Searched contact for searchText "+searchText);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while searching group details for searchText "+searchText+":" + e);

		}
		return mnUsers1;
	}

	@Override
	public List<MnGroupDomain> getGroupNamesForSearch(String userId)
	{
		List<MnGroupDomain> mnGroupList = null;
		Query query = null;
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("getGroupNamesForSearch method  is called "+userId);
			}

			query = new Query(Criteria.where("loginUserId").is(userId));
			mnGroupList = mongoOperations.find(query, MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
			if(logger.isDebugEnabled()){
				logger.debug("Got Searched group names for "+userId);
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while fetching group names for "+userId+" :" + e);

		}
		return mnGroupList;
	}

	@Override
	public String addInvitedUser(Integer userId,String userSelectDate, String[] userMail,boolean trialCheck) {
		String status="success";
		String existingMailList="";
		Integer id=1;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
		Integer addedLength=0;
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("addInvitedUser method  is called "+userId);
			}
			
			if(trialCheck==true)
			{
				Query query=new Query();
				query.with(new Sort(Sort.Direction.ASC, "_id"));
				List<MnInvitedUsers> listMail = mongoOperations.find(query,MnInvitedUsers.class,JavaMessages.Mongo.MNInvitedUsers);
				
				if (listMail != null && listMail.size() != 0)
				{
					id=listMail.size()+1;
				}
				else
				{
					id = 1;
				}
				Calendar cc=Calendar.getInstance();
				String date=formatter.format(cc.getTime());
					
				for(int i=0;i<userMail.length;i++)
				{
					Query query3 = new Query(Criteria.where("invitedMailID").is(userMail[i].toLowerCase().trim()).and("status").is("A"));
					MnInvitedUsers exist = mongoOperations.findOne(query3, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
					
					Query query4 = new Query(Criteria.where("emailId").is(userMail[i].toLowerCase().trim()));
					MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class, JavaMessages.Mongo.MNUSERS);

					if((!(exist!=null && !exist.equals(""))) && (!(mnUser!=null && !mnUser.equals(""))))
					{
						MnInvitedUsers invitedUsers=new MnInvitedUsers();
						invitedUsers.setId(id+i);
						invitedUsers.setUserId(userId);
						invitedUsers.setInvitedMailID(userMail[i].toLowerCase().trim());
						invitedUsers.setDate(date);
						invitedUsers.setStatus("A");
						invitedUsers.setUserCreated(false);
						invitedUsers.setUserSelectDate(userSelectDate);
						mongoOperations.insert(invitedUsers,JavaMessages.Mongo.MNInvitedUsers);
					
					}else
					{
						existingMailList=existingMailList+userMail[i]+",";
					}
					
					
				}
				
			}else{ //The else part will be executed for premium users and 'if' part for all the other users
			
			Query query1 = new Query(Criteria.where("userId").is(userId));
			MnUsers mnUserList = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			Integer length=mnUserList.getRequestedUser();
			if(length!=null && length>1)
			{
			
			Query query=new Query();
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnInvitedUsers> listMail = mongoOperations.find(query,MnInvitedUsers.class,JavaMessages.Mongo.MNInvitedUsers);
			
			if (listMail != null && listMail.size() != 0)
			{
				id=listMail.size()+1;
			}
			else
			{
				id = 1;
			}
			Calendar cc=Calendar.getInstance();
			String date=formatter.format(cc.getTime());
			
			Query query2 = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			List<MnInvitedUsers> users = mongoOperations.find(query2, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			if(users!=null && !users.isEmpty())
				addedLength=users.size();
			length=length-1;
			length=length-addedLength;
				
			for(int i=0;i<userMail.length;i++)
			{
				if(i<length)
				{
				Query query3 = new Query(Criteria.where("invitedMailID").is(userMail[i].toLowerCase().trim()).and("status").is("A"));
				MnInvitedUsers exist = mongoOperations.findOne(query3, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
				
				Query query4 = new Query(Criteria.where("emailId").is(userMail[i].toLowerCase().trim()));
				MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class, JavaMessages.Mongo.MNUSERS);

				if((!(exist!=null && !exist.equals(""))) && (!(mnUser!=null && !mnUser.equals(""))))
				{
					
					MnInvitedUsers invitedUsers=new MnInvitedUsers();
					invitedUsers.setId(id+i);
					invitedUsers.setUserId(userId);
					invitedUsers.setInvitedMailID(userMail[i].toLowerCase().trim());
					invitedUsers.setDate(date);
					invitedUsers.setStatus("A");
					invitedUsers.setUserCreated(false);
					invitedUsers.setUserSelectDate(userSelectDate);
					Query query7=new Query(Criteria.where("userId").is(userId));
					MnSubscription mnsubscription=mongoOperations.findOne(query7, MnSubscription.class,JavaMessages.Mongo.MNSubscription);
					Integer ids=mnsubscription.getSubId();
					invitedUsers.setPaidSubId(ids);
					mongoOperations.insert(invitedUsers,JavaMessages.Mongo.MNInvitedUsers);
					
				}
				else
				{
					existingMailList=existingMailList+userMail[i]+",";
				}
				}
				else{
					status="Your requesting limit is over";
				}
			}
			}
			}
		}catch (Exception e) {
			logger.error("Exception in adding invited users for "+userId+":"+e);
		}
		if(logger.isDebugEnabled()){
			logger.debug("Added invited usersusers for "+userId);
		}
		if(existingMailList.equals("")){
		return status;
		}else{
		return existingMailList;
		}
	}

	@Override
	public List<MnInvitedUsers> getInvitedUser(Integer userId) 
	{
		List<MnInvitedUsers> users=null;
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("getInvitedUser method  is called "+userId);
			}
			Query userQuery = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			MnUsers mnUser = mongoOperations.findOne(userQuery, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
			if(mnUser.isMultipleUsersRequest())
			{
				users=new ArrayList<MnInvitedUsers>();
				Query query = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
				users = mongoOperations.find(query, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			}
			else
			{
				users=null;
			}
			if(logger.isDebugEnabled()){
				logger.debug("Got invited users for "+userId);
			}
			
		}catch (Exception e) {
			logger.error("Error in getInvitedUser method for "+userId+":"+e.getMessage());
		}
		return users;
	}

	@Override
	public String deletInviteUser(Integer userId, String mailId) {

		String status="";
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("deletInviteUser method  is called "+userId);
			}
			Query query3 = new Query(Criteria.where("invitedMailID").is(mailId).and("status").is("A"));
			MnInvitedUsers exist = mongoOperations.findOne(query3, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
			if(exist!=null)
			{
				Update update1=new Update();
				update1.set("status", "I");
				mongoOperations.updateFirst(query3, update1, JavaMessages.Mongo.MNInvitedUsers);
				status="success";
			}
			
			Query query4 = new Query(Criteria.where("emailId").is(mailId));
			MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(mnUser!=null)
			{
				Update update2=new Update();
				update2.set("status", "I");
				mongoOperations.updateFirst(query4, update2, JavaMessages.Mongo.MNUSERS);
				
				Query query7=new Query(Criteria.where("emailId").is(mailId));
				Update update1=new Update();
				update1.set("status", "I");
				mongoOperations.updateFirst(query7, update1,JavaMessages.Mongo.MNUSERS);
				
				status="success";
			}
			if(logger.isDebugEnabled()){
				logger.debug("Got invited user for "+userId);
			}
			
		}catch (Exception e) {
			logger.error("Error in getInvitedUser method for "+userId+":"+e.getMessage());
			status="error";
		}
		return status;
	
	}

	@Override
	public String updateInvitedUser(Integer userId, String newMail,String oldEmail) 
	{
		String status="";
		try
		{
				if(logger.isDebugEnabled()){
				logger.debug("updateInvitedUser method  is called "+userId);
				}
				Query query3 = new Query(Criteria.where("invitedMailID").is(newMail).and("status").is("A"));
				MnInvitedUsers exist = mongoOperations.findOne(query3, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
				
				Query query4 = new Query(Criteria.where("emailId").is(newMail));
				MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class, JavaMessages.Mongo.MNUSERS);
				
				if((!(exist!=null && !exist.equals(""))) && (!(mnUser!=null && !mnUser.equals(""))))
				{
					Query query5 = new Query(Criteria.where("invitedMailID").is(oldEmail).and("status").is("A"));
					MnInvitedUsers updateMail = mongoOperations.findOne(query5, MnInvitedUsers.class, JavaMessages.Mongo.MNInvitedUsers);
					if(updateMail!=null)
					{
						Update update=new Update();
						update.set("invitedMailID", newMail);
						mongoOperations.updateFirst(query5, update, JavaMessages.Mongo.MNInvitedUsers);
						
						Query query7=new Query(Criteria.where("emailId").is(oldEmail));
						Update update1=new Update();
						update1.set("emailId", newMail);
						mongoOperations.updateFirst(query7, update1,JavaMessages.Mongo.MNUSERS);
					}
					
				}
				else
				{
					status=status+newMail;
				}
				if(logger.isDebugEnabled()){
					logger.debug("Updated invited User for "+userId);
				}
			
		}catch (Exception e) {
			logger.error("Exception while updating invited users for "+userId+":"+e.getMessage());
		}
		return status;
	
	}
	
	@Override
	public String checkPaymentGroupForPremiumUsers(String userId){
		if(logger.isDebugEnabled()){
			logger.debug("checkPaymentGroupForPremiumUsers method  is called "+userId);
		}
		StringBuffer jsonStr = new StringBuffer("[");
		Set<Integer> group=new HashSet<Integer>();
		List<MnUsers>mnUser=new ArrayList<MnUsers>();
		MnUsers userDetail=new MnUsers();
		MnUsers mnUserdetail=new MnUsers();
		MnSubscriptionMulti updateMailDetail=new MnSubscriptionMulti();
		MnUsers mnUserdetails=new MnUsers();
		
		try{
		Query query11=new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
		MnUsers mnUserdetaills=mongoOperations.findOne(query11,MnUsers.class,JavaMessages.Mongo.MNUSERS);
		Query query=new Query(Criteria.where("loginUserId").is(userId).and("defaultGroup").is("true"));
		MnGroupDomain mnGroup=mongoOperations.findOne(query,MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
		if(mnGroup!=null && !mnGroup.equals("")){
			String userLevel="Premium";
			Query query8=new Query(Criteria.where("userId").is(Integer.parseInt(mnGroup.getLoginUserId())));
			mnUserdetail=mongoOperations.findOne(query8,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			Query query9=new Query(Criteria.where("userId").is(mnUserdetail.getUserId()));
			updateMailDetail = mongoOperations.findOne(query9, MnSubscriptionMulti.class, JavaMessages.Mongo.MNSubscriptionMulti);
			Query query7=new Query(Criteria.where("userId").is(updateMailDetail.getPaidUserId()));
			mnUserdetails=mongoOperations.findOne(query7,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			jsonStr.append("{\"name\" : \"" +  mnUserdetail.getUserName() + "\",");
			jsonStr.append("\"mailId\" : \"" +mnUserdetail.getEmailId() + "\",");
			jsonStr.append("\"level\" : \"" + userLevel+ "\",");
			jsonStr.append("\"paidBy\" : \"" + mnUserdetails.getUserName()+ "\",");
			jsonStr.append("\"userId\" : \"" + mnUserdetail.getUserId()+ "\",");
			jsonStr.append("\"expiryDate\" : \"" + mnUserdetail.getEndDate() + "\"},");
			
			group=mnGroup.getGroupDetails();
			for(Integer id:group){
			Query query1=new Query(Criteria.where("userId").is(id));
			mnUser=mongoOperations.find(query1,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			for(MnUsers mnUsers:mnUser){
				Query query2=new Query(Criteria.where("userId").is(mnUsers.getUserId()));
				MnSubscriptionMulti updateMail = mongoOperations.findOne(query2, MnSubscriptionMulti.class, JavaMessages.Mongo.MNSubscriptionMulti);
				Query query3=new Query(Criteria.where("userId").is(updateMail.getPaidUserId()));
				userDetail=mongoOperations.findOne(query3,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				jsonStr.append("{\"name\" : \"" +  mnUsers.getUserName() + "\",");
				jsonStr.append("\"mailId\" : \"" +mnUsers.getEmailId() + "\",");
				jsonStr.append("\"level\" : \"" + userLevel+ "\",");
				jsonStr.append("\"paidBy\" : \"" + userDetail.getUserName()+ "\",");
				jsonStr.append("\"userId\" : \"" + mnUsers.getUserId()+ "\",");
				jsonStr.append("\"expiryDate\" : \"" + mnUsers.getEndDate() + "\"},");
				}
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			}
			else if((mnUserdetaills.getUserLevel()).contains("Premium")==true){//for single pay users
				String userLevel="Premium";
				Query query12=new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));//for trail users
				mnUserdetail=mongoOperations.findOne(query12,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				Query query9=new Query(Criteria.where("userId").is(mnUserdetail.getUserId()));
				updateMailDetail = mongoOperations.findOne(query9, MnSubscriptionMulti.class, JavaMessages.Mongo.MNSubscriptionMulti);
				Query query2=new Query(Criteria.where("userId").is(updateMailDetail.getPaidUserId()).and("status").is("A"));//for trail users
				mnUserdetails=mongoOperations.findOne(query2,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				jsonStr.append("{\"name\" : \"" +  mnUserdetail.getUserName() + "\",");
				jsonStr.append("\"mailId\" : \"" +mnUserdetail.getEmailId() + "\",");
				jsonStr.append("\"level\" : \"" + userLevel+ "\",");
				jsonStr.append("\"paidBy\" : \"" + mnUserdetails.getUserName()+ "\",");
				jsonStr.append("\"userId\" : \"" + mnUserdetail.getUserId()+ "\",");
				jsonStr.append("\"expiryDate\" : \"" + mnUserdetail.getEndDate() + "\"},");
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			else{
				String userLevel="Trial";
				Query query12=new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));//for trail users
				mnUserdetail=mongoOperations.findOne(query12,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				jsonStr.append("{\"name\" : \"" +  mnUserdetail.getUserName() + "\",");
				jsonStr.append("\"mailId\" : \"" +mnUserdetail.getEmailId() + "\",");
				jsonStr.append("\"level\" : \"" + userLevel+ "\",");
				jsonStr.append("\"paidBy\" : \"" + "-"+ "\",");
				jsonStr.append("\"userId\" : \"" + mnUserdetail.getUserId()+ "\",");
				jsonStr.append("\"expiryDate\" : \"" + mnUserdetail.getEndDate() + "\"},");
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
		if(logger.isDebugEnabled()){
			logger.debug("checked payment group for premium users for "+userId);
		}
		}catch(Exception e){
			logger.error("Exception while checking payment Group for premium for users "+userId+":"+e);
		}
		return jsonStr.toString();
	}

	@Override
	public String createDefaultGroup(Integer userId) 
	{
		String status="";
		Query query=null;
		try
		{
			if(logger.isDebugEnabled()){
				logger.debug("createDefaultGroup method  is called "+userId);
			}
		MnUsers requestedUserCheck =null;
		Query query5 = new Query(Criteria.where("userId").is(userId).and("multipleUsersRequest").is(true).and("status").is("A"));
		requestedUserCheck = mongoOperations.findOne(query5, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		if(requestedUserCheck!=null){
			String groupUserId=Integer.toString(requestedUserCheck.getUserId());
			MnGroupDomain mnGroupDomain=new MnGroupDomain();
			mnGroupDomain.setLoginUserId(groupUserId);
			Integer groupId = 0;
			query=new  Query(Criteria.where("loginUserId").ne(""));
			query.with(new Sort(Sort.Direction.ASC, "_id"));
			List<MnGroupDomain> listGroup = mongoOperations.find(query,MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
			if (listGroup != null && listGroup.size() != 0)
			{
				MnGroupDomain lastGroup = listGroup.get(listGroup.size() - 1);
				groupId = lastGroup.getGroupId() + 1;
			}
			else
			{
				groupId = 1;
			}
			mnGroupDomain.setGroupId(groupId);
			mnGroupDomain.setGroupName("Payment group");
			mnGroupDomain.setDefaultGroup("true");
			mongoOperations.insert(mnGroupDomain,JavaMessages.Mongo.MNGROUP);
			writeLog(Integer.parseInt(mnGroupDomain.getLoginUserId()), "A",  "Added new group: <B>"+mnGroupDomain.getGroupName()+"</B>", "contact", "","", null,  "contact");
		}
		if(logger.isDebugEnabled()){
			logger.debug("Created Default Group for userId: "+userId);
		}
		}catch (Exception e) {
			logger.error("Exception while fetching default group for "+userId+" :"+e.getMessage());
		}
		return status;
	}
	@Override
	public MnUsers getExitsEmail(String emailId) {
		if(logger.isDebugEnabled())
		logger.debug("getExitsEmail method called:  "+emailId);
		MnUsers mnUser = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("emailId").is(emailId));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
		}
		catch (Exception e)
		{
			logger.error("Exception while fetching Email details :" + e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getExitsEmail method called:  ");
		return mnUser;
	}
	
	@Override
	public String userInformation(String userId){
		
		if(logger.isDebugEnabled()){
			logger.debug("userInformation method  is called "+userId);
		}
		
		StringBuffer jsonStr = new StringBuffer("[");
		MnUsers mnUsers=new MnUsers();
		MnUsers mnUsers1=new MnUsers();
		MnUsers mnUsersinfo=new MnUsers();
		Query query=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
		MnSubscription mnSubscribe=mongoOperations.findOne(query,MnSubscription.class,JavaMessages.Mongo.MNSubscription);
		try{
		if(mnSubscribe!=null && !mnSubscribe.equals("")){
			Query query1=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			mnUsers=mongoOperations.findOne(query1,MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if(mnUsers!=null && !mnUsers.equals("")){
				jsonStr.append("{\"offer\" : \"" + "$"+ mnSubscribe.getSubType() + "\",");
				jsonStr.append("\"startDate\" : \"" +mnUsers.getStartDate() + "\",");
				jsonStr.append("\"expiryDate\" : \"" + mnUsers.getEndDate() + "\"},");
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			}else{
				Query query1=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
				mnUsersinfo=mongoOperations.findOne(query1,MnUsers.class,JavaMessages.Mongo.MNUSERS);
				Query query2=new Query(Criteria.where("invitedMailID").is(mnUsersinfo.getEmailId()));
				MnInvitedUsers mnInvitedUsers=mongoOperations.findOne(query2,MnInvitedUsers.class,JavaMessages.Mongo.MNInvitedUsers);
				Query query3=new Query(Criteria.where("userId").is(mnInvitedUsers.getUserId()));
				MnSubscription mnSubscribe1=mongoOperations.findOne(query3,MnSubscription.class,JavaMessages.Mongo.MNSubscription);
				if(mnSubscribe1!=null && !mnSubscribe1.equals("")){
					Query query4=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
					mnUsers1=mongoOperations.findOne(query4,MnUsers.class,JavaMessages.Mongo.MNUSERS);
					if(mnUsers1!=null && !mnUsers1.equals("")){
						jsonStr.append("{\"offer\" : \"" + "$"+ mnSubscribe1.getSubType() + "\",");
						jsonStr.append("\"startDate\" : \"" +mnUsers1.getStartDate() + "\",");
						jsonStr.append("\"expiryDate\" : \"" + mnUsers1.getEndDate() + "\"},");
					}
					jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
					jsonStr.append("]");
					}
			
			}
		if(logger.isDebugEnabled()){
			logger.debug("user Information inserted for "+userId);
		}
		}catch(Exception e){
			logger.error("Exception in user information method for "+userId+":"+e.getMessage());
		}
		
		return jsonStr.toString();
	}


	@Override
	public String addFriendRequestForInviteUser(String mail,
			String userFirstName, String userLastName) {
		String status="";
		try{
			SendMail sendMail=new SendMail(); 	
			String recipients[]={mail};
						String message=MailContent.inviteUser+userFirstName+" "+userLastName+MailContent.inviteUser1+" " +
						"<div><a href="+"https://www.musicnoteapp.com/musicnote/index.html?MailLink?"+mail+"#registration"+">Click here to register to Musicnote</a></div>";
				
					sendMail.postEmail(recipients, userFirstName+" "+userLastName+" has invited you to Musicnote ",message);
			
		}catch(Exception e){
		logger.error("Exception in addFriendRequestForInviteUser method");	
		}
		return status;
	}

}
