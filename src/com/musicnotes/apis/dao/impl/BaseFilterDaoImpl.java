package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.dao.impl.BaseDaoImpl;
import com.musicnotes.apis.dao.interfaces.IFilterDao;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.util.JavaMessages;

public class BaseFilterDaoImpl extends BaseDaoImpl implements IFilterDao {
	Logger logger = Logger.getLogger(BaseFilterDaoImpl.class);
	boolean blockedStatus=false;
	public boolean isValidToken(String token, String time,
			HttpServletRequest request) {

		MnUsersToken usersToken = null;
		boolean userCheckId = false;
		try {
			Query query = new Query(Criteria.where("tokens").is(token));
			usersToken = mongoOperations.findOne(query, MnUsersToken.class,
					JavaMessages.Mongo.MNUsersToken);
			
			if (usersToken != null) {
				blockedStatus=blockedUserCheck(usersToken.getUserId());
				if(blockedStatus)
				{
				// check the time if the token is valid or not(with in hour)
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date dateCreate = df.parse(usersToken.getCreatedDateTime());
				Calendar createTime = Calendar.getInstance();
				createTime.setTime(dateCreate);
				createTime.add(Calendar.HOUR, 1);
				createTime.add(Calendar.MINUTE, 5);
				Calendar timeNow = Calendar.getInstance();

				// logger.info("time now ::"+timeNow.getTime()
				// +" createTime :::"+createTime.getTime());
				if (timeNow.before(createTime))
					userCheckId = true;
				else
					userCheckId = false;
				}
				else
				userCheckId = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Login checking  : " + e);
		}
		if (logger.isDebugEnabled())
			logger.debug("check token and returned valid or not userCheckId::::"+ userCheckId+" userId "+usersToken.getUserId());

		return userCheckId;
	}

	@Override
	public boolean isValidTokenForUpload(String token, String date) {
		if (logger.isDebugEnabled())
			logger.debug("isValidTokenUpload called" + token);

		MnUsersToken usersToken = null;
		boolean userCheckId = false;
		try {
			Query query = new Query(Criteria.where("tokens").is(token));
			usersToken = mongoOperations.findOne(query, MnUsersToken.class,
					JavaMessages.Mongo.MNUsersToken);
			logger.debug("usersToken::"+usersToken);
			if (usersToken != null) {
				blockedStatus=blockedUserCheck(usersToken.getUserId());
				if(blockedStatus)
				{
				// check the time if the token is valid or not(with in hour)
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date dateCreate = df.parse(usersToken.getCreatedDateTime());
				Calendar createTime = Calendar.getInstance();
				createTime.setTime(dateCreate);
				createTime.add(Calendar.HOUR, 1);
				createTime.add(Calendar.MINUTE, 5);
				Calendar timeNow = Calendar.getInstance();

				logger.info("time now ::"+timeNow.getTime()
				+" createTime :::"+createTime.getTime());
				if (timeNow.before(createTime))
					userCheckId = true;
				else
					userCheckId = false;
				}else
					userCheckId = false;

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Login checking  : " + e);
		}
		if (logger.isDebugEnabled())
			logger.debug("isValidTokenUpload"+ userCheckId+"userId "+usersToken.getUserId());

		return userCheckId;
	}

	@Override
	public boolean isValidTokenForUser(String token, String date,HttpServletRequest request) {
		MnUsersToken usersToken = null;
		boolean userCheckId = false;
		try {
			Query query = new Query(Criteria.where("tokens").is(token));
			usersToken = mongoOperations.findOne(query, MnUsersToken.class,
					JavaMessages.Mongo.MNUsersToken);
			if (usersToken != null) {
				blockedStatus=blockedUserCheck(usersToken.getUserId());
				if(blockedStatus)
				{
				// check the time if the token is valid or not(with in hour)
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date dateCreate = df.parse(usersToken.getCreatedDateTime());
				Calendar createTime = Calendar.getInstance();
				createTime.setTime(dateCreate);
				createTime.add(Calendar.HOUR, 1);
				createTime.add(Calendar.MINUTE,5);
				Calendar timeNow = Calendar.getInstance();

				// logger.info("time now ::"+timeNow.getTime()
				// +" createTime :::"+createTime.getTime());
				if (timeNow.before(createTime))
					userCheckId = true;
				else
					userCheckId = false;
				}else
					userCheckId = false;

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("User checking  : " + e);
		}
		if (logger.isDebugEnabled())
			logger.debug("check token and returned valid or not userCheckId::::"+ userCheckId+" userId "+usersToken.getUserId());

		return userCheckId;
	}

	@Override
	public boolean isValidTokenForNewGenerate(String token, String date,HttpServletRequest request) 
	{
		MnUsersToken usersToken = null;
		boolean userCheckId = false;
		try 
		{
			Query query = new Query(Criteria.where("tokens").is(token));
			usersToken = mongoOperations.findOne(query, MnUsersToken.class,JavaMessages.Mongo.MNUsersToken);
			if (usersToken != null)
			{
				blockedStatus=blockedUserCheck(usersToken.getUserId());
				if(blockedStatus)
					userCheckId = true;
				else
					userCheckId = false;
			}
				else
					userCheckId = false;
			
		} catch (Exception e) {
			logger.error("User checking  : " + e.getMessage());
		}
		if (logger.isDebugEnabled())
			logger.debug("check token and returned valid or not userCheckId::::"+ userCheckId+" userId "+usersToken.getUserId());

		return userCheckId;
	}
	
	public boolean blockedUserCheck(Integer userId)
	{
		MnBlockedUsers blockedUsers=null;
		boolean status=false;
		try
		{
			Query query=new Query(Criteria.where("userId").is(userId).and("blockedLevel").is("All").and("status").is("A"));
			blockedUsers=mongoOperations.findOne(query, MnBlockedUsers.class,JavaMessages.Mongo.MNBLOCKEDUSERS);
			if(blockedUsers==null)
			{
				status=true;
			}
		}catch(Exception e)
		{
			
		}
		return status;
	}
		

}
