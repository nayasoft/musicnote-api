package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnAdminComplaintMailTemplate;
import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnAdminFiles;
import com.musicnotes.apis.domain.MnAdminSecurityQuestion;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnBlog;
import com.musicnotes.apis.domain.MnComplaintAction;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnAdminFiles;
import com.musicnotes.apis.domain.MnUserLogs;

public interface IAdminDao {

	String updateMacheckDefaultConfiguration();

	String addDefaultConfiguration(MnAdminConfiguration admin);

	String checkExistingUserLevel(String userLevel, Integer mailNo);

	String updateMailConfigurations(MnMailConfiguration configuration);

	String deletMailConfigure(String mailNo);

	List<MnMailConfiguration> getMailConfiguration();

	String updateMailNotification(Boolean status1, Boolean status2);

	MnAdminConfiguration viewDefaultConfiguration();

	String updateDefaultConfiguration(String configId,String paymentAmountDays, String status1, String uploadLimit,String userId);

	String createMailConfigurations(MnMailConfiguration mailConfiguration);

	List<MnUsers> fetchUserDetailsBasedLevelRole(MnUsers user);

	List<MnComplaints> fetchComplaintDetailsForAdmin(String userComplaint);

	List<MnNoteDetails> getComplaintNote(String listId, String noteId, String userId);

	String getComplaintDeleteNote(String listId, String noteId,
			String userId, String noteName, String adminAction, String adminComment, String actionFrom, String compliantId);

	String fetchUserDetailsForChartView(MnUsers user);

	String deleteComments(String cId, String listId, String noteId, String compliantId, String adminComment);

	String adminDeleteReportComment(String cId, String listId, String noteId, String adminAction, String adminComment);

	List<MnComplaints> fetchNoteComplaintDetailsForAdmin(String complaints);

	String deleteAttachmentInCrowd(String fileName, String userId,
			String noteId, String listId, String noteName, String adminComment, String action, String compliantId);

	String deleteAttachmentInBothNoteCrowd(String fileName, String userId,
			String noteId, String listId, String noteName, String adminComment, String action);

	String adminStatusReport(String listId, String noteId, String userId,
			String noteName, String adminAction, String adminComment, String compliantId,String mailTemplateId);

	String addMailTemplates(String templateName, String templateText);

	List<MnAdminComplaintMailTemplate> fetchMailTemplates(String userId);

	String updateMailTemplates(String templateName, String templateText,
			String templateId);

	String adminAddSecurityQuestions(String question);

	List<MnAdminSecurityQuestion> fetchsecurityQuestionView(String userId);

	String updateSecurityQuestions(String qId, String questionId,
			String question,String type);
	
	List<MnComplaintAction> fetchComplaints();
	
	List<MnUsers> getMnUserDetails();
	
	String addPrefrenceFlag(String userId,String addPrefrence);

	MnUsers fetchUserForComplaint(Integer userId);

	String insertBlockedUser(MnBlockedUsers mnBlockingUsers);

	MnBlockedUsers fetchBlockedUserList(Integer temp);
	
	String updateBlockedUser(MnBlockedUsers mnBlockedUsers);
	
	List<MnAdminFiles> getMnAdminFilesDetails();

	String insertFileDetailsForAdmin(MnAdminFiles mnAdminFiles);

	String deleteUploadedFile(MnAdminFiles mnAdminFiles);

	String checkSecurityQuestion(String questionId);

	String crowdComplaintDeleteButtonBlock(String complaintId);
	MnUsers getUserDetailsObject(Integer userId);

	List<MnAdminFiles> getUploadedFilesForHelp();
    List<MnUsers> fetchUserDetailsBasedOnReportSignUpLevel(String reportLevel,String reportStartDate,String reportEndDate);

    List<MnUserLogs> fetchUserDetailsBasedOnReportLoginLevel(String reportLevel,String reportStartDate,String reportEndDate);

    MnUsers getViewUserDetails(Integer userId);

    String checkBlockedUserForAddPreference(String userId);
    
    String insertBlogDetails(MnBlog mnBlog);
    
    List<MnBlog> getBlogDetails(String status);

	String updateBlogDetails(MnBlog mnBlog);

	String deleteBlogDetails(String blogId);

}
