package com.musicnotes.apis.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

import com.musicnotes.apis.dao.impl.FriendDaoImpl;
import com.musicnotes.apis.dao.impl.GroupDaoImpl;


public class SendMail
{
	Logger logger = Logger.getLogger(SendMail.class);
	 static final String FROM = "support@musicnoteapp.com";   // Replace with your "From" address. This address must be verified.
	    static final String TO = "rveeraprathaban@nayasoft.in";  // Replace with a "To" address. If you have not yet requested
	                                                       // production access, this address must be verified.
	    // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
	    static final String SMTP_USERNAME = "AKIAJK3DDSMNVBOUB6NQ";  // Replace with your SMTP username.
	    static final String SMTP_PASSWORD = "AmmwM+00ip8foTDJBjhv6d74Jv6Nguu1Rd8SRwy6MgAr";  // Replace with your SMTP password.
	    
	    // Amazon SES SMTP host name. This example uses the us-east-1 region.
	    static final String HOSTNAME = "email-smtp.us-west-2.amazonaws.com";    
	    
	    // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
	    // STARTTLS to encrypt the connection.
	    static final int SMTP_HOST_PORT = 587;
	   // static final int SMTP_HOST_PORT = 25;
	
	public void postEmail( String recipients[], String subject, String messages) 
	throws MessagingException
	{
   	 try {/*
   		 logger.info("postEmail method called :");
		    Properties props = new Properties();
		    props.put("mail.transport.protocol", "smtps");
		    props.put("mail.smtps.host", HOSTNAME);
		    props.put("mail.smtps.auth", "true");
		    props.put("mail.smtps.port", SMTP_HOST_PORT);
		    props.put("mail.smtp.starttls.enable", "true");
		    Session mailSession = Session.getDefaultInstance(props);
		    mailSession.setDebug(true);
		    Transport transport = mailSession.getTransport();
		    MimeMessage message = new MimeMessage(mailSession);

		    message.setSubject(subject);
		    message.setContent(messages, "text/html");
		    Address[] from = InternetAddress.parse("support@musicnoteapp.com");//Your domain email
		    message.addFrom(from);
		    message.setFrom(new InternetAddress(""));
		    String toAddress=recipients[0];
		    message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress)); //Send email To (Type email ID that you want to send)

		    transport.connect(HOSTNAME, SMTP_USERNAME, SMTP_PASSWORD);
		    transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		    transport.close();
		*/
   		 // Create a Properties object to contain connection configuration information.
     	Properties props = System.getProperties();
     	props.put("mail.transport.protocol", "smtp");
     	props.put("mail.smtp.port", SMTP_HOST_PORT); 
     	
     	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
     	// The SMTP session will begin on an unencrypted connection, and then the client
         // will issue a STARTTLS command to upgrade to an encrypted connection.
     	props.put("mail.smtp.auth", "true");
     	props.put("mail.smtp.starttls.enable", "true");
     	props.put("mail.smtp.starttls.required", "true");

         // Create a Session object to represent a mail session with the specified properties. 
     	Session session = Session.getDefaultInstance(props);

         // Create a message with the specified information. 
         MimeMessage msg = new MimeMessage(session);
         msg.setFrom(new InternetAddress(FROM));
         msg.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients[0]));
         msg.setSubject(subject);
         msg.setContent(messages,"text/html");
             
         // Create a transport.        
         Transport transport = session.getTransport();
         

         // Connect to Amazon SES using the SMTP username and password you specified above.
         transport.connect(HOSTNAME, SMTP_USERNAME, SMTP_PASSWORD);
     	
         // Send the email.
         transport.sendMessage(msg, msg.getAllRecipients());
   	 
   	 } catch (Exception e) {
			logger.error("Error in postEmail method :"+e.getMessage());
		 e.printStackTrace();
		}
		}

	public  Date  getDateNow() {
		Calendar currentDate = Calendar.getInstance();
		return currentDate.getTime();
	}

	

}
