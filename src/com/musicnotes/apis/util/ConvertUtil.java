package com.musicnotes.apis.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;

public class ConvertUtil
{
	static Logger logger=Logger.getLogger(ConvertUtil.class);
	
	public static void main(String[] args) throws Exception
	{

		String xml = "<callList><contacts><contact> <contactPhoneNum>9790814203</contactPhoneNum> <contactNotes>sample test case 3</contactNotes> <firstName class='string'>Bharath</firstName> <lastName class='string'>Reddy</lastName> <fullName class='string'>Jaya Bharath reddy</fullName> <email class='string'>jaya_n_1@yahoo.co.in</email> <other class='string'>department 3</other> </contact> <contact> <contactPhoneNum>9790814203</contactPhoneNum> <contactNotes>sample test case 3</contactNotes> <firstName class='string'>Bharath</firstName> <lastName class='string'>Reddy</lastName> <fullName class='string'>Jaya Bharath reddy</fullName> <email class='string'>jaya_n_1@yahoo.co.in</email> <other class='string'>department 3</other> </contact></contacts> <contactGroupDesc>Bharath call list from webservice</contactGroupDesc> <countryCode>1</countryCode> <connectbackCountryCode>1</connectbackCountryCode> <dnc>false</dnc> <duplicatesAllowed>false</duplicatesAllowed> <emailAllowed>false</emailAllowed> <cellPhoneAllowed>false</cellPhoneAllowed> <createSeparateDuplicateList>false</createSeparateDuplicateList> <createAlternateAreaCodeList>false</createAlternateAreaCodeList> <broadcastGroupSIDs/> </callList>";
		String json = xmlTOJason(xml);
		System.out.println(json);
		String xml1 = jasonToXml(json, "callList", "contact");
		System.out.println(xml1);

	}

	public static String jasonToXml(String jason, String root, String element)
	{

		JSON json1 = JSONSerializer.toJSON(jason);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setRootName(root);
		serializer.setElementName(element);
		serializer.setTypeHintsEnabled(false);
		String xml = serializer.write(json1);

		return xml;
	}

	public static String xmlTOJason(String xml)
	{
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSON json = xmlSerializer.read(xml);

		return json.toString();
	}


	public static String dbObjectsToJson(List<DBObject> dbObjects)
	{
		List dbList=new BasicDBList();
		for(DBObject dbObject : dbObjects)
		{
			dbList.add(dbObject);
		}
		return dbList.toString();
	}

	public String javaToJsonConverstion(List<?> classList)
	{
		ObjectMapper mapper = new ObjectMapper();
		String jsonConvertedString = "";

		try
		{
			// Converting a Java object to a Json String
			Writer strWriter = new StringWriter();
			mapper.writeValue(strWriter, classList);
			jsonConvertedString = strWriter.toString();
		} catch (JsonGenerationException e)
		{

			e.printStackTrace();

			logger.error("Error in javaToJsonConverstion");

		} catch (JsonMappingException e)
		{
			e.printStackTrace();

			logger.error("Error in javaToJsonConverstion");

		} catch (IOException e)
		{

			e.printStackTrace();

			logger.error("Error in javaToJsonConverstion");

		}

		return jsonConvertedString;
	}


	
	public static boolean urlCheck(String paramString3){
		 try
		    {
		      URL con = new URL(paramString3);
		      return true; 
		    }
		    catch (MalformedURLException e)
		    {
		      return false; 
		    }
	}
	public static boolean isURLValid(String url) {
		if ((url != null) && (!url.isEmpty())) {
			String urlTrimmed = url.trim();
			if (!urlTrimmed.contains(" ")) {
				String expression = "((http|https)(:\\/\\/))?([a-zA-Z0-9]+[.]{1}){2}[a-zA-z0-9]+(\\/{1}[a-zA-Z0-9]+)*\\/?";
				
				CharSequence inputStr = urlTrimmed;
				
				Pattern pattern = Pattern.compile(expression);
				Matcher matcher = pattern.matcher(inputStr);
				if (matcher.matches()) {
					return true;
				} else {
					expression ="[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
					pattern = Pattern.compile(expression);
					matcher = pattern.matcher(inputStr);
					if (matcher.matches()){
						return true;
					}else{
						expression ="^(http|https)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\+&amp;%\\$#\\=~_\\-]+))*$";
						pattern = Pattern.compile(expression);
						matcher = pattern.matcher(inputStr);
						if (matcher.matches()){
							return true;
						}else{
							expression ="(www)+\\.+[a-z]+\\.+[a-z]+";
							pattern = Pattern.compile(expression);
							matcher = pattern.matcher(inputStr);
							if (matcher.matches()){
								return true;
							}else{
								expression ="^((https?|ftp)://|(www|ftp)\\.)[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";;
								pattern = Pattern.compile(expression);
								matcher = pattern.matcher(inputStr);
								if (matcher.matches())
									return true;
								else
									return false;
							}
						}
					}
					
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
