package com.musicnotes.apis.util;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;



public class PasswordGenerator 
{
	static ShaPasswordEncoder encoder = new ShaPasswordEncoder();
	

	public static byte[] encoder(String userName,String passWord) 
	{
		return encoder.encodePassword(userName, passWord).getBytes();
	}
		
}


