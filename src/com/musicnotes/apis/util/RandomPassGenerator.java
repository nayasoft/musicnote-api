package com.musicnotes.apis.util;

import java.util.Random;

public class RandomPassGenerator {
	public static String getRandomPassword()
	{
		 Random random = new Random();
		 String[] startEndPart={"AZ","BX","CY","DB","EL","FM","GO","HU","IP","JR"};
		 String[] part1={"8j","9f","6y","5r","3h","4k","2d","1v","0o","7x"};
		 String[] part2={"*6","*9","*5","@1","@2","@3","@9","6@","7*","@5"};
		 String randomPass=startEndPart[random.nextInt(10)]+part1[random.nextInt(10)]+part2[random.nextInt(10)]+startEndPart[random.nextInt(10)];
		 return randomPass;
	}
}
